(* compatibility_le.v *)

(* This file contains the proof of the compatibility lemmas for the weakening
   relation, that is, the proof that the weakening rules preserve their
   semantic interpretation given in the file [interp.v].
*)

From effect_system Require Export rules interp.
From lib Require Import lists maps_and_sets.


(** * Effect Signatures. *)

Section sem_le_eff_sig.
  Context `{!heapG Σ}.

  (* Concrete signature. *)
  Lemma SConc_sem_le_eff_sig D s α β α' β' :
    D ⊨ α ≤T α' -∗
      D ⊨ β' ≤T β -∗
        D ⊨ (ESig s α β) ≤S (ESig s α' β').
  Proof.
    iIntros "#HA #HB !#" (???) "#HD".
    iSplit; [|done].
    iApply EFF_weaken; iApply SIG_mono.
    - by iModIntro; iApply "HA".
    - by iModIntro; iApply "HB".
  Qed.

  (* Abstract signature. *)
  Lemma SAbst_sem_le_eff_sig D i :
    ⊢ D ⊨ (RVar i) ≤S (RVar i).
  Proof. by iIntros "!#" (???) "_"; iSplit; [iApply iEff_le_refl|]. Qed.

End sem_le_eff_sig.


(** * Rows. *)

Section sem_le_row.
  Context `{!heapG Σ}.

  (* Empty row. *)
  Lemma RNil_sem_le_row D b :
    ⊢ D ⊨ [] ≤R [] @ b.
  Proof.
    iIntros "!#" (???) "#HD". iSplit; [|iSplit]; try auto.
    by iApply iEff_le_refl.
  Qed.

  (* Row extension. *)
  Lemma RCons_sem_le_row D b ρ σ :
    ⊢ D ⊨ ρ ≤R (σ :: ρ) @ b.
  Proof.
    iIntros "!#" (???) "#HD". iSplit; [|iSplit].
    - iApply EFF_incl.
      by apply interp_row_subseteq; set_solver.
    - iApply EffLabels_incl.
      by apply fmap_submseteq, interp_row_submseteq, submseteq_cons.
    - iPureIntro. intros _.
      rewrite interp_row_cons fmap_app.
      by apply submseteq_inserts_l.
  Qed.

  (* Permutation of signatures in a row. *)
  Lemma RSwap_sem_le_row D b ρ σ σ' :
    ⊢ D ⊨ (σ :: σ' :: ρ) ≤R (σ' :: σ :: ρ) @ b.
  Proof.
    iIntros "!#" (???) "#HD". iSplit; [|iSplit].
    - iApply EFF_incl.
      by apply interp_row_subseteq; set_solver.
    - iApply EffLabels_incl.
      by apply fmap_submseteq, interp_row_submseteq, submseteq_swap.
    - iPureIntro. intros _.
      rewrite !interp_row_cons !fmap_app !app_assoc.
      apply submseteq_skips_r.
      by apply Permutation_submseteq, Permutation_app_comm.
  Qed.

  (* Weakening of the tail of rows. *)
  Lemma RSkip_sem_le_row D b ρ ρ' σ σ' :
    D ⊨ σ ≤S σ' -∗
      (D ⊨ ρ ≤R ρ' @ false) -∗
        D ⊨ (σ :: ρ) ≤R (σ' :: ρ') @ b.
  Proof.
    specialize sem_le_eff_sig_persistent as Hsig.
    iIntros "#HS".
    clear Hsig.
    iIntros "#HR !#" (???) "#HD".
    iDestruct ("HR" with "HD") as "(_&_&%Hsubm)".
    iDestruct ("HS" with "HD") as "[Hincl %Heq]".
    iSplit; [|iSplit].
    - rewrite !interp_row_cons.
      iDestruct ("HR" with "HD") as "[Hρ _]".
      iApply EFF_app; [|done].
      by iApply "Hincl".
    - rewrite !interp_row_cons !fmap_app.
      iApply EffLabels_incl.
      rewrite Heq.
      by apply submseteq_skips_l, Hsubm.
    - iPureIntro; intros _.
      rewrite !interp_row_cons !fmap_app Heq.
      apply submseteq_app; [done|].
      by apply Hsubm.
  Qed.

  (* Erasure of an Abs signature. *)
  Lemma RAbs_sem_le_row D s ρ ss js :
    D !! s = Some (ss, js) →
      le.conc_sigs ρ ⊆ ss →
        le.abst_sigs ρ ⊆ js →
          ⊢ D ⊨ ((EAbs s) :: ρ) ≤R ρ @ true.
  Proof.
    iIntros (Hlkp Hss Hjs); iIntros "!#" (???) "#HD".
    iSplit; [|iSplit;[|by iIntros (?)]].
    - iApply iEff_le_trans; [|by iApply EFF_introduce].
      by iApply EFF_weaken; iApply SIG_le_bottom.
    - by iApply (EffLabels_row_cons with "HD").
  Qed.

  (* Transitivity. *)
  Lemma RTrans_sem_le_row D b ρ ρ' ρ'' :
    D ⊨ ρ  ≤R ρ' @ b -∗
      D ⊨ ρ' ≤R ρ'' @ b -∗
        D ⊨ ρ  ≤R ρ'' @ b.
  Proof.
    iIntros "#HR #HR' !#" (???) "#HD".
    iDestruct ("HR"  with "HD") as "(Hρ  & HL  & %Hsubm )".
    iDestruct ("HR'" with "HD") as "(Hρ' & HL' & %Hsubm')".
    iSplit; [|iSplit;[iIntros "HL''"|]].
    - by iApply (iEff_le_trans with "Hρ").
    - by iApply "HL"; iApply "HL'".
    - iPureIntro; intros ->.
      by apply (submseteq_trans _ _ _
                 (Hsubm eq_refl) (Hsubm' eq_refl)).
  Qed.

End sem_le_row.


(** * Types. *)

Section sem_le_ty.
  Context `{!heapG Σ}.

  (* Reflexivity. *)
  Lemma TRefl_sem_le_ty D α :
    ⊢ D ⊨ α ≤T α.
  Proof. by iIntros "!#" (???) "_"; auto. Qed.

  (* Bottom. *)
  Lemma TBot_sem_le_ty D α :
    ⊢ D ⊨ TBot ≤T α.
  Proof. by iIntros "!#" (???); iIntros "_" (?) "?". Qed.

  (* Top. *)
  Lemma TTop_sem_le_ty D α :
    ⊢ D ⊨ α ≤T TTop.
  Proof. by iIntros "!#" (???); iIntros "_" (?) "?". Qed.

  (* Arrow. *)
  Lemma TArrow_sem_le_ty D b a a' α α' ρ ρ' β β' :
    let D' := le.update_disj_ctx ρ' D in

    a ≤A a' →
      D' ⊨ α' ≤T α -∗
        D' ⊨ β ≤T β' -∗
          D' ⊨ ρ ≤R ρ' @ b -∗
            D ⊨ (α -{a; ρ}-> β) ≤T (α' -{a'; ρ'}-> β').
  Proof.
    intros D' Ha.
    iIntros "#HA #HB #HR !#" (???) "#HD".
    iIntros (v) "#Hv". fold interp._ty.
    iIntros "!#" (w) "#Hw". fold interp._ty.
    iApply mwp_mono_att; first by apply Ha.
    iApply mwp_learn. iIntros "#Hρ".
    iDestruct (interp_le_disj_ctx_update with "Hρ HD") as "#HD'". fold D'.
    iDestruct ("HR" with "HD'") as "[HiEff_le HLabels]".
    iSpecialize ("Hv" $! w with "[]").
    { by iApply "HA". }
    iSpecialize ("HLabels" with "Hρ").
    case a; iIntros "_"; iSpecialize ("Hv" with "HLabels").
    - iApply (ewp_strong_mono with "Hv"); try done.
      by iIntros "!#" (y) "Hy !>"; iApply "HB".
    - iApply (pewp_strong_mono with "Hv"); try done.
      by iIntros "!#" (y) "Hy"; iApply "HB".
  Qed.

  (* Transitivity. *)
  Lemma TTrans_sem_le_ty D α α' α'' :
    D ⊨ α ≤T α' -∗
      D ⊨ α' ≤T α'' -∗
        D ⊨ α ≤T α''.
  Proof.
    iIntros "#HA #HA' !#" (???) "#HD".
    iIntros (v) "#Hv".
    iApply ("HA'" with "HD").
    by iApply ("HA" with "HD").
  Qed.

End sem_le_ty.
