(* mixed_weakestpre.v *)

(* In this file, we define a notion of weakest precondition that combines
   [wp] and [pwp]. This definition will allow us to factor out the properties
   that are common to both of these notions. Another approach to avoid the
   duplication of proofs would be to introduce a type class with an abstract
   notion of weakest precondition and with its reasoning rules stated as
   axioms. We leave this alternative approach to future consideration. *)

From iris.proofmode Require Import base tactics classes.
From iris.program_logic  Require Import weakestpre adequacy.
From logic Require Import ieff protocol_agreement tactics notation.
From logic Require Import heap pure_weakestpre labeled_effects adequacy.
From effect_system Require Import types rules.


(** * Mixed Weakest Precondition. *)

(** Definition. *)

Section mixed_weakest_precondition.
  Context `{!heapG Σ}.

  Definition mwp_def (a : att) :
    expr -d> eff_listO Σ -d> (val -d> iPropO Σ) -d> iPropO Σ :=
    match a with I => labeled_effects.wp_def | P => pwp_def end.

  Global Instance mwp_ne a e E n :
    Proper ((dist n) ==> (dist n)) (mwp_def a e E).
  Proof. by case a; apply _. Qed.

  Global Instance mwp_proper a e E : Proper ((≡) ==> (≡)) (mwp_def a e E).
  Proof. by case a; apply _. Qed.

End mixed_weakest_precondition.


(** Notation. *)

Notation "'MWP' e @ a <| E '|' '>' {{ Φ  } }" :=
  (mwp_def a e%E E Φ)
  (at level 20, e, a, E, Φ at level 200).

Notation "'MWP' e @ a <| E '|' '>' {{ v , Φ } }" :=
  (mwp_def a e%E E (λ v, Φ)%I)
  (at level 20, e, a, E, Φ at level 200).


(** Handler judgment. *)

Section handler_judgment.
  Context `{!heapG Σ}.

  Definition is_mixed_shandler (a : att) :
    (expr -d> expr -d> eff_label -d>
       pure_iEff Σ -d> eff_list Σ -d> eff_list Σ -d>
         (_ -d> iPropO Σ) -d> (_ -d> iPropO Σ) -d> iPropO Σ) :=
    match a with P => is_pure_shandler | I => is_shandler end.

  Definition is_mixed_handler (a : att) :
    (expr -d> expr -d> iEff Σ -d> eff_list Σ -d> (_ -d> iPropO Σ) -d>
       (_ -d> iPropO Σ) -d> iPropO Σ) :=
    match a with P => is_pure_handler | I => is_handler end.

  Lemma is_mixed_shandler_unfold a h r l Ψ E E' Φ Φ' :
    is_mixed_shandler a h r l Ψ E E' Φ Φ' ⊣⊢ (
     (* Return branch. *)
     □ (∀ (v : val),
        Φ v -∗ MWP (r v) @ a <| E' |> {{ Φ' }}) ∧

     (* Effect branch. *)
     □ (∀ (v k : val),
        protocol_agreement v Ψ (λ w,
          MWP (k w) @ a <| (l, Ψ) :: E |> {{ Φ }}) -∗
        MWP (h v k) @ a <| E' |> {{ Φ' }}))%I.
  Proof. by case a. Qed.

  Lemma is_mixed_handler_unfold a h r Ψ E' Φ Φ' :
    is_mixed_handler a h r Ψ E' Φ Φ' ⊣⊢ (
     (* Return branch. *)
     □ (∀ (v : val),
        Φ v -∗ MWP (r v) @ a <| E' |> {{ Φ' }}) ∧
  
     (* Effect branch. *)
     □ (∀ (v k : val),
        protocol_agreement v Ψ (λ w,
          MWP (k w) @ a <| E' |> {{ Φ' }}) -∗
        MWP (h v k) @ a <| E' |> {{ Φ' }}))%I.
  Proof. by case a. Qed.

End handler_judgment.


(** Reasoning rules. *)

(* Value, step, and bind rules. *)

Section reasoning_rules.
  Context `{!heapG Σ}.

  Lemma mwp_value a E Φ v : Φ v -∗ MWP (of_val v) @ a <| E |> {{ Φ }}.
  Proof. iIntros "HΦ". case a; by [iApply wp_value|iApply pwp_value]. Qed.

  Lemma mwp_pure_step a e e' E Φ :
    pure_prim_step e e' → 
      ▷ MWP e' @ a <| E |> {{ Φ }} -∗ MWP e @ a <| E |> {{ Φ }}.
  Proof.
    iIntros "% He'". case a;
    by [iApply wp_pure_step|iApply pwp_pure_step].
  Qed.

  Lemma mwp_bind K `{NeutralEctx K} a E Φ e e' :
    e' = fill K e  →
      MWP e @ a <| E |> {{ v, MWP fill K (of_val v) @ a <| E |> {{ Φ }} }} -∗
        MWP e' @ a <| E |> {{ Φ }}.
  Proof. iIntros (->) "He". case a; by [iApply wp_bind|iApply pwp_bind]. Qed.

  Lemma pwp_mwp a E Φ e :
    PWP e <| E |> {{ Φ }} -∗
      MWP e @ a <| E |> {{ Φ }}.
  Proof. iIntros "He". by case a; [iApply pwp_wp|]. Qed.

  Lemma mwp_wp a E Φ e :
    MWP e @ a <| E |> {{ Φ }} -∗
      WP e <| E |> {{ Φ }}.
  Proof. iIntros "He". by case a; [|iApply pwp_wp]. Qed.

  Lemma mwp_mono_att a a' E Φ e :
    le._att a a' →
      MWP e @ a <| E |> {{ Φ }} -∗
        MWP e @ a' <| E |> {{ Φ }}.
  Proof.
    intros Ha; case a, a'; iIntros "?"; try done.
    by iApply pwp_mwp.
  Qed.

  Lemma mwp_learn a E Φ e :
    (EffLabels E.*1 -∗ MWP e @ a <| E |> {{ Φ }}) -∗
      MWP e @ a<| E |> {{ Φ }}.
  Proof. iIntros "He"; case a; by iIntros "#HE"; iApply "He". Qed.

End reasoning_rules.


(** Tactics. *)

Ltac match_mwp_goal lemma tac :=
  match goal with
  | [ |- @bi_emp_valid _                (mwp_def _ ?e _ _) ] => tac lemma e
  | [ |- @environments.envs_entails _ _ (mwp_def _ ?e _ _) ] => tac lemma e
  end.

Ltac mwp_pure_step_lemma :=
  iApply mwp_pure_step.

Ltac mwp_bind_rule_lemma K :=
  iApply (mwp_bind K).

Ltac mwp_pure_step :=
  match_mwp_goal mwp_pure_step_lemma pure_step_tac.

Ltac mwp_bind_rule :=
  match_mwp_goal mwp_bind_rule_lemma bind_rule_tac.

Ltac mwp_value_or_step :=
  ((iApply mwp_value) || (mwp_bind_rule; mwp_pure_step));
  try iNext; simpl.

Ltac mwp_pure_steps :=
  repeat mwp_value_or_step.


(** Monotonicity. *)

Section monotonicity.
  Context `{!heapG Σ}.

  Lemma mwp_mono a E Φ Φ' e :
    □ (∀ v, Φ v -∗ Φ' v) -∗
      MWP e @ a <| E |> {{ Φ }} -∗
        MWP e @ a <| E |> {{ Φ' }}.
  Proof.
    iIntros "#HΦ He". case a; [|by iApply pwp_mono].
    iApply (wp_mono with "[] He").
    iIntros "!#" (v) "Hv !>"; by iApply "HΦ".
  Qed.

  Lemma mwp_mono_pure a E Φ Φ' e :
    (∀ v, Φ v -∗ Φ' v) -∗
      MWP e @ a <| [] |> {{ Φ }} -∗
        MWP e @ a <| E |> {{ Φ' }}.
  Proof.
    iIntros "HΦ He". case a.
    - iApply (wp_mono_pure with "[HΦ] He"). 
      by iIntros (v) "Hv"; iApply "HΦ".
    - by iApply (pwp_mono_pure with "HΦ He").
  Qed.

  Lemma mwp_eff_frame a E E' Φ e :
    E ⊆+ E' →
      MWP e @ a <| E |> {{ Φ }} -∗
        MWP e @ a <| E' |> {{ Φ }}.
  Proof.
    iIntros (Hsubm) "He". case a.
    - by iApply wp_eff_frame.
    - by iApply pwp_eff_frame.
  Qed.

  Corollary mwp_strong_mono a E E' Φ Φ' e :
    E ⊆+ E' →
      □ (∀ v, Φ v -∗ Φ' v) -∗
        MWP e @ a <| E |> {{ Φ }} -∗
          MWP e @ a <| E' |> {{ Φ' }}.
  Proof.
    iIntros (Hsub) "#HΦ He". case a.
    - iApply (wp_strong_mono with "[] He"); first done.
      iIntros "!#" (v) "Hv !>". by iApply "HΦ".
    - by iApply (pwp_strong_mono with "HΦ He").
  Qed.

  Lemma mwp_weaken a l E (Ψ Ψ' : pure_iEff Σ) Φ e :
    (Ψ ⊑ Ψ')%ieff -∗
      MWP e @ a <| (l, Ψ) :: E |> {{ Φ }} -∗
        MWP e @ a <| (l, Ψ') :: E |> {{ Φ }}.
  Proof.
    iIntros "#Hle He". case a.
    - by iApply (wp_weaken with "[] He").
    - by iApply (pwp_weaken with "[] He").
  Qed.

  Corollary mwp_dismiss a l E Ψ Φ e :
    MWP e @ a <| (l, ⊥) :: E |> {{ Φ }} -∗
      MWP e @ a <| (l, Ψ) :: E |> {{ Φ }}.
  Proof. iApply mwp_weaken. by iApply iEff_le_bottom. Qed.

End monotonicity.


(** Client side. *)

(* Reasoning rule for performing an effect. *)

Section effects.
  Context `{!heapG Σ}.

  Lemma mwp_perform a (l : eff_label) (v : val) E (Ψ : pure_iEff Σ) Φ :
    (l, Ψ) ∈ E →
      protocol_agreement v Ψ Φ -∗
        MWP (perform #l v) @ a <| E |> {{ Φ }}.
  Proof.
    iIntros (Hin) "Hprot". case a.
    - by iApply wp_perform.
    - by iApply pwp_perform.
  Qed.

End effects.


(** Handler. *)

(* Reasoning rule for installing shallow and deep handlers. *)

Section handler.
  Context `{!heapG Σ}.

  Lemma mwp_shandle a l E Ψ Ψ' Φ Φ' (client h r : val) :
    let E' := (l, Ψ') :: E in
    MWP client #() @ a <| (l, Ψ) :: E |> {{ Φ }} -∗
      is_mixed_shandler a h r l Ψ E E' Φ Φ' -∗
        MWP (shandle #l client h r) @ a <| E' |> {{ Φ' }}.
  Proof.
    intros E'.
    iIntros "Hclient Hhandler". case a.
    - by iApply (wp_shandle with "Hclient").
    - by iApply (pwp_shandle with "Hclient").
  Qed.

  Lemma mwp_handle a (l : eff_label) E Ψ Ψ' Φ Φ' (client h r : val) :
    MWP client #() @ a <| (l, Ψ) :: E |> {{ Φ }} -∗
      is_mixed_handler a h r Ψ ((l, Ψ') :: E) Φ Φ' -∗
        MWP (handle #l client h r) @ a <| (l, Ψ') :: E |> {{ Φ' }}.
  Proof.
    iIntros "Hclient Hhandler". case a.
    - by iApply (wp_handle with "Hclient").
    - by iApply (pwp_handle with "Hclient").
  Qed.

End handler.


(** Try Finally. *)

Section try_finally.
  Context `{!heapG Σ}.

  Lemma mwp_try_finally a (e₁ e₂ : val) E Φ :
    MWP e₁ #() @ a <| E |> {{ _, True }} -∗
      MWP e₂ #() @ a <| [] |> {{ Φ }} -∗
        MWP (try_finally e₁ e₂) @ a <| E |> {{ Φ }}.
  Proof. by case a; [iApply wp_try_finally|iApply pwp_try_finally]. Qed.

End try_finally.


(** Dynamic Wind *)

(* Reasoning rule for [dynamic_wind]. *)

Section dynamic_wind.
  Context `{!heapG Σ}.

  Lemma mwp_dynamic_wind a (pre body post : val) E Φ :
    □ MWP pre #() @ a <| [] |> {{ _, True }} -∗
      □ MWP post #() @ a <| [] |> {{ _, True }} -∗
        MWP body #() @ a <| E |> {{ Φ }} -∗
          MWP dynamic_wind pre body post @ a <| E |> {{ Φ }}.
  Proof. by case a; [iApply wp_dynamic_wind|iApply pwp_dynamic_wind]. Qed.

End dynamic_wind.


(** Adequacy. *)

(* We prove that if the assertion [MWP e @ a <|[]|> {{Φ}}]
   holds, then _e is safe_: it does not get stuck. *)

Section adequacy.
  Context `{!heapPreG Σ}.

  Lemma mwp_adequacy a e σ φ :
    (∀ `{!heapG Σ}, ⊢ MWP e @ a <|[]|> {{ v, ⌜φ v⌝ }}) →
    adequate NotStuck e σ (λ v _, φ v).
  Proof.
    intros Hmwp. apply eff_adequacy. intros.
    iPoseProof (mwp_wp $! (Hmwp heapG0) with "[]") as "Hwp".
    { rewrite /EffLabels /Eff //=.
      by iSplit; [iPureIntro; apply NoDup_nil|].
    }
    iApply (ewp_strong_mono with "Hwp"). done.
    { by iApply EFF_empty. }
    { by iIntros "!#" (?) "? !>". }
  Qed.

  Lemma mwp_not_stuck a e σ Φ :
    (∀ `{!heapG Σ}, ⊢ MWP e @ a <|[]|> {{ Φ }}) →
     ∀ e' σ',
       rtc erased_step ([e], σ) ([e'], σ') →
         not_stuck e' σ'.
  Proof.
    intros Hmwp ?? Hstep.
    assert (adequate NotStuck e σ (λ _ _, True)) as Hadequate.
    { apply (mwp_adequacy a)=>?.
      iApply mwp_mono; [|iApply Hmwp].
      by iIntros "!#" (?) "_".
    }
    eapply adequate_alt in Hadequate as [_ Hnot_stuck]; eauto.
    apply Hnot_stuck; auto.
    set_solver.
  Qed.

End adequacy.
