(* subst_map.v *)

(* This file is an adaptation of the theory [iris_heap_lang/metatheory.v] from
   the Iris project.
*)

From stdpp Require Import gmap.
From logic Require Export lang.
From lib Require Import maps_and_sets.


(** * Closed Expressions. *)

Fixpoint is_closed_expr (s : bsort) (X : list string) (e : expr) : bool :=
  match e with
  (* In alignment with the definition of the function [subst],
     we consider captured contexts and values as closed expressions. *)
  | Val _ | Eff _ _ => true
  | Var s' x => if decide (s = s') then bool_decide (x ∈ X) else true
  | Rec s' f x e =>
     if decide (s = s') then
       is_closed_expr s (f :b: x :b: X) e
     else
       is_closed_expr s X e
  | UnOp _ e | Fst e | Snd e | InjL e | InjR e | Alloc e | Load e | Do e =>
     is_closed_expr s X e
  | App e1 e2 | BinOp _ e1 e2 | Pair e1 e2 | Store e1 e2 =>
     is_closed_expr s X e1 && is_closed_expr s X e2
  | If e0 e1 e2 | Case e0 e1 e2 | TryWith e0 e1 e2 =>
     is_closed_expr s X e0 && is_closed_expr s X e1 && is_closed_expr s X e2
  end.

Lemma is_closed_weaken s X Y e : is_closed_expr s X e → X ⊆ Y → is_closed_expr s Y e.
Proof. revert s X Y; induction e; try (naive_solver (eauto; set_solver));
       (by destruct s0, s; naive_solver (eauto; set_solver)).
Qed.

Lemma is_closed_weaken_nil s X e : is_closed_expr s [] e → is_closed_expr s X e.
Proof. intros. by apply is_closed_weaken with [], list_subseteq_nil. Qed.


(** * Substitution. *)

Lemma subst_is_closed s X e x es : is_closed_expr s X e → x ∉ X → subst s x es e = e.
Proof.
  revert s X. induction e=> s' X /=; rewrite ?bool_decide_spec ?andb_True=> ??;
    repeat case_decide; simplify_eq/=; f_equal; intuition eauto with set_solver.
Qed.

Lemma subst_is_closed_nil s e x v : is_closed_expr s [] e → subst s x v e = e.
Proof. intros. apply subst_is_closed with []; set_solver. Qed.

Fixpoint subst_map (s : bsort) (vs : gmap string val) (e : expr) : expr :=
  match e with
  | Val _ => e
  | Var s' y =>
      if decide (s = s') then (if vs !! y is Some v then Val v else e) else e
  | Do e => Do (subst_map s vs e)
  | Eff _ _ => e
  | TryWith e1 e2 e3 =>
     TryWith (subst_map s vs e1) (subst_map s vs e2) (subst_map s vs e3)
  | Rec s' f y e =>
      if decide (s = s') then
        Rec s' f y (subst_map s (binder_delete y (binder_delete f vs)) e)
      else
        Rec s' f y (subst_map s vs e)
  | App e1 e2 => App (subst_map s vs e1) (subst_map s vs e2)
  | UnOp op e => UnOp op (subst_map s vs e)
  | BinOp op e1 e2 => BinOp op (subst_map s vs e1) (subst_map s vs e2)
  | If e0 e1 e2 => If (subst_map s vs e0) (subst_map s vs e1) (subst_map s vs e2)
  | Pair e1 e2 => Pair (subst_map s vs e1) (subst_map s vs e2)
  | Fst e => Fst (subst_map s vs e)
  | Snd e => Snd (subst_map s vs e)
  | InjL e => InjL (subst_map s vs e)
  | InjR e => InjR (subst_map s vs e)
  | Case e0 e1 e2 => Case (subst_map s vs e0) (subst_map s vs e1) (subst_map s vs e2)
  | Alloc e => Alloc (subst_map s vs e)
  | Load e => Load (subst_map s vs e)
  | Store e1 e2 => Store (subst_map s vs e1) (subst_map s vs e2)
  end.

Lemma subst_map_empty s e : subst_map s ∅ e = e.
Proof.
  assert (∀ x, binder_delete x (∅:gmap _ val) = ∅) as Hdel.
  { intros [|x]; by rewrite /= ?delete_empty. }
  induction e; simplify_map_eq; rewrite ?Hdel; auto with f_equal.
  - by case_decide.
  - by case_decide; rewrite IHe.
Qed.
Lemma subst_map_insert s x v vs e :
  subst_map s (<[x:=v]>vs) e = subst s x v (subst_map s (delete x vs) e).
Proof.
  revert vs. induction e=> vs; simplify_map_eq; auto with f_equal.
  - repeat match goal with
    | |- context [ <[?x:=_]> _ !! ?y ] =>
       destruct (decide (x = y)); simplify_map_eq=> //
    | |- context [ decide _ ] =>
       case_decide; simplify_eq; simpl
    end; try naive_solver;
    case (vs !! _); simplify_option_eq; try naive_solver.
  - case (decide (s = s0)) as [Heq|Hneq]; simplify_eq; simpl.
    + rewrite decide_True; [|done].
      destruct (decide _) as [[??]|[<-%dec_stable|[<-%dec_stable ?]]%not_and_l_alt].
      * rewrite !binder_delete_insert // !binder_delete_delete; eauto with f_equal.
      * by rewrite /= delete_insert_delete delete_idemp.
      * by rewrite /= binder_delete_insert // delete_insert_delete
        !binder_delete_delete delete_idemp.
    + rewrite decide_False; [|done]. by rewrite IHe.
Qed.

Lemma subst_map_delete_subst s x v vs e :
  subst_map s (delete x vs) (subst s x v e) = subst s x v (subst_map s (delete x vs) e).
Proof.
  revert vs. induction e=> vs; simplify_map_eq; auto with f_equal.
  - match goal with
    | |- context [ decide (?x = ?y) ] =>
       case (decide (x = y)) as [?|?]; simplify_map_eq=> //
    end.
    case (decide (x = x0)) as [Heq|Hneq].
    + by rewrite Heq lookup_delete //= !decide_True.
    + rewrite !lookup_delete_ne //= decide_True // lookup_delete_ne //.
      by case (vs !! x0); simplify_option_eq.
  - case (decide (s = s0)) as [Heq|Hneq]; simplify_eq; simpl.
    + rewrite (@decide_True _ (s0 = s0)); [|done].
      destruct (decide _) as [[??]|[<-%dec_stable|[<-%dec_stable ?]]%not_and_l_alt];
      by rewrite !binder_delete_delete; eauto with f_equal.
    + rewrite decide_False; [|done]. by rewrite IHe.
Qed.
Lemma subst_map_singleton s x v e :
  subst_map s {[x:=v]} e = subst s x v e.
Proof. by rewrite subst_map_insert delete_empty subst_map_empty. Qed.

Lemma subst_map_union s vs vs' e :
  vs ##ₘ vs' →
    subst_map s (vs ∪ vs') e = subst_map s vs (subst_map s vs' e).
Proof.
  revert vs'. induction vs as [|x v vs Hlkp IH] using map_ind;
  intros vs' Hdisj.
  - by rewrite map_empty_union subst_map_empty.
  - rewrite subst_map_insert -subst_map_delete_subst.
    rewrite delete_notin; [|done].
    revert Hdisj. rewrite map_disjoint_insert_l.
    intros [Hlkp' Hdisj].
    rewrite -(delete_notin vs' x); [|done].
    rewrite -subst_map_insert delete_notin; [|done].
    rewrite -insert_union_l insert_union_r; [|done].
    apply IH. by rewrite map_disjoint_insert_r.
Qed.

Lemma subst_map_binder_insert s b v vs e :
  subst_map s (binder_insert b v vs) e =
  subst' s b v (subst_map s (binder_delete b vs) e).
Proof. destruct b; rewrite ?subst_map_insert //. Qed.
Lemma subst_map_binder_insert_empty s b v e :
  subst_map s (binder_insert b v ∅) e = subst' s b v e.
Proof. by rewrite subst_map_binder_insert binder_delete_empty subst_map_empty. Qed.

Lemma subst_subst s x v1 v2 e :
  subst s x v1 (subst s x v2 e) = subst s x v2 e.
Proof.
  induction e; simpl;
  repeat match goal with
    | [ Heq : _ = _ |- _ ] => rewrite Heq
  end; try done.
  - by repeat case_decide; simpl; repeat case_decide.
  - by repeat case_decide; simpl; repeat case_decide; try rewrite IHe.
Qed.

Lemma subst_subst_ne b s1 s2 v1 v2 e :
  s1 ≠ s2 →
    subst b s1 v1 (subst b s2 v2 e) = subst b s2 v2 (subst b s1 v1 e).
Proof.
  intros ?. induction e; simpl;
  repeat match goal with
    | [ Heq : _ = _ |- _ ] => rewrite Heq
  end; try done.
  - by repeat case_decide; simpl; repeat case_decide; try naive_solver.
  - by repeat case_decide; simpl; repeat case_decide; try rewrite IHe;
    try naive_solver.
Qed.

Lemma subst_map_binder_insert_2 s b1 v1 b2 v2 vs e :
  subst_map s (binder_insert b1 v1 (binder_insert b2 v2 vs)) e =
  subst' s b2 v2 (subst' s b1 v1 (subst_map s (binder_delete b2 (binder_delete b1 vs)) e)).
Proof.
  destruct b1 as [|s1], b2 as [|s2]=> /=; auto using subst_map_insert.
  rewrite subst_map_insert. destruct (decide (s1 = s2)) as [->|].
  - by rewrite delete_idemp subst_subst delete_insert_delete.
  - by rewrite delete_insert_ne // subst_map_insert subst_subst_ne.
Qed.
Lemma subst_map_binder_insert_2_empty s b1 v1 b2 v2 e :
  subst_map s (binder_insert b1 v1 (binder_insert b2 v2 ∅)) e =
  subst' s b2 v2 (subst' s b1 v1 e).
Proof.
  by rewrite subst_map_binder_insert_2 !binder_delete_empty subst_map_empty.
Qed.

(* [subst s] commutes with [subst_map s'] if [s ≠ s']. *)
Lemma subst_subst_map x v vs e :
    subst L x v (subst_map V vs e) = subst_map V vs (subst L x v e).
Proof.
  revert vs; induction e; intros vs; simpl; simpl; try naive_solver;
  repeat match goal with
    | [ Heq : ∀_, _ = _ |- _ ] => rewrite (Heq vs)
  end; try done.
  - destruct s; simpl.
    * by case (vs !! x0).
    * by case (decide (x = x0)); [intros ->|intros ?]; simpl.
  - destruct s; simpl.
    * by rewrite IHe.
    * case_decide; [|done]. by rewrite IHe.
Qed.

(** * Map Substitution on Closed Expressions. *)

Lemma subst_map_is_closed s X e vs :
  is_closed_expr s X e →
  (∀ x, x ∈ X → vs !! x = None) →
  subst_map s vs e = e.
Proof.
  revert X vs. assert (∀ x x1 x2 X (vs : gmap string val),
    (∀ x, x ∈ X → vs !! x = None) →
    x ∈ x2 :b: x1 :b: X →
    binder_delete x1 (binder_delete x2 vs) !! x = None).
  { intros x x1 x2 X vs ??. rewrite !lookup_binder_delete_None. set_solver. }
  induction e=> X vs /= ? HX; repeat case_match; naive_solver eauto with f_equal.
Qed.

Lemma subst_map_is_closed_nil s e vs : is_closed_expr s [] e → subst_map s vs e = e.
Proof. intros. apply subst_map_is_closed with []; set_solver. Qed.

Lemma is_closed_subst_map_var s x vs :
  is_Some (vs !! x) → is_closed_expr s [] (subst_map s vs (Var s x)).
Proof. by simpl; destruct 1 as [v ->]; rewrite decide_True //. Qed.


Global Instance Permutation_is_closed_expr s :
  Proper (Permutation ==> eq ==> eq) (is_closed_expr s).
Proof.
  intros xs ys Hp ? e ->. revert xs ys Hp.
  induction e; intros xs ys Hp; simpl; try done;
  try repeat match goal with
  | [ E : ∀ _ _ _, _ = _  |- _ ] =>
    rewrite (E xs ys Hp) end;
  try done; try (case_decide; [|done]).
  - by apply bool_decide_ext, elem_of_Permutation_proper.
  - apply IHe.
    destruct f, x; simpl;
    by try repeat apply Permutation_cons.
Qed.


(** * All-Targeting Substitution. *)

(* [subst_all s [] v e] substitutes all open [s]-sorted variables of [e] with [v]. *)
Fixpoint subst_all (s : bsort) (X : list string) (v : val) (e : expr) : expr :=
  match e with
  | Val _ => e
  | Var s' x => if decide (s = s') then (if decide (x ∈ X) then e else Val v) else e
  | Do e => Do (subst_all s X v e)
  | Eff _ _ => e
  | TryWith e1 e2 e3 =>
     TryWith (subst_all s X v e1) (subst_all s X v e2) (subst_all s X v e3)
  | Rec s' f x e =>
      if decide (s = s') then
        Rec s' f x (subst_all s (f :b: x :b: X) v e)
      else
        Rec s' f x (subst_all s X v e)
  | App e1 e2 => App (subst_all s X v e1) (subst_all s X v e2)
  | UnOp op e => UnOp op (subst_all s X v e)
  | BinOp op e1 e2 => BinOp op (subst_all s X v e1) (subst_all s X v e2)
  | If e0 e1 e2 => If (subst_all s X v e0) (subst_all s X v e1) (subst_all s X v e2)
  | Pair e1 e2 => Pair (subst_all s X v e1) (subst_all s X v e2)
  | Fst e => Fst (subst_all s X v e)
  | Snd e => Snd (subst_all s X v e)
  | InjL e => InjL (subst_all s X v e)
  | InjR e => InjR (subst_all s X v e)
  | Case e0 e1 e2 => Case (subst_all s X v e0) (subst_all s X v e1) (subst_all s X v e2)
  | Alloc e => Alloc (subst_all s X v e)
  | Load e => Load (subst_all s X v e)
  | Store e1 e2 => Store (subst_all s X v e1) (subst_all s X v e2)
  end.

Lemma subst_all_lookup_toal {A} `{EqDecision A, Inhabited A}
  s (f : A → val) (vs : gmap string A) x :
  subst_all s [] (f inhabitant) (subst_map s (f <$> vs) (Var s x)) =
    Val (f (vs !!! x)).
Proof.
  case (decide (x ∈ dom vs)) as [Hin|Hnotin]; simpl.
  - rewrite lookup_lookup_total_dom; [|set_solver].
    by rewrite decide_True //= lookup_total_fmap.
  - revert Hnotin. rewrite not_elem_of_dom. intro Hlkp.
    rewrite lookup_fmap lookup_total_alt Hlkp //=.
    by repeat rewrite !decide_True //=.
Qed.

Lemma subst_subst_all_1 s x xv X v e :
  x ∈ X →
    subst s x xv (subst_all s X v e) =
      subst_all s X v (subst s x xv e).
Proof.
  revert X.
  induction e; intros X Hin; simpl;
  try repeat match goal with
  | [ Heq : ∀ _ _, _ = _  |- _ ] =>
    rewrite (Heq X Hin) end; try done;
  repeat case_decide; simpl;
  repeat case_decide; try naive_solver;
  try rewrite IHe //.
  set_solver.
Qed.

Lemma subst_subst_all_2 s s' x xv X v e :
  s ≠ s' →
    subst s x xv (subst_all s' X v e) =
      subst_all s' X v (subst s x xv e).
Proof.
  intro Hss'; revert X; induction e; intro X; simpl; destruct s, s';
  try repeat match goal with
  | [ Heq : ∀ _, _ = _  |- _ ] =>
    rewrite (Heq X) end; try done;
  repeat case_decide; simplify_eq; simpl;
  repeat case_decide; simplify_eq; simpl; try done;
  try rewrite IHe; done.
Qed.

Lemma subst_subst_all_3 s s' x xv X v e :
  s ≠ s' →
    subst' s x xv (subst_all s' X v e) =
      subst_all s' X v (subst' s x xv e).
Proof. by case x as [|y]; last apply subst_subst_all_2. Qed.

Lemma subst_all_equiv s X Y v e :
  (∀ x, x ∈ X ↔ x ∈ Y) →
    subst_all s X v e = subst_all s Y v e.
Proof.
  revert X Y. induction e; intros X Y Hequiv; simpl;
  try repeat match goal with
  | [ Heq : ∀ _ _ _, _ = _  |- _ ] =>
    rewrite (Heq X Y Hequiv) end; try done.
  - by rewrite (decide_ext (x ∈ X) (x ∈ Y)) //.
  - rewrite (IHe (f :b: x :b: X) (f :b: x :b: Y)) // =>?.
    by set_solver.
Qed.

Lemma elem_of_cons_delete {A} X i (x : A) :
  X !! i = Some x →
    (∀ y, y ∈ X ↔ y ∈ x :: (delete i X)).
Proof.
  intros Hlkp y.
  rewrite -{-1}(take_drop_middle _ _ _ Hlkp).
  assert (length (take i X) = i) as Hi.
  { by apply take_length_le, Nat.lt_le_incl, lookup_lt_is_Some_1. }
  rewrite -{1}Hi delete_middle.
  rewrite -{1}(take_drop_middle _ _ _ Hlkp).
  by set_solver.
Qed.

Lemma elem_of_cons_duplicate {A} X (x : A) :
  x ∈ X →
    (∀ y, y ∈ X ↔ y ∈ x :: X).
Proof. intros Hin y. by set_solver. Qed.

Lemma subst_all_delete s i x xv X v e :
  X !! i = Some x →
    subst_all s X v (subst s x xv e) =
      subst_all s (delete i X) v (subst s x xv e).
Proof.
  revert X i. induction e; intros X i Hlkp; simpl;
  try repeat match goal with
  | [ Heq : ∀ _ _ _, _ = _  |- _ ] =>
    rewrite (Heq X i Hlkp) end; try done.
  - case (decide (s = s0)) as [Heq|Hneq].
    + case_decide. done. simpl.
      rewrite (decide_ext (x0 ∈ X) (x0 ∈ delete i X)) //.
      rewrite (elem_of_cons_delete _ _ _ Hlkp).
      set_solver.
    + by rewrite //= !decide_False //.
  - case (decide (s = s0)) as [Heq|Hneq].
    + rewrite -!Heq. f_equal.
      destruct (decide _) as [[??]|[<-%dec_stable|[<-%dec_stable ?]]%not_and_l_alt].
      * case f, x0; simpl; first by apply (IHe X i).
        apply (IHe (_ :: X) (S i)). done.
        apply (IHe (_ :: X) (S i)). done.
        apply (IHe (_ :: _ :: X) (S (S i))). done.
      * apply subst_all_equiv. intros y.
        case x0 as [|w]; simpl;
        last rewrite !(Permutation_swap w x) !(elem_of_cons _ _ w);
        rewrite -(elem_of_cons_delete _ _ _ Hlkp);
        rewrite (elem_of_cons_duplicate X x) //;
        by apply (elem_of_list_lookup_2 _ i).
      * apply subst_all_equiv. intros y.
        case f as [|w]; simpl;
        last rewrite !(elem_of_cons _ _ w);
        rewrite -(elem_of_cons_delete _ _ _ Hlkp);
        rewrite (elem_of_cons_duplicate X x) //;
        by apply (elem_of_list_lookup_2 _ i).
    + by rewrite (IHe X i).
Qed.

