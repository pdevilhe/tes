(* fundamental_le.v *)

(* This file contains the proof of the Fundamental Theorem of the
   weakening relation. *)

From effect_system Require Import compatibility_le.

Section fundamental_le.
  Context `{!heapG Σ}.

  Theorem fundamental_le_ty {D α α'}
       (Hty:  D ⊢ α ≤T α') : ⊢ D ⊨ α ≤T α'

     with fundamental_le_row {D b ρ ρ'}
       (Hrow: D ⊢ ρ ≤R ρ' @ b) : ⊢ D ⊨ ρ ≤R ρ' @ b

     with fundamental_le_eff_sig {D σ σ'}
       (Hsig: D ⊢ σ ≤S σ') : ⊢ D ⊨ σ ≤S σ'.

  Proof.
    - destruct Hty.
      + by iApply TRefl_sem_le_ty.
      + by iApply TBot_sem_le_ty.
      + by iApply TTop_sem_le_ty.
      + iApply TArrow_sem_le_ty; first done.
        * by iApply fundamental_le_ty.
        * by iApply fundamental_le_ty.
        * by iApply fundamental_le_row.
      + by iApply TTrans_sem_le_ty; iApply fundamental_le_ty.

    - destruct Hrow.
      + by iApply RNil_sem_le_row.
      + by iApply RCons_sem_le_row.
      + by iApply RSwap_sem_le_row.
      + iApply RSkip_sem_le_row; try done.
        * by iApply fundamental_le_eff_sig.
        * by iApply fundamental_le_row.
      + by iApply RAbs_sem_le_row.
      + by iApply RTrans_sem_le_row; iApply fundamental_le_row.

    - destruct Hsig.
      + by iApply SConc_sem_le_eff_sig; iApply fundamental_le_ty.
      + by iApply SAbst_sem_le_eff_sig.
  Qed.

End fundamental_le.
