(* rules.v *)

(* This file contains the formal definition of the type-and-effect system TES.
   More specifically, we define an inductive predicate [typed] corresponding to
   the typing judgment of the system. Each constructor of this predicate
   corresponds to a typing rule of the system.
*)

From stdpp Require Export gmultiset gmap stringmap.
From logic Require Export deep_handler labeled_effects.
From effect_system Require Export notation.
From lib Require Import maps_and_sets lists.

(** * Auxiliary Definitions. *)

(** Unboxed Types, Binary Operators, and Unary Operators. *)

(* The following definitions are taken from the Iris tutorial @POPL'20:

       https://gitlab.mpi-sws.org/iris/tutorial-popl20

   They enable a concise statement of the typing rules of both binary and
   unary operators.
*)

Section unboxed_bin_un.

  Inductive ty_unboxed : ty → Prop :=
    | TUnit_unboxed : ty_unboxed TUnit
    | TBool_unboxed : ty_unboxed TBool
    | TInt_unboxed : ty_unboxed TInt.
  Existing Class ty_unboxed.
  Existing Instances TUnit_unboxed TBool_unboxed TInt_unboxed.

  Inductive ty_un_op : un_op → ty → ty → Prop :=
    | Ty_un_op_int op : ty_un_op op TInt TInt
    | Ty_un_op_bool : ty_un_op NegOp TBool TBool.
  Existing Class ty_un_op.
  Existing Instances Ty_un_op_int Ty_un_op_bool.

  Inductive ty_bin_op : bin_op → ty → ty → ty → Prop :=
    | Ty_bin_op_eq α :
       ty_unboxed α → ty_bin_op EqOp α α TBool
    | Ty_bin_op_arith op :
       TCElemOf op [PlusOp; MinusOp; MultOp; QuotOp; RemOp;
                    AndOp; OrOp; XorOp; ShiftLOp; ShiftROp] →
       ty_bin_op op TInt TInt TInt
    | Ty_bin_op_compare op :
       TCElemOf op [LeOp; LtOp] → ty_bin_op op TInt TInt TBool
    | Ty_bin_op_bool op :
       TCElemOf op [AndOp; OrOp; XorOp] → ty_bin_op op TBool TBool TBool.
  Existing Class ty_bin_op.
  Existing Instances Ty_bin_op_eq Ty_bin_op_arith Ty_bin_op_compare
    Ty_bin_op_bool.

End unboxed_bin_un.


(** Variables. *)

(* There are three sorts of variables in the syntax of types: (1) type
   variables, (2) row variables, and (3) effect names. An effect name is 
   the string attached to an effect signature (that is, a term of type
   [eff_sig]). Here, we define methods for computing the set of effect names
   that appear in a type, in a row, or in an environment.
*)

Module vars.

  Definition _eff_sig (f : ty → gset eff_name) (σ : eff_sig) : gset eff_name :=
    match σ with
    | ESig s α β =>
        {[ s ]} ∪ (f α) ∪ (f β)
    | RVar _ =>
        (∅ : gset string)
    end.

  Definition _row_pre (f : ty → gset eff_name) (ρ : row eff_sig) : gset eff_name :=
    ⋃ ((_eff_sig f) <$> ρ).

  Fixpoint _ty (α : ty) : gset eff_name :=
    match α with
    | TTop
    | TBot
    | TUnit
    | TBool
    | TInt
    | TVar _ =>
        (∅ : gset eff_name)
    | TProd α β
    | TSum α β =>
        (_ty α) ∪ (_ty β)
    | TRef α
    | TList α
    | TForall _ α =>
        (_ty α)
    | TArr a α ρ β =>
        (_ty α) ∪ (_row_pre _ty ρ) ∪ (_ty β)
    end.
  
  Definition _row : row eff_sig → gset eff_name :=
    _row_pre _ty.
  
  Definition _env (Γ : gmap eff_name ty) : gset eff_name :=
    ⋃ ((λ '(_, α), _ty α) <$> (map_to_list Γ)).

  (* The assertion [fresh s Γ ρ α] holds if the string [s] is free in
     [ρ], [α], and [Γ], when seen as an effect name, and it must also not
     appear in the domain of [Γ]. *)
  Definition _fresh s (Γ : stringmap ty) ρ α :=
    s ∉ dom Γ       ∧
    s ∉ vars._env Γ ∧
    s ∉ vars._row ρ ∧
    s ∉ vars._ty α.

End vars.

Section vars_properties.

  (* The function [lift_to_binder] lifts a predicate on strings into
     a predicate on binders. *)
  Definition lift_to_binder (P : (eff_name → Prop)) (b : binder) : Prop :=
    match b with BNamed s => P s | BAnon => True end.

  Lemma binder_delete_notin {A} `{EqDecision A}
        (x : binder) (m : gmap eff_name A) :
    lift_to_binder (.∉ (dom m)) x →
      binder_delete x m = m.
  Proof.
    destruct x; simpl; [done|].
    rewrite not_elem_of_dom.
    by apply delete_notin.
  Qed.

  Lemma vars_env_insert s x α Γ :
    Γ !! x = None →
      s ∉ vars._env (<[x:=α]> Γ) →
        s ∉ vars._ty α ∧ s ∉ vars._env Γ.
  Proof.
    intros Hlkp.
    by rewrite /vars._env (map_to_list_insert Γ _ _ Hlkp) //=
               not_elem_of_union.
  Qed.

  Lemma vars_ty_prod s α β :
    s ∉ vars._ty (α * β)%ty ↔
      s ∉ vars._ty α ∧ s ∉ vars._ty β.
  Proof. set_solver. Qed.

End vars_properties.


(** * Weakening Relation. *)

(** Definitions. *)

(* We define a weakening relation on types, rows, and signatures. *)

Module le.

  (* Weakening relation on purity attributes. *)
  Definition _att : att → att → Prop :=
    λ a a',
      match a, a' with
      | I, P =>
         False
      | _, _ =>
         True
      end.

  (* A _disjointness context_ is a map that associates an effect name [s]
     to a pair of a set of effect names [ss] and a set of row variables [js].
     This context contains the information that the dynamic instance of [s]
     does not belong to instances of [ss], and that it does not belong to any
     of the set of dynamic instances abstracted by a row variable in [js]. *)
  Definition disj_ctx : Type := gmap eff_name (gmultiset eff_name * gset nat).

  (* [merge_ctx D D'] merges [D] and [D'] by performing the key-wise
     union of effect name sets and row-variable sets. *)
  Definition merge_ctx : disj_ctx → disj_ctx → disj_ctx :=
    union_with (λ '(ss, js) '(rs, ks), Some ((ss ∪ rs), (js ∪ ks))).

  (* [disj_ctx_included D D'] holds if [D] stores less disjointness
     information than [D']. More precisely, for every set of effect names [ss],
     set of row variables [js], and effect name [s], if [D] associates [s] to
     the pair [(ss, js)], then [D'] associates [s] to a pair [(rs, ks)] such
     that [ss ⊆ rs] and [js ⊆ ks]. *)
  Definition disj_ctx_included : relation disj_ctx :=
    map_included (λ '(ss, js) '(rs, ks), ss ⊆ rs ∧ js ⊆ ks).

  (* [conc_sigs ρ] computes the (multi)set of effect names appearing in [ρ]. *)
  Definition conc_sigs : row eff_sig → gmultiset string :=
    fix go ρ :=
      match ρ with
      | [] => ∅
      | ESig s _ _ :: ρ => {[+ s +]} ⊎ go ρ
      | RVar _ :: ρ => go ρ
      end.

  (* [abst_sigs ρ] computes the set of row variables appearing in [ρ]. *)
  Definition abst_sigs : row eff_sig → gset nat :=
    fix go ρ :=
      match ρ with
      | [] => ∅
      | RVar i :: ρ => {[i]} ∪ go ρ
      | _ :: ρ => go ρ
      end.

  (* The function [row_to_disj_ctx ρ'] builds a disjointness context by
     exploiting the assumption that there is no aliasing among the dynamic
     labels associated to a row [ρ']. *)
  Definition row_to_disj_ctx (ρ' : row eff_sig) : disj_ctx :=
    set_to_map (λ s,
      (s, (conc_sigs ρ' ∖ {[+ s +]}, abst_sigs ρ'))
    ) (dom (conc_sigs ρ')).

  (* When deducing the weakening relation between two arrow types,
     we can update the disjointness context with the information
     we learn from the assumption that a row is the disjoint union
     of signatures. *)
  Definition update_disj_ctx : row eff_sig → disj_ctx → disj_ctx :=
    λ ρ' D, merge_ctx (row_to_disj_ctx ρ') D.

  (* Weakening relation on effect signatures. *)
  Inductive _eff_sig :
    disj_ctx → eff_sig → eff_sig → Prop :=

  (* Concrete signature. *)
  | SConc_le D s α β α' β' :

      _ty D α α' →
        _ty D β' β →
          _eff_sig D (ESig s α β) (ESig s α' β')

  (* Abstract signature. *)
  | SAbst_le D i :

      _eff_sig D (RVar i) (RVar i)


  (* Weakening relation on rows. *)
  with _row :
    disj_ctx → bool → row eff_sig → row eff_sig → Prop :=

  (* Empty row. *)
  | RNil_le D b :

      _row D b [] []

  (* Row extension. *)
  | RCons_le D b ρ σ :

      _row D b ρ (σ :: ρ)

  (* Permutation of signatures in a row. *)
  | RSwap_le D b ρ σ σ' :

      _row D b (σ :: σ' :: ρ) (σ' :: σ :: ρ)

  (* Weakening of the tail of rows. *)
  | RSkip_le D b ρ ρ' σ σ' :

      _eff_sig D σ σ' →
        _row D false ρ ρ' →
          _row D b (σ :: ρ) (σ' :: ρ')

  (* Erasure of an Abs signature. *)
  | RAbs_le D s ρ ss js :

      D !! s = Some (ss, js) →
        conc_sigs ρ ⊆ ss →
          abst_sigs ρ ⊆ js →
            _row D true ((EAbs s) :: ρ) ρ

  (* Transitivity. *)
  | RTrans_le D b ρ ρ' ρ'' :

      _row D b ρ ρ' →
        _row D b ρ' ρ'' →
          _row D b ρ ρ''


  (* Weakening relation on types. *)
  with _ty : disj_ctx → ty → ty → Prop :=

  (* Reflexivity. *)
  | TRefl_le D α :

      _ty D α α

  (* Bottom. *)
  | TBot_le D α :

      _ty D ⊥ α

  (* Top. *)
  | TTop_le D α :

      _ty D α ⊤

  (* Arrow. *)
  | TArrow_le D b α α' a a' ρ ρ' β β' :

      let D' := update_disj_ctx ρ' D in

      _att a a' →
        _ty D' α' α →
          _ty D' β β' →
            _row D' b ρ ρ' →
              _ty D (α -{a; ρ}-> β) (α' -{a'; ρ'}-> β')

  (* Transitivity. *)
  | TTrans_le D α α' α'' :

      _ty D α α' →
        _ty D α' α'' →
          _ty D α α''.

End le.

Notation "a '≤A' a'" := (le._att a a')
  (at level 74, a' at next level).
Notation "D ⊢ α ≤T α'" := (le._ty D α α')
  (at level 74, α, α' at next level).
Notation "D ⊢ ρ ≤R ρ' @ b" := (le._row D b ρ ρ')
  (at level 74, ρ, ρ', b at next level).
Notation "D ⊢ σ ≤S σ'" := (le._eff_sig D σ σ')
  (at level 74, σ, σ' at next level).


(** Properties. *)

(* We prove properties about the weakening relation
   and its related definitions. *)

Section le_properties.

  (* An effect signature [e] is either concrete or abstract. *)
  Lemma elem_of_row e ρ :
    e ∈ ρ →
      (∃ s α β, e = ESig s α β ∧ s ∈ le.conc_sigs ρ) ∨
      (∃ i,     e = RVar i     ∧ i ∈ le.abst_sigs ρ).
  Proof.
    induction ρ; [by inversion 1|].
    rewrite elem_of_cons. intros [<-|He].
    - by case e as [s α β|i]; [left|right]; set_solver.
    - destruct (IHρ He) as [[s [α [β [-> Hs]]]]|[i [-> Hi]]];
      [left|right]; [exists s, α, β| exists i];
      try case a as [???|?]; set_solver.
  Qed.

  (* A concrete signature has the form [ESig s α β]
     for some [α] and [β]. *)
  Lemma elem_of_conc_sigs_iff s ρ' :
    s ∈ le.conc_sigs ρ' ↔ ∃ α β, ESig s α β ∈ ρ'.
  Proof.
    induction ρ' as [|e ρ'].
    - by split; [intros Hin|intros [?[? Hin]]]; inversion Hin.
    - by case e as [s' α β|i]; simpl; set_solver.
  Qed.
  Corollary elem_of_conc_sigs s ρ' :
    s ∈ le.conc_sigs ρ' → ∃ α β, ESig s α β ∈ ρ'.
  Proof. by rewrite elem_of_conc_sigs_iff. Qed.

  (* An abstract signature has the form [RVar i] for some [i]. *)
  Lemma elem_of_abst_sigs_iff i ρ' : i ∈ le.abst_sigs ρ' ↔ RVar i ∈ ρ'.
  Proof.
    induction ρ' as [|e ρ'].
    - by split; inversion 1.
    - by case e as [s' α β|j]; simpl; set_solver.
  Qed.
  Corollary elem_of_abst_sigs i ρ' : i ∈ le.abst_sigs ρ' → RVar i ∈ ρ'.
  Proof. by rewrite elem_of_abst_sigs_iff. Qed.

  (* The domain of the disjointness context obtained from [ρ'] is
     the set of effect names appearing in concrete signatures of [ρ'] *)
  Lemma dom_row_to_disj_ctx ρ' :
    dom (le.row_to_disj_ctx ρ') = dom (le.conc_sigs ρ').
  Proof.
    rewrite /le.row_to_disj_ctx. apply set_eq.
    intros s. rewrite elem_of_dom. split.
    - intros [(ss, js) Hlkp]. revert Hlkp.
      rewrite lookup_set_to_map; [|by auto].
      intros [s' [Hs' Heq]].
      by injection Heq as ->.
    - intros Hin.
      exists (le.conc_sigs ρ' ∖ {[+ s +]}, le.abst_sigs ρ').
      rewrite lookup_set_to_map; [|by auto].
      by exists s.
  Qed.

  (* The domain of the union of disjointness contexts [D] and [D'] is
     the union of the domains of [D] and [D']. *)
  Lemma dom_merge_ctx D D' :
    dom (le.merge_ctx D D') = (dom D) ∪ (dom D').
  Proof.
    rewrite /le.merge_ctx. apply set_eq ; intros s; split.
    - by apply dom_union_with_subseteq.
    - apply subseteq_dom_union_with.
      by intros (?,?) (?,?).
  Qed.

  (* The disjointness context obtained from a row [ρ'] associates an effect
     name [s] to the pair [(le.conc_sigs ρ' ∖ {[+ s +]}, le.abst_sigs ρ')]. *)
  Lemma lookup_row_to_disj_ctx ρ' s ss js :
    le.row_to_disj_ctx ρ' !! s = Some (ss, js) →
        s ∈ dom (le.conc_sigs ρ') ∧
        ss = le.conc_sigs ρ' ∖ {[+ s +]}   ∧
        js = le.abst_sigs ρ'.
  Proof.
    rewrite /le.row_to_disj_ctx lookup_set_to_map; [|auto].
    intros [s' [Hs' Heq]]. by injection Heq as ->.
  Qed.

  (* Another formulation of the property that [le.row_to_disj_ctx ρ']
     associates a key [s] to the pair [(le.conc_sigs ρ' ∖ {[+ s +]},
     le.abst_sigs ρ')]. *)
  Lemma elem_of_dom_conc_sigs ρ' s :
    s ∈ dom (le.conc_sigs ρ') →
      le.row_to_disj_ctx ρ' !! s =
        Some (le.conc_sigs ρ' ∖ {[+ s +]}, le.abst_sigs ρ').
  Proof.
    intros Hin.
    assert (is_Some ((le.row_to_disj_ctx ρ') !! s))
      as [(ss, js) Hlkp].
    { by rewrite -elem_of_dom dom_row_to_disj_ctx. }
    by destruct (lookup_row_to_disj_ctx _ _ _ _ Hlkp) as (_&->&->).
  Qed.

  (* The disjointness context associated to an empty row is the empty. *)
  Lemma row_to_disj_ctx_nil : le.row_to_disj_ctx [] = ∅.
  Proof. by rewrite /le.row_to_disj_ctx //=. Qed.

  (* Updating a disjointness context with an empty row is ineffectual. *)
  Lemma update_disj_ctx_nil D : le.update_disj_ctx [] D = D.
  Proof.
    rewrite /le.update_disj_ctx /le.merge_ctx.
    by rewrite row_to_disj_ctx_nil LeftId_instance_1.
  Qed.

  (* Updating the empty disjointness context with a row [ρ] is equivalent to
     constructing the context associated to [ρ]. *)
  Lemma update_disj_ctx_empty ρ : le.update_disj_ctx ρ ∅ = le.row_to_disj_ctx ρ.
  Proof. by rewrite /le.update_disj_ctx /le.merge_ctx RightId_instance_1. Qed.

  (* Merging a context [D] to the result of merging [D'] and [D''] is ineffectual
     if [D] is included in [D']. *)
  Lemma merge_ctx_idemp D D' D'' :
    le.disj_ctx_included D D' →
      le.merge_ctx D (le.merge_ctx D' D'') = le.merge_ctx D' D''.
  Proof.
    intros Hincl. rewrite /le.merge_ctx.
    apply map_eq.
    intros i. rewrite !lookup_union_with.
    destruct (D !! i) as [(ss, js)|] eqn:Hlkp;
    rewrite Hlkp;
    last by rewrite union_with_left_id.
    assert (∃ rs ks, D' !! i = Some (rs, ks) ∧ ss ⊆ rs ∧ js ⊆ ks)
      as [rs [ks (Hlkp' & Hss & Hjs)]].
    { specialize (Hincl i). revert Hincl.
      rewrite Hlkp. simpl.
      destruct (D' !! i) as [(rs, ks)|] eqn:Hlkp';
      rewrite Hlkp'; [|done].
      by intros [? ?]; exists rs, ks.
    }
    rewrite Hlkp'.
    destruct (D'' !! i) as [(ss'', js'')|] eqn:Hlkp'';
    rewrite Hlkp'' //=; do 2 f_equal; multiset_solver.
  Qed.

  (* The context associated to a row [ρ] is included in the context
     associated to a row [ρ'], if the signatures from [ρ] are included
     in [ρ']. *)
  Lemma update_disj_ctx_incl ρ ρ' :
    le.conc_sigs ρ ⊆ le.conc_sigs ρ' →
      le.abst_sigs ρ ⊆ le.abst_sigs ρ' →
        le.disj_ctx_included (le.row_to_disj_ctx ρ) (le.row_to_disj_ctx ρ').
  Proof.
    intros Hconc Habst s.
    case (decide (s ∈ le.conc_sigs ρ)) as [Hin|Hnotin].
    - assert (is_Some (le.row_to_disj_ctx ρ  !! s) ∧
              is_Some (le.row_to_disj_ctx ρ' !! s))
        as [[(ss, js) Hlkp] [(rs, ks) Hlkp']].
      { rewrite -!elem_of_dom !dom_row_to_disj_ctx !gmultiset_elem_of_dom.
        by split; [| apply (gmultiset_elem_of_subseteq _ _ _ Hin Hconc)].
      }
      rewrite Hlkp Hlkp'. simpl.
      destruct (lookup_row_to_disj_ctx _ _ _ _ Hlkp)  as (Hs&->&->).
      destruct (lookup_row_to_disj_ctx _ _ _ _ Hlkp') as (Hs'&->&->).
      split; [|done]. multiset_solver.
    - rewrite (_: le.row_to_disj_ctx ρ !! s = None) //=;[|
      by rewrite -not_elem_of_dom dom_row_to_disj_ctx gmultiset_elem_of_dom].
      by destruct (le.row_to_disj_ctx ρ' !! s) as [?|] eqn:?;
      rewrite Heqo.
  Qed.

  (* Upating the context with a row [ρ] is ineffectual if we have already
     updated this context with a row [ρ'] that contains more signatures
     than [ρ]. *)
  Lemma update_disj_ctx_submseteq D ρ ρ' :
    le.conc_sigs ρ ⊆ le.conc_sigs ρ' →
      le.abst_sigs ρ ⊆ le.abst_sigs ρ' →
        le.update_disj_ctx ρ (le.update_disj_ctx ρ' D) =
        le.update_disj_ctx ρ' D.
  Proof. by intros ??; apply merge_ctx_idemp, update_disj_ctx_incl. Qed.

  (* Removing a signature [s] from the set [le.conc_sigs ρ] is equivalent
     to removing this signature primarily from [ρ] and building the
     set of concrete signatures afterwards. *)
  Lemma conc_sigs_difference ρ i s α β :
    ρ !! i = Some (ESig s α β) →
      (le.conc_sigs ρ) ∖ {[+ s +]} = le.conc_sigs (delete i ρ).
  Proof.
    revert i. induction ρ as [|e ρ]; [done|].
    intros i. case i.
    - injection 1 as ->. by multiset_solver.
    - intros j Hj. case e as [s' ??|?]; simpl;
      rewrite -(IHρ j Hj); try multiset_solver.
      case (decide (s = s')); [intros ->|intros Hneq];
      last multiset_solver.
      assert (s' ∈ le.conc_sigs ρ) as Heq%gmultiset_disj_union_difference'.
      { rewrite elem_of_conc_sigs_iff. exists α, β.
        rewrite elem_of_list_lookup. by exists j. }
      rewrite -Heq. by multiset_solver.
  Qed.

  (* If a list of signatures [ss] is included in the multiset [le.conc_sigs ρ],
     then for each signature [s] in [ss], we can find a pair of types [α] and
     [β] such that [ESig s α β] belongs to [ρ]. Moreover, the multiplicity of
     a signature [s] in [ss] is lesser than the multiplicity of [s] in [ρ]. *)
  Lemma incluced_in_conc_sigs ss ρ :
    list_to_set_disj ss ⊆ le.conc_sigs ρ →
      ∃ ts,
        length ss = length ts ∧
        imap2 (λ _ s '(α, β), ESig s α β) ss ts ⊆+ ρ.
  Proof.
    revert ρ; induction ss as [|r rs]; intros ρ Hr.
    - exists []. by split; [|apply submseteq_nil_l].
    - assert (r ∈ le.conc_sigs ρ)
        as [α [β Hsig]]%elem_of_conc_sigs_iff.
      { by multiset_solver. }
      assert (list_to_set_disj rs ⊆ le.conc_sigs ρ ∖ {[+ r +]})
        as Hrs_subm.
      { by multiset_solver. }
      destruct (elem_of_list_lookup_1 _ _ Hsig) as [i Hi].
      assert (le.conc_sigs ρ ∖ {[+ r +]} = le.conc_sigs (delete i ρ))
        as Haux.
      { by apply (conc_sigs_difference _ _ _ _ _ Hi). }
      rewrite Haux in Hrs_subm.
      destruct (IHrs _ Hrs_subm) as [ts [Hlen Hsubm]].
      exists ((α, β) :: ts). simpl. split; [by f_equal|].
      by apply (submseteq_delete_r _ _ i).
  Qed.

End le_properties.


(** * Typing Rules. *)


Reserved Notation "Δ '.|' Γ '.|-{' a '}-' e : ρ : τ"
  (at level 74, Γ, a, e, ρ, τ at next level).

Inductive typed :

  gmap string unit → gmap string ty → att → expr → row eff_sig → ty → Prop :=


  (* Unit. *)
  | Unit_typed {Δ Γ a ρ} :

      Δ .| Γ .|-{a}- #() : ρ : ()


  (* Bool. *)
  | Bool_typed {Δ Γ a ρ} {b : bool} :

      Δ .| Γ .|-{a}- #b : ρ : TBool


  (* Int. *)
  | Int_typed {Δ Γ a ρ} {i : Z} :

      Δ .| Γ .|-{a}- #i : ρ : TInt


  (* Nil. *)
  | Nil_typed {Δ Γ a ρ τ} :

      Δ .| Γ .|-{a}- InjR #() : ρ : lst τ


  (* Cons. *)
  | Cons_typed {Δ Γ a ρ τ x xs} :

      Δ .| Γ .|-{a}- x : ρ : τ →
        Δ .| Γ .|-{a}- xs : ρ : lst τ →
          Δ .| Γ .|-{a}- InjL (x, xs) : ρ : lst τ


  (* Variables. *)
  | Var_typed {Δ Γ x a ρ α} :

      Γ !! x = Some α →
        Δ .| Γ .|-{a}- (Var V x) : ρ : α


  (* Introduction of a product type. *)
  | Prod_typed {Δ Γ a e₁ e₂ ρ α β} :

      Δ .| Γ .|-{a}- e₁ : ρ : α →
        Δ .| Γ .|-{a}- e₂ : ρ : β →
          Δ .| Γ .|-{a}- (e₁, e₂) : ρ : α * β


  (* Fst - Elimination of a product type. *)
  | Fst_typed {Δ Γ a e ρ α} β :

      Δ .| Γ .|-{a}- e : ρ : α * β →
        Δ .| Γ .|-{a}- Fst e : ρ : α


  (* Snd - Elimination of a product type. *)
  | Snd_typed {Δ Γ a e ρ} α {β} :

      Δ .| Γ .|-{a}- e : ρ : α * β →
        Δ .| Γ .|-{a}- Snd e : ρ : β


  (* Introduction of a sum type by left injection. *)
  | InjL_typed {Δ Γ a e ρ α β} :

      Δ .| Γ .|-{a}- e : ρ : α →
        Δ .| Γ .|-{a}- InjL e : ρ : α + β


  (* Introduction of a sum type by right injection. *)
  | InjR_typed {Δ Γ a e ρ α β} :

      Δ .| Γ .|-{a}- e : ρ : β →
        Δ .| Γ .|-{a}- InjR e : ρ : α + β


  (* Elimination of a sum type. *)
  | Case_typed {Δ Γ a e e₁ e₂ ρ} α β {τ} :

      Δ .| Γ .|-{a}- e₁ : ρ : (α -{a; ρ}-> τ)%ty →
        Δ .| Γ .|-{a}- e₂ : ρ : (β -{a; ρ}-> τ)%ty →
          Δ .| Γ .|-{a}- e : ρ : α + β →
            Δ .| Γ .|-{a}- Case e e₁ e₂ : ρ : τ

  (* Elimination of a list type. *)
  | ListCase_typed {Δ Γ a e e₁ e₂ ρ} α {τ} :

      Δ .| Γ .|-{a}- e₁ : ρ : ((α * lst α) -{a; ρ}-> τ)%ty →
        Δ .| Γ .|-{a}- e₂ : ρ : τ →
          Δ .| Γ .|-{a}- e : ρ : lst α →
            Δ .| Γ .|-{a}- Case e e₁ (Lam BAnon e₂) : ρ : τ


  (* Memory allocation - Introduction of a reference type. *)
  | Alloc_typed {Δ Γ} a {e ρ α} :

      Δ .| Γ .|-{a}- e : ρ : α → 
        Δ .| Γ .|-{I}- ref e : ρ : ref α


  (* Load instruction. *)
  | Load_typed {Δ Γ} a {e ρ α} :

      Δ .| Γ .|-{a}- e : ρ : ref α → 
        Δ .| Γ .|-{I}- Load e : ρ : α


  (* Write instruction. *)
  | Store_typed {Δ Γ} a₁ a₂ {e₁ e₂ ρ} α :

      Δ .| Γ .|-{a₁}- e₁ : ρ : ref α → 
        Δ .| Γ .|-{a₂}- e₂ : ρ : α → 
          Δ .| Γ .|-{I}- (e₁ <- e₂) : ρ : ()


  (* Introduction of effect-polymorphic type. *)
  | RLam_typed {Δ Γ a e ρ α} :

      Δ .| (rvar_lift._ty 0) <$> Γ .|-{P}- e : (rvar_lift._row 0 ρ) : α →
        Δ .| Γ .|-{a}- e : ρ : (∀R: α)


  (* Elimination of effect-polymorphic type. *)
  | RApp_typed {Δ Γ a e ρ} α ρ' :

      Δ .| Γ .|-{a}- e : ρ : (∀R: α) →
        Δ .| Γ .|-{a}- e : ρ : rvar_subst._ty 0 ρ' α


  (* Introduction of value-polymorphic type. *)
  | TLam_typed {Δ Γ a e ρ α} :

      Δ .| (tvar_lift._ty 0) <$> Γ .|-{P}- e : (tvar_lift._row 0 ρ) : α →
        Δ .| Γ .|-{a}- e : ρ : (∀T: α)


  (* Elimination of value-polymorphic type. *)
  | TApp_typed {Δ Γ a e ρ} α β :

      Δ .| Γ .|-{a}- e : ρ : (∀T: α) →
        Δ .| Γ .|-{a}- e : ρ : tvar_subst._ty 0 β α


  (* Application. *)
  | App_typed {Δ Γ a e₁ e₂ ρ} α {β} :

      Δ .| Γ .|-{a}- e₁ : ρ : (α -{a; ρ}-> β) →
        Δ .| Γ .|-{a}- e₂ : ρ  :  α →
          Δ .| Γ .|-{a}- (e₁ e₂) : ρ : β


  (* If-else branching. *)
  | If_typed {Δ Γ a eb e₁ e₂ ρ α} :

      Δ .| Γ .|-{a}- eb : ρ : 𝔹 →
        Δ .| Γ .|-{a}- e₁ : ρ : α →
          Δ .| Γ .|-{a}- e₂ : ρ : α →
            Δ .| Γ .|-{a}- (if: eb then e₁ else e₂) : ρ : α


  (* Binary operation. *)
  | BinOp_typed {Δ Γ a op e₁ e₂ ρ} α β {σ} :

      Δ .| Γ .|-{a}- e₁ : ρ : α →
        Δ .| Γ .|-{a}- e₂ : ρ : β →
          ty_bin_op op α β σ →
            Δ .| Γ .|-{a}- BinOp op e₁ e₂ : ρ : σ


  (* Possibly recursive function definitions. *)
  | Rec_typed {Δ Γ a₁ a₂ f x e ρ α β} :

      let Γ' := binder_insert f (α -{a₁; ρ}-> β)%ty (binder_insert x α Γ) in

      Δ .| Γ' .|-{a₁}- e : ρ : β →
        Δ .| Γ  .|-{a₂}- (rec: f x := e) : [] : (α -{a₁; ρ}-> β)


  (* Monotonicity. *)
  | Mono_typed {Δ Γ} a {a' e} b ρ α {ρ' α'} :

      a ≤A a' →
        ∅ ⊢ ρ ≤R ρ' @ b →
          ∅ ⊢ α ≤T α' →
            Δ .| Γ .|-{a}- e : ρ  : α →
              Δ .| Γ .|-{a'}- e : ρ' : α'


  (* Introduce effect name both in the context and in the row. *)
  | LetEff_typed {Δ Γ} a {e} {s : string} {ρ α} :

      let Abs_ρ := EAbs s :: ρ in
      let Δ' := <[s:=()]> Δ in

      vars._fresh s Γ ρ α →
        Δ' .| Γ .|-{a}- e : Abs_ρ : α →
          Δ .| Γ .|-{I}- (effect: s in e) : ρ : α


  (* Perform an effect. *)
  | Perform_typed {Δ Γ a e s ρ} α {β} :

      s ∈ dom Δ →
        ESig s α β ∈ ρ →
          Δ .| Γ .|-{a}- e : ρ : α →
            Δ .| Γ .|-{a}- (perform (Var L s) e) : ρ : β


  (* (Shallowly) handle an effect. *)
  | SHandle_typed {Δ Γ a s e h r ρ} α β τ {α' β' τ'} :

      let sρ  := ESig s α  β  :: ρ in
      let sρ' := ESig s α' β' :: ρ in

      s ∈ dom Δ →
        Δ .| Γ .|-{a}- e : sρ' : (() -{a; sρ}-> τ)%ty →
          Δ .| Γ .|-{a}- h : sρ' : (α -{a; [] }-> (β -{a; sρ}-> τ) -{a; sρ'}-> τ')%ty →
            Δ .| Γ .|-{a}- r : sρ' : (τ -{a; sρ'}-> τ')%ty →
              Δ .| Γ .|-{a}- (shandle (Var L s) e h r) : sρ' : τ'


  (* Try-finally statement. *)
  | TryFinally_typed {Δ Γ a e₁ e₂ ρ α} :

      Δ .| Γ .|-{a}- e₁ : ρ : (() -{a; ρ}-> ())%ty →
        Δ .| Γ .|-{a}- e₂ : ρ : (() -{a; [] }-> α)%ty →
          Δ .| Γ .|-{a}- (try_finally e₁ e₂) : ρ : α


  (* Dynamic-wind instruction. *)
  | DynamicWind_typed {Δ Γ a e₁ e₂ e₃ ρ α} :

      Δ .| Γ .|-{a}- e₁ : ρ : (() -{a; [] }-> ())%ty →
        Δ .| Γ .|-{a}- e₂ : ρ : α →
          Δ .| Γ .|-{a}- e₃ : ρ : (() -{a; [] }-> ())%ty →
            Δ .| Γ .|-{a}- (dynamic_wind e₁ (tk e₂) e₃) : ρ : α


  (* Remove effect name from the context. *)
  (* This rule could be proven as a derived rule,
     but it is easier to state it as a syntactic rule
     and to prove its compatibility lemma, than to
     prove the derived rule by induction. *)
  | RemoveEff_typed {Δ Γ a s e ρ τ} :

      let Δ' := <[s:=()]> Δ in

      Δ .| Γ .|-{a}- e : ρ : τ →
        Δ' .| Γ .|-{a}- e : ρ : τ


where "Δ .| Γ .|-{ a }- e : ρ : τ" := (typed Δ Γ a e ρ τ).


(** * Derived Weakening Rules. *)

Section derived_weakening_rules.

  (* The weakening relation on attributes is reflexive. *)
  Lemma ARefl_le a : a ≤A a.
  Proof. by case a. Qed.

  (* [P] is less than any attribute [a]. *)
  Lemma APure_le a : P ≤A a.
  Proof. by case a. Qed.

  (* [I] is greater than any attribute [a]. *)
  Lemma AImpure_le a : a ≤A I.
  Proof. by case a. Qed.

  (* If we can prove that [ρ] is weaker than [ρ'] without the ability
     to erase absence signatures, then we can prove it with this abilty. *)
  Lemma with_or_without D b ρ ρ' : D ⊢ ρ ≤R ρ' @ b → D ⊢ ρ ≤R ρ' @ true.
  Proof.
    induction 1.
    - by apply le.RNil_le.
    - by apply le.RCons_le.
    - by apply le.RSwap_le.
    - by apply le.RSkip_le.
    - by apply (le.RAbs_le _ _ _ ss js).
    - by apply (le.RTrans_le _ _ _ ρ').
  Qed.

  (* We can drop the permission to erase absence signatures. *)
  Corollary drop_permission D b ρ ρ' : D ⊢ ρ ≤R ρ' @ false → D ⊢ ρ ≤R ρ' @ b.
  Proof. by case b; [apply with_or_without|]. Qed.

  (* Reflexivity of the weakening relation on effect signatures. *)
  Lemma SRefl_le D σ : D ⊢ σ ≤S σ.
  Proof.
    case σ as [??|?].
    - by apply le.SConc_le; apply le.TRefl_le.
    - by apply le.SAbst_le.
  Qed.

  (* Reflexivity of the weakening relation on rows. *)
  Lemma RRefl_le D b ρ : D ⊢ ρ ≤R ρ @ b.
  Proof.
    apply drop_permission.
    induction ρ as [|σ ρ].
    - by apply le.RNil_le.
    - by apply le.RSkip_le; [apply SRefl_le|].
  Qed.

  (* The empty row is weaker than any row. *)
  Lemma RNil_l_le D b ρ : D ⊢ [] ≤R ρ @ b.
  Proof.
    induction ρ as [|σ ρ].
    - by apply le.RNil_le.
    - by apply (le.RTrans_le _ _ _ ρ); [|apply le.RCons_le].
  Qed.

  (* A row is always weaker than one of its permutations. *)
  Lemma RPermute_le D b ρ ρ' : ρ ≡ₚ ρ' → D ⊢ ρ ≤R ρ' @ b.
  Proof.
    intros Hperm.
    apply drop_permission.
    induction Hperm as [| | |ρ ρ' ρ''].
    - by apply le.RNil_le.
    - by apply le.RSkip_le; [apply SRefl_le|].
    - by apply le.RSwap_le.
    - by apply (le.RTrans_le _ _ _ ρ').
  Qed.

End derived_weakening_rules.


(** * Derived Typing Rules. *)

Section derived_rules.

  (* Monotonicity on attributes. *)
  Lemma AttMono_typed {Δ Γ} a {a' e ρ α} :
    a ≤A a' →
      Δ .| Γ .|-{a}- e : ρ  : α →
        Δ .| Γ .|-{a'}- e : ρ : α.
  Proof.
    intros Ha He.
    apply (Mono_typed a false ρ α); try done.
    { by apply RRefl_le. }
    { by apply le.TRefl_le. }
  Qed.

  (* Monotonicity on types. *)
  Lemma TypeMono_typed {Δ Γ a e ρ} α {α'} :
    ∅ ⊢ α ≤T α' →
      Δ .| Γ .|-{a}- e : ρ  : α →
        Δ .| Γ .|-{a}- e : ρ : α'.
  Proof.
    intros Hα He.
    apply (Mono_typed a false ρ α); try done.
    { by apply ARefl_le. }
    { by apply RRefl_le. }
  Qed.

  (* Monotonicity on rows. *)
  Lemma RowMono_typed {Δ Γ a e} b ρ {ρ' α} :
    ∅ ⊢ ρ ≤R ρ' @ b →
      Δ .| Γ .|-{a}- e : ρ  : α →
        Δ .| Γ .|-{a}- e : ρ' : α.
  Proof.
    intros Hρ He.
    apply (Mono_typed a b ρ α); try done.
    { by apply ARefl_le. }
    { by apply le.TRefl_le. }
  Qed.

  (* Pure expressions. *)
  Lemma Pure_typed {Δ Γ a e ρ α} :
    Δ .| Γ .|-{P}- e : ρ : α →
      Δ .| Γ .|-{a}- e : ρ : α.
  Proof.
    intro He.
    apply (Mono_typed P false ρ α); last done.
    { by apply APure_le. }
    { by apply RRefl_le. }
    { by apply le.TRefl_le. }
  Qed.

  (* Impure expressions. *)
  Lemma Impure_typed {Δ Γ a e ρ α} :
    Δ .| Γ .|-{a}- e : ρ : α →
      Δ .| Γ .|-{I}- e : ρ : α.
  Proof.
    intro He.
    apply (Mono_typed a false ρ α); last done.
    { by apply AImpure_le. }
    { by apply RRefl_le. }
    { by apply le.TRefl_le. }
  Qed.

  (* Bottom type. *)
  Lemma Bot_typed {Δ Γ a e ρ α} :
    Δ .| Γ .|-{a}- e : ρ : ⊥ →
      Δ .| Γ .|-{a}- e : ρ : α.
  Proof. by intro He; apply (TypeMono_typed ⊥); [apply le.TBot_le|]. Qed.

  (* Weakening of a signature. *)
  Lemma Dismiss_typed {Δ Γ a s e ρ α β τ} :
    let Abs_ρ := EAbs s :: ρ in
    let Pre_ρ := ESig s α β :: ρ in

    Δ .| Γ .|-{a}- e : Abs_ρ : τ →
      Δ .| Γ .|-{a}- e : Pre_ρ : τ.
  Proof.
    intros ?? He.
    apply (RowMono_typed false Abs_ρ); last done.
    apply le.RSkip_le; [|by apply RRefl_le].
    apply le.SConc_le.
    - by apply le.TBot_le.
    - by apply le.TTop_le.
  Qed.

  (* Empty row. *)
  Lemma MonoEmpty_typed {Δ Γ a e ρ α} :
    Δ .| Γ .|-{a}- e : [] : α →
      Δ .| Γ .|-{a}- e : ρ : α.
  Proof.
    intro He.
    apply (RowMono_typed false []); last done.
    by apply RNil_l_le.
  Qed.

  (* Permutation of effect names in a row. *)
  Lemma Permute_typed {Δ Γ a e} ρ {ρ' α} :
    ρ ≡ₚ ρ' →
      Δ .| Γ .|-{a}- e : ρ  : α →
        Δ .| Γ .|-{a}- e : ρ' : α.
  Proof.
    intros Hρ He.
    apply (RowMono_typed false ρ); last done.
    by apply RPermute_le.
  Qed.

  (* Typing rule for let expressions. *)
  Lemma Bind_typed {Δ Γ a x e₁ e₂ ρ} α {β} :
    let Γ' := binder_insert x α Γ in

    lift_to_binder (.∉ (dom Δ)) x →
      Δ .| Γ .|-{a}- e₁ : ρ : α →
        Δ .| Γ' .|-{a}- e₂ : ρ : β →
          Δ .| Γ  .|-{a}- (let: x := e₁ in e₂) : ρ : β.
  Proof.
    intros ? Hx He1 He2.
    apply (App_typed α); [|done].
    by apply MonoEmpty_typed, Pure_typed, Rec_typed.
  Qed.

  (* Derived rule for a deep handler. *)
  Lemma Handle_typed {Δ Γ a s e h r ρ} α β τ {α' β' τ'} :
    let sρ  := ESig s α  β  :: ρ in
    let sρ' := ESig s α' β' :: ρ in

    s ∈ dom Δ →
      Δ .| Γ .|-{a}- e : sρ : τ →
        Δ .| Γ .|-{a}- h : sρ' : (α -{a; [] }-> (β -{a; sρ'}-> τ') -{a; sρ'}-> τ')%ty →
          Δ .| Γ .|-{a}- r : sρ' : (τ -{a; sρ'}-> τ')%ty →
            Δ .| Γ .|-{a}- (handle (vl s) (tk e) h r) : sρ' : τ'.
  Proof.
    intros ??.
    set c_type := (() -{a; sρ}-> τ)%ty.
    set k_type := (β -{a; sρ'}-> τ')%ty.
    set h_type := (α -{a; [] }-> k_type -{a; sρ'}-> τ')%ty.
    set r_type := (τ -{a; sρ'}-> τ')%ty.

    intros Hin He Hh Hr.
    apply (App_typed r_type); [|done].
    apply (App_typed h_type); [|done].
    apply (App_typed c_type);
    apply MonoEmpty_typed, Rec_typed; [|done].
    repeat apply MonoEmpty_typed, Rec_typed; simpl.
    apply (SHandle_typed α β τ). done.
    (* client. *)
    - apply Var_typed; by simpl_map.
    (* effect branch. *)
    - repeat apply MonoEmpty_typed, Rec_typed; simpl.
      apply (App_typed k_type);[
      apply (App_typed α)|].
      + apply (TypeMono_typed h_type);[|
        apply Var_typed; by simpl_map].
        apply (le.TArrow_le _ false); try apply le.TRefl_le.
        apply ARefl_le.
        by apply RNil_l_le.
      + apply Var_typed.
        rewrite lookup_insert_ne //.
        by rewrite lookup_insert.
      + apply MonoEmpty_typed, Rec_typed; simpl.
        apply (App_typed r_type); last  by (apply Var_typed; simpl_map).
        apply (App_typed h_type); last  by (apply Var_typed; simpl_map).
        apply (App_typed c_type); first by (apply Var_typed; simpl_map).
        apply MonoEmpty_typed, Rec_typed; simpl.
        apply (App_typed β); apply Var_typed; by simpl_map.
    (* return branch. *)
    - apply MonoEmpty_typed, Rec_typed; simpl.
      apply (App_typed τ);
      apply Var_typed; by simpl_map.
  Qed.

  (* We specialize the rule (Handle_typed) to the case where the
     external signature attached to [s] is the absence signature.
     We can exploit the weakening relation to erase this
     signature from the row of the continuation. *)
  Lemma Handle_typed' {Δ Γ a s e h r ρ} α β τ {τ'} :
    let sρ  := ESig s α β :: ρ in
    let sρ' := EAbs s     :: ρ in

    s ∈ dom Δ →
      Δ .| Γ .|-{a}- e : sρ : τ →
        Δ .| Γ .|-{a}- h : sρ' : (α -{a;[] }-> (β -{a;ρ}-> τ') -{a;sρ'}-> τ') →
          Δ .| Γ .|-{a}- r : sρ' : (τ -{a;sρ'}-> τ') →
            Δ .| Γ .|-{a}- (handle (vl s) (tk e) h r) : sρ' : τ'.
  Proof.
    intros ?? Hin He Hh Hr.
    apply (Handle_typed _ _ _ Hin He); last done.
    set (γ  := (α -{a; [] }-> (β -{a; ρ }-> τ') -{a; sρ'}-> τ')%ty).
    apply (TypeMono_typed γ); last done.
    apply (le.TArrow_le _ true);
    [apply ARefl_le|apply le.TRefl_le| |apply le.RNil_le].
    apply (le.TArrow_le _ true);
    [apply ARefl_le| |apply le.TRefl_le|apply RRefl_le].
    apply (le.TArrow_le _ true); try apply le.TRefl_le; try apply ARefl_le.
    apply (le.RAbs_le _ _ _ (le.conc_sigs ρ)
                            (le.abst_sigs ρ)); try done.
    rewrite update_disj_ctx_nil.
    rewrite update_disj_ctx_submseteq; try multiset_solver.
    rewrite update_disj_ctx_empty.
    assert (s ∈ dom (le.conc_sigs (ESig s ⊥ ⊤ :: ρ))) as Hs.
    { rewrite gmultiset_elem_of_dom //=; by multiset_solver. }
    rewrite (elem_of_dom_conc_sigs _ _ Hs) //=.
    do 2 f_equal. by multiset_solver.
  Qed.

End derived_rules.


(* Simple type-checking solver. *)

Ltac simple_checker := repeat
  match goal with  
  (* Variables. *)
  | [ |- (_ .| _ .|-{_}- (string_to_expr _) : _  : _) ] =>
      by apply Var_typed; simpl_map
  | [ |- (_ .| _ .|-{_}- (Var V _) : _  : _) ] =>
      by apply Var_typed; simpl_map
  (* Unit. *)
  | [ |- (_ .| _ .|-{_}- _ : _  : TUnit) ] =>
      by apply Unit_typed
  (* Int. *)
  | [ |- (_ .| _ .|-{_}- _ : _  : TInt) ] =>
      by apply Int_typed
  (* Nil. *)
  | [ |- (_ .| _ .|-{_}- InjR _ : _  : TList _) ] =>
      by apply Nil_typed
  (* Cons. *)
  | [ |- (_ .| _ .|-{_}- InjL _ : _  : TList _) ] =>
      apply Cons_typed
  (* Product. *)
  | [ |- (_ .| _ .|-{_}- _ : _  : TProd _ _) ] =>
      apply Prod_typed
  (* Right injection. *)
  | [ |- (_ .| _ .|-{_}- InjR _ : _  : TSum _ _) ] =>
      apply InjR_typed
  (* Left injection. *)
  | [ |- (_ .| _ .|-{_}- InjL _ : _  : TSum _ _) ] =>
      apply InjL_typed
  (* Value-polymorphic type. *)
  | [ |- (_ .| _ .|-{_}- _ : _  : TForall T _) ] =>
      apply TLam_typed; simpl;
      try rewrite !fmap_insert //=;
      try rewrite !fmap_empty //=
  (* Effect-polymorphic type. *)
  | [ |- (_ .| _ .|-{_}- _ : _  : TForall R _) ] =>
      apply RLam_typed; simpl;
      try rewrite !fmap_insert //=;
      try rewrite !fmap_empty //=
  (* Arrow type. *)
  | [ |- (_ .| _ .|-{_}- _ : _  : TArr _ _ _ _) ] =>
      apply MonoEmpty_typed, Rec_typed; try done; simpl
  end.


(** * Typing Examples. *)

Section typing_examples.

  (* Lexically scoped handler. *)

  Section lexically_scoped_handler.

    Lemma LexHandle_typed {Δ Γ} {s : string} a {e : expr} {h r ρ} υ τ κ {κ'} :
      let Γ' := binder_insert "x" υ Γ in
      let θ  := RVar 0 in
      let lρ := rvar_lift._row 0 ρ in
      let lυ := rvar_lift._ty  0 υ in
      let lτ := rvar_lift._ty  0 τ in
      let lκ := rvar_lift._ty  0 κ in
  
      let lex_handle :=
        (effect: s in
         handle (vl s) (tk
           (e (λ: "x", perform (vl s) "x")))
         (* with *) h r)%E
      in
  
      vars._fresh s Γ ρ (υ * τ * κ * κ') →
        Δ .| Γ .|-{a}- e : ρ : (∀R: (lυ -{P; [θ] }-> lτ) -{a; θ :: lρ}-> lκ) →
          Δ .| Γ .|-{a}- h : ρ : (υ -{a; [] }-> (τ -{a; ρ}-> κ') -{a; ρ}-> κ') →
            Δ .| Γ .|-{a}- r : ρ : (κ -{a; ρ}-> κ') →
              Δ .| Γ .|-{I}- lex_handle : ρ : κ'.
    Proof.
      set esig := ESig s υ τ.
      set client_type := ((υ -{a; [esig] }-> τ) -{a; esig :: ρ}-> κ)%ty.
      set h_type := (υ -{a; [] }-> (τ -{a; ρ }-> κ') -{a; ρ}-> κ')%ty.
      set r_type := (κ -{a; ρ}-> κ')%ty.
  
      intros ???????.
      intros
        (Hs & HΓ & Hρ &
        [[[Hυ Hτ ]%vars_ty_prod
              Hκ ]%vars_ty_prod
              Hκ']%vars_ty_prod)
        He Hh Hr.
      apply (LetEff_typed a); [done|].
      apply (Handle_typed' υ τ κ); first set_solver.
      - apply (App_typed (υ -{P; [esig] }-> τ)).
        + apply RemoveEff_typed.
          apply (RowMono_typed false ρ); [by apply le.RCons_le|].
          specialize (RApp_typed _ [esig] He). simpl.
          rewrite !rvar_subst_lift_ty. fold rvar_subst._row.
          rewrite !rvar_subst_row_cons //=.
          rewrite rvar_subst_lift_row /rvar_subst._row_pre //=.
        + simple_checker.
          apply (Perform_typed υ); [| |simple_checker];
          set_solver.
      - apply RemoveEff_typed.
        apply (RowMono_typed false ρ); [by apply le.RCons_le|].
        apply (TypeMono_typed h_type);[|done].
        do 2 (apply (le.TArrow_le _ true);
        [apply ARefl_le|apply le.TRefl_le| |try apply le.RNil_le]).
        apply le.TRefl_le.
        by apply le.RCons_le.
      - apply RemoveEff_typed.
        apply (RowMono_typed false ρ); [by apply le.RCons_le|].
        apply (TypeMono_typed r_type); last done.
        apply (le.TArrow_le _ true);
        [apply ARefl_le|apply le.TRefl_le| |try apply le.RNil_le].
        apply le.TRefl_le.
        by apply le.RCons_le.
    Qed.

  End lexically_scoped_handler.

  (* The function [counter]. *)

  Section counter.

    (* Implementation. *)

    Definition tick_eff_branch : expr :=
      λ: <> "k", λ: "n", "k" #() ("n" + #1).

    Definition tick_ret_branch : expr :=
      λ: "y", λ: "n", ("y", "n").

    Definition counter : expr := λ: "ff", λ: "f",
      let lex_handle e h r :=
        (effect: "s" in
         handle (Var L "s") (λ: <>, e (λ: "x", perform (Var L "s") "x"))%E
         (* with *) h r)%E
      in
      (lex_handle (λ:"tick",
         "ff" (λ: "x", "tick" #();; "f" "x"))
       (* with *)
         tick_eff_branch
         tick_ret_branch
      ) #0.

    (* Type. *)

    Definition counter_type a : ty :=
      let α  := TVar 2 in
      let β  := TVar 1 in
      let γ  := TVar 0 in
      let γ' := (γ * ℤ)%ty in

      let θ := [RVar 0] in

      (∀T: ∀T: ∀T:
        (∀R: (α -{a; θ}-> β) -{I; θ}-> γ ) --->
        (∀R: (α -{a; θ}-> β) -{I; θ}-> γ')
      ).

    (* Type derivation. *)

    Lemma counter_typed {a a'} :
      ∅ .| ∅ .|-{a'}- counter : [] : (counter_type a).
    Proof.
      set α :=  TVar 2.
      set β :=  TVar 1.
      set γ :=  TVar 0.
      set tsig := ESig "s" () ().
      set θ  := RVar 0.
      set θ' := RVar 1.

      unfold counter_type.
      simple_checker.
      apply (App_typed ℤ); [|by simple_checker].
      apply (LexHandle_typed I () () γ); first done; simple_checker.
      - apply (App_typed (α -{a;[θ; θ'] }-> β)).
        { fold θ θ' α β γ.
          apply (RApp_typed ((α -{a; [θ] }-> β) -{I; [θ] }-> γ) [θ; θ']).
          by simple_checker.
        }
        { simple_checker.
          apply (Bind_typed ()); first done.
          + apply (App_typed ()); last by simple_checker.
            apply (TypeMono_typed (() -{P; [θ] }-> ())).
            * apply (le.TArrow_le _ false);
              try apply le.TRefl_le; try apply APure_le.
              by apply le.RSkip_le; [apply SRefl_le|apply RNil_l_le].
            * by simple_checker.
          + apply (RowMono_typed false [θ']).
            { by apply le.RCons_le. }
            { by apply (App_typed α); simple_checker. }
        }
      - apply (App_typed ℤ).
        { apply (App_typed ()); simple_checker. }
        { apply (BinOp_typed ℤ ℤ); last (by apply Ty_bin_op_arith, _); simple_checker. }
    Qed.

    Lemma counter_counter_typed :
      let e : expr :=
        (counter (counter (λ: "f", "f" #())) (λ: <>, #()))%E
      in
      ∅ .| ∅ .|-{I}- e : [] : ((() * ℤ) * ℤ).
    Proof.
      set α  := TVar 2.
      set β  := TVar 1.
      set γ  := TVar 0.
      set γ' := (γ * ℤ)%ty.

      set θ := [RVar 0].

      set τ := λ α β γ, ((α -{θ}-> β) -{θ}-> γ)%ty.
      set τ₁  := (τ ()%ty ()%ty).
      set τ₂  := (τ ()%ty β).
      set τ₃  := (τ α β).

      intros ?.
      apply (App_typed (() -{ [] }-> ()));
      last by apply Rec_typed, Unit_typed.
      apply (AttMono_typed P); first by apply APure_le.
      apply (RApp_typed (τ₁ ((() * ℤ) * ℤ))%ty []).
      apply (App_typed (∀R: (τ₁ (() * ℤ)))).
      - apply (TApp_typed (        (∀R: τ₁ γ) ---> (∀R: τ₁ γ')) (() * ℤ)).
        apply (TApp_typed (∀T:     (∀R: τ₂ γ) ---> (∀R: τ₂ γ')) ()).
        apply (TApp_typed (∀T: ∀T: (∀R: τ₃ γ) ---> (∀R: τ₃ γ')) ()).
        by apply counter_typed.
      - apply (App_typed (∀R: τ₁ ())).
        + apply (TApp_typed (        (∀R: τ₁ γ) ---> (∀R: τ₁ γ')) ()).
          apply (TApp_typed (∀T:     (∀R: τ₂ γ) ---> (∀R: τ₂ γ')) ()).
          apply (TApp_typed (∀T: ∀T: (∀R: τ₃ γ) ---> (∀R: τ₃ γ')) ()).
          by apply counter_typed.
        + apply RLam_typed, Rec_typed.
          apply (App_typed ()); last by apply Unit_typed.
          by apply Var_typed; simpl_map.
    Qed.

  End counter.

  (* The function [iter]. *)

  Section iter.

    (* Implementation. *)

    Definition iter : expr := rec: "iter" "xs" "f" :=
      match: "xs" with
        InjL "args" => "f" (Fst "args");; "iter" (Snd "args") "f"
      | InjR <>     => #()
      end.

    (* Type. *)

    Definition iter_type a : ty :=
      let α := TVar 0 in
      let θ := [RVar 0] in
      ∀T: ∀R: lst α -{a;[] }-> (α -{a; θ}-> ()) -{a; θ}-> ().

    (* Type derivation. *)

    Lemma iter_typed {Δ Γ a a'} :
      Δ .| Γ .|-{a'}- iter : [] : (iter_type a).
    Proof.
      set α :=  TVar 0.
      set θ := [RVar 0].

      unfold iter_type.
      simple_checker.
      apply (ListCase_typed α); simple_checker.
      apply (Bind_typed ()); first done; simpl.
      { apply (App_typed α); simple_checker.
        apply (Fst_typed (lst α)); simple_checker.
      }
      { apply (App_typed (α -{a; θ}-> ()));[
        apply MonoEmpty_typed, (App_typed (lst α))|];
        simple_checker.
        apply (Snd_typed  α); simple_checker.
      }
    Qed.

  End iter.

  (* The function [filter]. *)

  Section filter.

    (* We suppose the existence of an effect [yield]. *)
    Definition yieldS := "yield".
    Definition yield  := (Var L yieldS).

    (* Implementation. *)

    Definition filter : expr := λ: "xs" "f",
      iter "xs" (λ: "x", if: "f" "x" then perform yield "x" else #()).

    (* Type. *)

    Definition yield_sig α := ESig yieldS α ().

    Definition filter_type a : ty :=
      let α :=  TVar 0  in
      let θ := [RVar 0] in
      ∀T: ∀R: lst α -{a;[] }-> (α -{a; θ}-> 𝔹) -{a; (yield_sig α) :: θ}-> ().

    (* Type derivation. *)

    Lemma filter_typed a :
      {[yieldS:=()]} .| ∅ .|-{a}- filter : [] : (filter_type a).
    Proof.
      set α :=  TVar 0.
      set θ := [RVar 0].
      set f_type := (α -{a; θ}-> 𝔹)%ty.
      set iter_type :=
        (lst α -{a;[] }-> (α -{a; θ}-> ()) -{a; θ}-> ())%ty.

      unfold filter_type.
      simple_checker.
      apply (App_typed (α -{a; (yield_sig α) :: θ}-> ()));[
      apply MonoEmpty_typed, (App_typed (lst α)) |].
      { fold α θ.
        apply (RApp_typed iter_type ((yield_sig α) :: θ)).
        apply (TApp_typed (∀R: iter_type) α).
        by apply iter_typed.
      }
      { simple_checker. }
      { simple_checker.
        apply If_typed; simple_checker.
        - apply (App_typed α); simple_checker.
          apply (TypeMono_typed (α -{a;θ}-> 𝔹));
          last by simple_checker.
          apply (le.TArrow_le _ false); try by apply le.TRefl_le.
          apply ARefl_le.
          by apply le.RCons_le.
        - apply (Perform_typed α); simple_checker;
          set_solver.
      }
    Qed.

  End filter.

End typing_examples.
