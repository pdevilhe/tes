(* interp.v *)

(* This file contains the definition of the interpretation of types, of rows,
   and of typing judgments. Types are interpreted as _semantic types_, which
   are persistent Iris's predicates, rows are interpreted as _semantic rows_,
   which are simply lists of type [eff_list], and typing judgments are
   interpreted as specifications written in terms of the weakest precondition
   from the theory [labeled_effects.v].
*)

From iris.proofmode Require Export base tactics classes.
From iris.base_logic.lib Require Export invariants.
From logic Require Export extended_weakestpre pure_weakestpre.
From logic Require Export labeled_effects heap.
From effect_system Require Export mixed_weakestpre.
From effect_system Require Export subst_map notation rules.
From lib Require Import lists maps_and_sets.

(* =-=-=-=-= Axiom of functional extensionality. =-=-=-=-= *)
   From Coq.Logic Require Import FunctionalExtensionality.
(* =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-= *)


(** * Semantic Types. *)

(* A semantic type is a persistent Iris's predicate. *)

Record sem_ty Σ := SemTy {
  sem_ty_car :> val → iProp Σ;
  sem_ty_persistent v : Persistent (sem_ty_car v)
}.
Arguments SemTy {_} _%_I {_}.
Arguments sem_ty_car {_} _ _ : simpl never.

Existing Instance sem_ty_persistent.

Declare Scope sem_ty_scope.
Bind Scope sem_ty_scope with sem_ty.
Delimit Scope sem_ty_scope with sem_ty.

(* We equip semantic types with a COFE structure by providing an ordered
   family of equivalences and a limit operator.
*)

Section sem_ty_cofe.
  Context {Σ : gFunctors}.

  Instance sem_ty_equiv : Equiv (sem_ty Σ) := λ A B, ∀ w, A w ≡ B w.
  Instance sem_ty_dist : Dist (sem_ty Σ) := λ n A B, ∀ w, A w ≡{n}≡ B w.
  Lemma sem_ty_ofe_mixin : OfeMixin (sem_ty Σ).
  Proof. by apply (iso_ofe_mixin (sem_ty_car : _ → val -d> _)). Qed.
  Canonical Structure sem_tyO := Ofe (sem_ty Σ) sem_ty_ofe_mixin.
  Global Instance sem_ty_cofe : Cofe sem_tyO.
  Proof.
    apply (iso_cofe_subtype' (λ A : val -d> iPropO Σ, ∀ w, Persistent (A w))
      (@SemTy _) sem_ty_car)=> //; first apply _.
    apply limit_preserving_forall=> w.
    by apply bi.limit_preserving_Persistent=> n ??.
  Qed.

  Global Instance sem_ty_inhabited : Inhabited (sem_ty Σ) :=
    populate (SemTy inhabitant).

  Global Instance sem_ty_car_ne n :
    Proper (dist n ==> (=) ==> dist n) sem_ty_car.
  Proof. by intros A A' ? w ? <-. Qed.
  Global Instance sem_ty_car_proper :
    Proper ((≡) ==> (=) ==> (≡)) sem_ty_car.
  Proof. by intros A A' ? w ? <-. Qed.
End sem_ty_cofe.

Arguments sem_tyO : clear implicits.


(** * Semantic Rows. *)

(* A semantic signature is the pair of an effect label and a pure protocol. *)
Definition sem_eff_sig Σ := (eff_label * pure_iEff Σ)%type.

(* A semantic row is simply a list of type [eff_list]. *)
Definition sem_row Σ := eff_listO Σ.

Global Instance sem_row_inhabited {Σ} : Inhabited (sem_row Σ) :=
  populate [].


(** * Semantic Type Formers. *)

(* To simplify the definition of the interpretation of types and rows,
   we anticipate the interpretation of type constructors, such as products
   and sums, by interpreting them as functions from semantic types to
   semantic types. *)

(** Definitions. *)

Section sem_type_formers.

  (* Base types. *)
  Definition sem_ty_bot {Σ} : sem_ty Σ := SemTy (λ _, False)%I.
  Definition sem_ty_top {Σ} : sem_ty Σ := SemTy (λ _, True)%I.
  Definition sem_ty_unit {Σ} : sem_ty Σ := SemTy (λ w,
    ⌜ w = #() ⌝)%I.
  Definition sem_ty_bool {Σ} : sem_ty Σ := SemTy (λ w,
    ∃ b : bool, ⌜ w = #b ⌝)%I.
  Definition sem_ty_int {Σ} : sem_ty Σ := SemTy (λ w,
    ∃ n : Z, ⌜ w = #n ⌝)%I.

  (* Product type. *)
  Definition sem_ty_prod {Σ} (A1 A2 : sem_ty Σ) : sem_ty Σ := SemTy (λ w,
    ∃ w1 w2, ⌜w = (w1, w2)%V⌝ ∧ A1 w1 ∧ A2 w2)%I.

  (* Sums type. *)
  Definition sem_ty_sum {Σ} (A1 A2 : sem_ty Σ) : sem_ty Σ := SemTy (λ w,
      (∃ w1, ⌜w = InjLV w1⌝ ∧ A1 w1)
    ∨ (∃ w2, ⌜w = InjRV w2⌝ ∧ A2 w2))%I.

  (* Reference type. *)
  Definition tyN := nroot .@ "ty".
  Definition sem_ty_ref `{!heapG Σ} (A : sem_ty Σ) : sem_ty Σ := SemTy (λ w,
    ∃ l : loc, ⌜w = #l⌝ ∧ inv (tyN .@ l) (∃ v, l ↦ v ∗ A v))%I.

  (* List type. *)
  Definition encode_list : list val → val :=
    foldr (λ u acc, InjLV (u, acc)) (InjRV #()).
  Definition sem_ty_list {Σ} (A : sem_ty Σ) : sem_ty Σ := SemTy (λ w,
    ∃ us : list val, ⌜w = encode_list us⌝ ∧ [∗ list] u ∈ us, A u)%I.

  (* Effect signatures. *)
  Definition SIG_pre {Σ} (A B : sem_ty Σ) : iEff Σ :=
    (>> (a : val) >> ! a {{ A a }};
     << (b : val) << ? b {{ B b }}).
  Global Instance SIG_pre_pure_protocol {Σ} A B : @PureProtocol Σ (SIG_pre A B).
  Proof.
    constructor. intros v. exists B. iIntros (Q').
    rewrite !/SIG_pre !(iEff_tele_eq' [tele _] [tele _]).
    iIntros "[%a (-> & #HA & #HQ')]". iSplitR; [by iExists a; auto|].
    by iIntros (w) "#HB !#"; iApply "HQ'".
  Qed.
  Definition SIG {Σ} (A B : sem_ty Σ) : pure_iEff Σ := PureIEff (SIG_pre A B).

  Definition sem_eff_Pre {Σ} (l : eff_label) (A B : sem_ty Σ) : sem_eff_sig Σ :=
    (l, SIG A B).
  Definition sem_eff_Abs {Σ} (l : eff_label) : sem_eff_sig Σ :=
    (l, ⊥).

  (* Arrow type. *)
  Definition sem_ty_arr `{!heapG Σ}
    (a : att)
    (A : sem_ty Σ)
    (R : sem_row Σ)
    (B : sem_ty Σ) :=
    SemTy (λ (w : val), □ ∀ (v : val), A v -∗
      MWP (w v) @ a <| R |> {{ B }})%I.

  (* Universal quantification. *)
  Definition sem_ty_forall `{!heapG Σ}
    {A : ofe} (C : A → sem_ty Σ) :=
    SemTy (λ w, ∀ (E : A), (C E) w)%I.

End sem_type_formers.

(* Notations. *)
Notation "()" := sem_ty_unit : sem_ty_scope.
Infix "*" := sem_ty_prod : sem_ty_scope.
Infix "+" := sem_ty_sum : sem_ty_scope.
Notation "A '-{' R '}->' B" := (sem_ty_arr A R B)
  (at level 100, R, B at level 200) : sem_ty_scope.
Notation "'ref' A" := (sem_ty_ref A) : sem_ty_scope.

(* [Params t n] instances. *)
Instance: Params (@sem_ty_prod) 1 := {}.
Instance: Params (@sem_ty_sum) 1 := {}.
Instance: Params (@sem_ty_ref) 2 := {}.

(** Properties. *)

Section sem_ty_formers_properties.
  Context `{!heapG Σ}.

  Global Instance SIG_ne : NonExpansive2 (@SIG Σ).
  Proof.
    intros ? ??? ???.
    apply iEffPre_exist_ne=>?. apply iEffPre_base_ne; [done|done|].
    do 2 f_equiv; intros ?. by apply iEffPost_base_ne.
  Qed.

  Global Instance sem_ty_prod_ne : NonExpansive2 (@sem_ty_prod Σ).
  Proof. solve_proper. Qed.
  Global Instance sem_ty_sum_ne : NonExpansive2 (@sem_ty_sum Σ).
  Proof. solve_proper. Qed.
  Global Instance sem_ty_ref_ne : NonExpansive2 (@sem_ty_ref Σ _).
  Proof. solve_proper. Qed.

  Global Instance sem_ty_prod_proper :
    Proper ((≡) ==> (≡) ==> (≡)) (@sem_ty_prod Σ).
  Proof. solve_proper. Qed.
  Global Instance sem_ty_sum_proper :
    Proper ((≡) ==> (≡) ==> (≡)) (@sem_ty_sum Σ).
  Proof. solve_proper. Qed.
  Global Instance sem_ty_ref_proper :
    Proper ((≡) ==> (≡)) (@sem_ty_ref Σ _).
  Proof. solve_proper. Qed.

  Lemma SIG_agreement (A B : sem_ty Σ) v Φ :
    protocol_agreement v (SIG A B) Φ ≡
    (∃ (a : val), ⌜ v = a ⌝ ∗ A a ∗ □ (∀ b, B b -∗ Φ b))%I.
  Proof.
    rewrite /SIG (protocol_agreement_tele' [tele _] [tele _]). by auto.
  Qed.
  Lemma iEff_car_SIG (A B : sem_ty Σ) v Φ :
    iEff_car (SIG A B) v Φ ⊣⊢
    (∃ (a : val), ⌜ v = a ⌝ ∗ A a ∗ □ (∀ b, B b -∗ Φ b))%I.
  Proof.
    rewrite /SIG (iEff_tele_eq' [tele _] [tele _]). by auto.
  Qed.
End sem_ty_formers_properties.


(** * Interpretation of Types and Rows. *)

(** Definitions. *)

Module interp.

  Definition _eff_sig_pre {Σ}
    (f : ty → sem_ty Σ)
    (ξ : list (sem_row Σ))
    (δ : gmap eff_name eff_label)
    (σ : eff_sig) : sem_row Σ :=
    match σ with
    | ESig s α β =>
        [sem_eff_Pre (δ !!! s) (f α) (f β)]
    | RVar n =>
        (ξ !!! n)
    end.
  
  Definition _row_pre {Σ}
    (f : ty → sem_ty Σ)
    (ξ : list (sem_row Σ))
    (δ : gmap eff_name eff_label)
    (ρ : row eff_sig) : sem_row Σ :=
    concat ((_eff_sig_pre f ξ δ) <$> ρ).

  Fixpoint _ty `{!heapG Σ}
    (η : list (sem_ty Σ))
    (ξ : list (sem_row Σ))
    (δ : gmap eff_name eff_label)
    (α : ty) : sem_ty Σ :=
    match α with
    | TTop =>
        sem_ty_top
    | TBot =>
        sem_ty_bot
    | TUnit =>
        sem_ty_unit
    | TBool =>
        sem_ty_bool
    | TInt =>
        sem_ty_int
    | TRef α =>
        sem_ty_ref (_ty η ξ δ α)
    | TList α =>
        sem_ty_list (_ty η ξ δ α)
    | TProd α β =>
        sem_ty_prod (_ty η ξ δ α) (_ty η ξ δ β)
    | TSum α β =>
        sem_ty_sum (_ty η ξ δ α) (_ty η ξ δ β)
    | TArr A α ρ β =>
        sem_ty_arr A
          (_ty η ξ δ α)
          (_row_pre (_ty η ξ δ) ξ δ ρ)
          (_ty η ξ δ β)
    | TForall R α =>
        sem_ty_forall
          (λ E, _ty η (E :: ξ) δ α)
    | TForall T α =>
        sem_ty_forall
          (λ A, _ty (A :: η) ξ δ α)
    | TVar n =>
        (η !!! n)
    end.
  Instance: Params (@_ty) 3 := {}.
  
  Definition _eff_sig `{!heapG Σ}
    (η : list (sem_ty Σ))
    (ξ : list (sem_row Σ))
    (δ : gmap eff_name eff_label)
    (σ : eff_sig) : sem_row Σ :=
    _eff_sig_pre (_ty η ξ δ) ξ δ σ.

  Definition _row `{!heapG Σ}
    (η : list (sem_ty Σ))
    (ξ : list (sem_row Σ))
    (δ : gmap eff_name eff_label)
    (ρ : row eff_sig) : sem_row Σ :=
    _row_pre (_ty η ξ δ) ξ δ ρ.

  Definition _env `{!heapG Σ}
    (η : list (sem_ty Σ))
    (ξ : list (sem_row Σ))
    (δ : gmap eff_name eff_label)
    (Γ : gmap string ty) : gmap string (sem_ty Σ) :=
    (_ty η ξ δ) <$> Γ.

End interp.

Instance: Params (@interp._env) 3 := {}.

Notation "V⟦ η ; ξ ; δ ; α ⟧" := (interp._ty      η ξ δ α).
Notation "R⟦ η ; ξ ; δ ; ρ ⟧" := (interp._row     η ξ δ ρ).
Notation "S⟦ η ; ξ ; δ ; σ ⟧" := (interp._eff_sig η ξ δ σ).

(** Properties. *)

Section interp_properties.
  Context `{!heapG Σ}.
  Implicit Types Γ : gmap string ty.
  Implicit Types α : ty.
  Implicit Types i n j : nat.

  Lemma interp_env_empty η ξ δ : interp._env η ξ δ (∅ : gmap string ty) = ∅.
  Proof. by rewrite /interp._env fmap_empty. Qed.
  Lemma lookup_interp_env η ξ δ Γ x α :
    Γ !! x = Some α → (interp._env η ξ δ Γ) !! x = Some V⟦ η ; ξ ; δ ; α ⟧.
  Proof. intros Hx. by rewrite /interp._env lookup_fmap Hx. Qed.
  Lemma interp_env_binder_insert η ξ δ Γ x α :
    interp._env η ξ δ (binder_insert x α Γ)
    = binder_insert x V⟦ η ; ξ ; δ ; α ⟧ (interp._env η ξ δ Γ).
  Proof. destruct x as [|x]=> //=. by rewrite /interp._env fmap_insert. Qed.

End interp_properties.


(** * Interpretation of The Weakening Relation. *)

(** Definitions. *)

Module interp_le.

  (* Interpretation of a disjointness context. *)
  Definition _disj_ctx `{!heapG Σ}
    (η : list (sem_ty Σ))
    (ξ : list (sem_row Σ))
    (δ : gmap eff_name eff_label)
    (D : le.disj_ctx) : iProp Σ :=
    labeled_effects.Eff
      ((δ !!!.) <$> (elements (dom D))) ∗
    ⌜ map_Forall (λ (s : eff_name) '(ss, js),
        (δ !!! s) ∉ ((δ !!!.) <$> (elements ss)) ∧
        (δ !!! s) ∉ R⟦ η ; ξ ; δ ; RVar <$> elements js ⟧.*1
      ) D ⌝.

  (* Interpretation of the weakening relation on types. *)
  Definition _ty `{!heapG Σ}
    (η : list (sem_ty Σ))
    (ξ : list (sem_row Σ))
    (δ : gmap eff_name eff_label)
    (D : le.disj_ctx) (α α' : ty) : iProp Σ :=
    _disj_ctx η ξ δ D -∗
      (∀ v, V⟦ η ; ξ ; δ ; α ⟧ v -∗ V⟦ η ; ξ ; δ ; α' ⟧ v).

  (* Interpretation of the weakening relation on rows. *)
  Definition _row `{!heapG Σ}
    (η : list (sem_ty Σ))
    (ξ : list (sem_row Σ))
    (δ : gmap eff_name eff_label)
    (D : le.disj_ctx) (b : bool) (ρ ρ' : row eff_sig) : iProp Σ :=
    let E  : eff_list Σ := R⟦ η ; ξ ; δ ; ρ  ⟧ in
    let E' : eff_list Σ := R⟦ η ; ξ ; δ ; ρ' ⟧ in
    _disj_ctx η ξ δ D -∗
      (EFF E ⊑ EFF E')%ieff               ∧
      (EffLabels E'.*1 -∗ EffLabels E.*1) ∧
      (⌜ b = false → E.*1 ⊆+ E'.*1 ⌝).

  (* Interpretation of the weakening relation on signatures. *)
  Definition _eff_sig `{!heapG Σ}
    (η : list (sem_ty Σ))
    (ξ : list (sem_row Σ))
    (δ : gmap eff_name eff_label)
    (D : le.disj_ctx) (σ σ' : eff_sig) : iProp Σ :=
    let S  : eff_list Σ := S⟦ η ; ξ ; δ ; σ  ⟧ in
    let S' : eff_list Σ := S⟦ η ; ξ ; δ ; σ' ⟧ in
    _disj_ctx η ξ δ D -∗
      (EFF S ⊑ EFF S')%ieff ∧ (⌜ S.*1 = S'.*1 ⌝).

End interp_le.

(* Semantic weakening relation on types. *)
Definition sem_le_ty `{!heapG Σ}
  (D : le.disj_ctx) (α α' : ty) : iProp Σ :=
  tc_opaque (□ (∀ η ξ δ, interp_le._ty η ξ δ D α α'))%I.
Notation "D ⊨ α ≤T α'" := (sem_le_ty D α α')
  (at level 74, α, α' at next level).

(* Semantic weakening relation on rows. *)
Definition sem_le_row `{!heapG Σ}
  (D : le.disj_ctx) (b : bool) (ρ ρ' : row eff_sig) : iProp Σ :=
  tc_opaque (□ (∀ η ξ δ, interp_le._row η ξ δ D b ρ ρ'))%I.
Notation "D ⊨ ρ ≤R ρ' @ b" := (sem_le_row D b ρ ρ')
  (at level 74, ρ, ρ', b at next level).

(* Semantic weakening relation on effect signatures. *)
Definition sem_le_eff_sig `{!heapG Σ}
  (D : le.disj_ctx) (σ σ' : eff_sig) : iProp Σ :=
  tc_opaque (□ (∀ η ξ δ, interp_le._eff_sig η ξ δ D σ σ'))%I.
Notation "D ⊨ σ ≤S σ'" := (sem_le_eff_sig D σ σ')
  (at level 74, σ, σ' at next level).


(** * Interpretation of Typing Judgments. *)

(** Definitions. *)

(* Semantic typing contexts. *)
Definition env_sem_typed `{!heapG Σ}
    (Γ : gmap string (sem_ty Σ))
    (vs : gmap string val) : iProp Σ :=
  ([∗ map] i ↦ A;v ∈ Γ; vs, sem_ty_car A v)%I.
Instance: Params (@env_sem_typed) 3 := {}.

(* Semantic typing judgment. *)
Definition sem_typed `{!heapG Σ}
  (Δ  : gmap eff_name ())
  (Γ  : gmap string ty)
  (a  : att)
  (e  : expr)
  (ρ  : row eff_sig)
  (α  : ty) : iProp Σ :=
  tc_opaque (
    □ (∀ (η : list (sem_ty Σ))
         (ξ : list (sem_row Σ))
         (δ : gmap eff_name eff_label)
         (vs : gmap string val),

           ⌜ dom Δ ⊆ dom δ ⌝ -∗
             env_sem_typed (interp._env η ξ δ Γ) vs -∗

             let ss := (LitV ∘ LitLoc) <$> δ in
             let e' := (subst_map V vs
                       (subst_map L ss e)) in

             (* The closed program [e'] either    *)
             MWP e' @ a (* performs an effect     *)
                        <| R⟦ η ; ξ ; δ ; ρ ⟧ |>

                        (* or terminates normally. *)
                        {{ V⟦ η ; ξ ; δ ; α ⟧ }}))%I.

Instance: Params (@sem_typed) 6 := {}.
Notation "Δ .| Γ '.|={' a '}=' e : ρ : α" := (sem_typed Δ Γ a e ρ α)
  (at level 74, Γ, a, e, ρ, α at next level) : bi_scope.

(* Semantic typing judgment of values. *)
Definition sem_val_typed `{!heapG Σ}
  (v : val) (α : ty) : iProp Σ :=
  tc_opaque (∀ η ξ δ, interp._ty η ξ δ α v)%I.
Instance: Params (@sem_val_typed) 2 := {}.
Notation "⊨ᵥ v : α" := (sem_val_typed v α)
  (at level 20, v, α at next level) : bi_scope.
Arguments sem_val_typed : simpl never.


(** Properties of the semantic typing judgment. *)

Section sem_typed_properties.
  Context `{!heapG Σ}.
  Implicit Types A B : sem_ty Σ.
  Implicit Types C : sem_ty Σ → sem_ty Σ.

  (** Instances of the predicate [Persistent]. *)

  (* The semantic typing judgments are persistent assertions. *)

  Section sem_typed_persistent.

    Global Instance sem_typed_persistent Δ Γ a e ρ α :
      Persistent (sem_typed Δ Γ a e ρ α).
    Proof. rewrite /sem_typed /=. apply _. Qed.
    Global Instance sem_val_typed_persistent v α :
      Persistent (⊨ᵥ v : α).
    Proof. rewrite /sem_val_typed /=. apply _. Qed.
    Global Instance env_sem_typed_persistent Δ Γ :
      Persistent (env_sem_typed Δ Γ).
    Proof. rewrite /env_sem_typed /=. apply _. Qed.

  End sem_typed_persistent.


  (** Types. *)

  (* Properties of the interpretation of types. *)

  Section types.

    Lemma interp_ty_vars_ty η ξ δ s l α :
      s ∉ vars._ty α →
        V⟦ η ; ξ ; (<[s:=l]>δ) ; α ⟧ = V⟦ η ; ξ ; δ ; α ⟧.
    Proof.
      revert α η ξ.
      apply (special_ty_ind (λ α, ∀ η ξ, _ → _ = _)); try done.
      - intros α IH η ξ Hlen. by rewrite //= IH.
      - intros α IH η ξ Hlen. by rewrite //= IH.
      - intros α β IHα IHβ η ξ Hlen. rewrite //=.
        by rewrite IHα; [rewrite IHβ|]; set_solver.
      - intros α β IHα IHβ η ξ Hlen. rewrite //=.
        by rewrite IHα; [rewrite IHβ|]; set_solver.
      - intros t α IH η ξ Hvars. simpl.
        destruct t; f_equal;
        apply functional_extensionality=>?;
        by rewrite IH.
      - intros a α ρ β IHα IHρ IHβ η ξ Hvars. simpl.
        rewrite IHα; [|set_solver].
        rewrite IHβ; [|set_solver]. f_equiv.
        revert Hvars. rewrite //= !not_elem_of_union.
        intros [[Hvarsα Hvarsρ] Hvarsβ].
        induction ρ as [|s' ρ IH]; [done|].
        simpl. inversion IHρ. revert IH Hvarsρ.
        rewrite /interp._row_pre /vars._row_pre //=.
        intros IH Hvarsρ.
        f_equal; [|apply IH; try set_solver].
        destruct s'; simplify_eq; [|done].
        clear H2. destruct H1 as [Hτ Hτ0]. simpl.
        rewrite Hτ0; [|set_solver].
        rewrite Hτ; [|set_solver]. simpl in Hvarsρ.
        by rewrite lookup_total_insert_ne; [|set_solver].
    Qed.

  End types.


  (** Signatures. *)

  (* Properties of the interpretation of effect signatures. *)

  Section signature.

    Lemma SIG_le_bottom η ξ δ α :
      ⊢ (SIG V⟦ η ; ξ ; δ ; ⊥ ⟧ V⟦ η ; ξ ; δ ; α ⟧ ⊑ ⊥)%ieff.
    Proof.
      iIntros "!#" (v ϕ). rewrite SIG_agreement.
      by iIntros "[%a (_ & HFalse & _)]".
    Qed.

    Lemma SIG_mono A A' B B' :
      □ (∀ v, A v -∗ A' v) -∗
        □ (∀ v, B' v -∗ B v) -∗
          (SIG A B ⊑ SIG A' B')%ieff.
    Proof.
      iIntros "#HA' #HB !#" (v Φ).
      rewrite !SIG_agreement.
      iIntros "[%a (-> & Ha & #Hb)]".
      iExists a. iSplit; [done|iSplit].
      - by iApply "HA'".
      - iIntros "!#" (b) "Hb'".
        by iApply "Hb"; iApply "HB".
    Qed.

  End signature.


  (** Rows. *)

  (* Properties of the interpretation of rows. *)

  Section row.

    Lemma interp_row_cons ρ η ξ δ s :
       R⟦ η ; ξ; δ; s :: ρ ⟧ =
         (interp._eff_sig η ξ δ s) ++ R⟦ η ; ξ; δ; ρ ⟧.
    Proof. by rewrite /interp._row //=. Qed.

    Lemma interp_row_submseteq ρ ρ' η ξ δ :
      ρ ⊆+ ρ' →
        R⟦ η ; ξ; δ; ρ ⟧ ⊆+ R⟦ η ; ξ; δ; ρ' ⟧.
    Proof.
      induction 1; first done; try rewrite !interp_row_cons.
      - by apply submseteq_app.
      - by apply submseteq_app_middle.
      - by apply (submseteq_app []); [apply submseteq_nil_l|].
      - by apply (submseteq_trans _ R⟦ η ; ξ; δ; l2 ⟧).
    Qed.

    Lemma interp_row_app ρ ρ' η ξ δ :
      R⟦ η ; ξ; δ; ρ ++ ρ' ⟧ = R⟦ η ; ξ; δ; ρ ⟧ ++ R⟦ η ; ξ; δ; ρ' ⟧.
    Proof. by rewrite /interp._row /interp._row_pre fmap_app concat_app. Qed.

    Lemma interp_row_subseteq ρ ρ' η ξ δ :
      ρ ⊆ ρ' →
        R⟦ η ; ξ; δ; ρ ⟧ ⊆ R⟦ η ; ξ; δ; ρ' ⟧.
    Proof.
      intros Hincl.
      by apply concat_subseteq, list_fmap_subseteq.
    Qed.

    Lemma interp_row_incl s ρ η ξ δ :
      s ∈ ρ →
        (interp._eff_sig η ξ δ s) ⊆ R⟦ η ; ξ; δ; ρ ⟧.
    Proof.
      intro Helem_of. apply submseteq_subseteq.
      transitivity R⟦ η ; ξ ; δ ; [s] ⟧.
      - by rewrite /interp._row /interp._row_pre //= app_nil_r.
      - apply interp_row_submseteq.
        by rewrite singleton_submseteq_l.
    Qed.

    Lemma interp_row_elem_of s α β ρ η ξ δ :
      ESig s α β ∈ ρ →
        (δ !!! s, SIG V⟦ η ; ξ; δ; α ⟧ V⟦ η ; ξ; δ; β ⟧) ∈ R⟦ η ; ξ; δ; ρ ⟧.
    Proof.
      intro Hin.
      specialize (interp_row_incl  _ _ η ξ δ Hin).
      set_solver.
    Qed.

    Lemma interp_row_vars_row η ξ δ s l ρ :
      s ∉ vars._row ρ →
        R⟦ η ; ξ ; (<[s:=l]>δ) ; ρ ⟧ = R⟦ η ; ξ ; δ ; ρ ⟧.
    Proof.
      induction ρ as [|s' ρ]; [done|].
      rewrite !interp_row_cons //= not_elem_of_union.
      intros [Hnotin_s' Hnotin_ρ].
      rewrite IHρ; [|done]. f_equal.
      destruct s'; simpl; [|done].
      rewrite !interp_ty_vars_ty; try set_solver.
      by rewrite lookup_total_insert_ne; [|set_solver].
    Qed.

    Lemma elem_of_interp_row η ξ δ (lΨ : eff_label * pure_iEff Σ) ρ :
      lΨ ∈ R⟦ η ; ξ ; δ ; ρ ⟧ →
        ∃ σ, σ ∈ ρ ∧ lΨ ∈ S⟦ η ; ξ ; δ ; σ ⟧.
    Proof.
      rewrite /interp._row /interp._row_pre.
      rewrite elem_of_list_In in_concat.
      intros [E [HE Hl]]. revert HE Hl.
      rewrite -!elem_of_list_In elem_of_list_fmap.
      intros [σ [-> Hσ]] Hin'. by exists σ.
    Qed.

    Lemma not_elem_of_interp_row η ξ δ (l : eff_label) ρ :
      l ∉ (δ !!!.) <$> elements (le.conc_sigs ρ)  →
        l ∉ R⟦ η ; ξ ; δ ; RVar <$> elements (le.abst_sigs ρ) ⟧.*1  →
          l ∉ R⟦ η ; ξ ; δ ; ρ ⟧.*1.
    Proof.
      intros Hconc Habst.
      rewrite elem_of_list_fmap. intros [(l', Ψ) [-> Hl]].
      destruct (elem_of_interp_row _ _ _ _ _ Hl) as [σ [Hσ Hs]].
      destruct (elem_of_row _ _ Hσ) as [[s' [α [β [-> Hs']]]]|[i [-> Hi]]].
      - simpl in Hs. revert Hs.
        rewrite elem_of_list_singleton.
        inversion 1. apply Hconc.
        rewrite elem_of_list_fmap. exists s'.
        by split; [| rewrite gmultiset_elem_of_elements].
      - simpl in Hs. apply Habst. revert Hs.
        rewrite !elem_of_list_fmap. intros Hin'.
        exists (l', Ψ). split; [done|].
        rewrite /interp._row /interp._row_pre.
        rewrite elem_of_list_In in_concat.
        exists (ξ !!! i). rewrite -!elem_of_list_In. split; [|done].
        rewrite elem_of_list_fmap.
        exists (RVar i). by set_solver.
    Qed.

    Lemma Eff_row_cons η ξ δ s α β ρ :
      (δ !!! s) ↦□ #() -∗
        labeled_effects.Eff (R⟦ η ; ξ ; δ ; ρ ⟧.*1) -∗
          labeled_effects.Eff (R⟦ η ; ξ ; δ ; ESig s α β :: ρ ⟧.*1).
    Proof. iIntros "#Hs #Hρ". by iSplit. Qed.

    Lemma NoDup_row_cons η ξ δ s α β ρ :
      (δ !!! s) ∉ R⟦ η ; ξ ; δ ; ρ ⟧.*1 →
        NoDup (R⟦ η ; ξ ; δ ; ρ ⟧.*1) →
          NoDup (R⟦ η ; ξ ; δ ; ESig s α β :: ρ ⟧.*1).
    Proof. intros Hnotin HNP. by apply NoDup_cons. Qed.

  End row.


  (** Environments. *)

  (* Properties of the interpretation of environments. *)

  Section environment.

    Lemma env_sem_typed_lookup Γ vs x A :
      Γ !! x = Some A →
      env_sem_typed Γ vs -∗ ∃ v, ⌜ vs !! x = Some v ⌝ ∧ A v.
    Proof.
      iIntros (HΓx) "HΓ". rewrite /env_sem_typed.
      by iApply (big_sepM2_lookup_l with "HΓ").
    Qed.
    Lemma env_sem_typed_insert Γ vs x A v :
      A v -∗ env_sem_typed Γ vs -∗
      env_sem_typed (binder_insert x A Γ) (binder_insert x v vs).
    Proof.
      destruct x as [|x]=> /=; first by auto. rewrite /env_sem_typed.
      iIntros "#HA #HΓ". by iApply (big_sepM2_insert_2 with "[] HΓ").
    Qed.

    Lemma env_sem_typed_empty : ⊢ env_sem_typed ∅ ∅.
    Proof. iIntros "". rewrite /env_sem_typed. by rewrite big_sepM2_empty. Qed.
    Lemma env_sem_typed_empty_l_inv vs : env_sem_typed ∅ vs -∗ ⌜ vs = ∅ ⌝.
    Proof. by iIntros "?"; iApply big_sepM2_empty_r. Qed.

    Lemma interp_env_dom η ξ δ Γ : dom (interp._env η ξ δ Γ) = dom Γ.
    Proof. by rewrite /interp._env dom_fmap_L. Qed.
    Lemma env_sem_typed_dom Γ vs :
      env_sem_typed Γ vs -∗ ⌜ dom Γ = dom vs ⌝.
    Proof. by iApply big_sepM2_dom. Qed.
    Lemma env_sem_typed_dom' η ξ δ Γ vs :
      env_sem_typed (interp._env η ξ δ Γ) vs -∗ ⌜ dom Γ = dom vs ⌝.
    Proof. rewrite -(interp_env_dom η ξ δ). by apply env_sem_typed_dom. Qed.

    Lemma interp_env_insert η ξ δ x α Γ :
      interp._env η ξ δ (<[x:=α]> Γ) = 
        <[x:=(interp._ty η ξ δ α)]> (interp._env η ξ δ Γ).
    Proof. by rewrite /interp._env fmap_insert. Qed.

    Lemma interp_env_vars_env η ξ δ s l Γ :
      s ∉ vars._env Γ →
        interp._env η ξ (<[s:=l]>δ) Γ = interp._env η ξ δ Γ.
    Proof.
      induction Γ as [|x α Γ Hlkp] using map_ind.
      - by rewrite !interp_env_empty.
      - intros Hnotin.
        destruct (vars_env_insert _ _ _ _ Hlkp Hnotin) as [Hα HΓ].
        rewrite !interp_env_insert IHΓ; [|done].
        by rewrite interp_ty_vars_ty.
    Qed.

  End environment.


  (** Interpretation and lifting. *)

  (* Rewriting rules for the interpretation of types with lifting of de
     Bruijn's indices. *)

  Section interp_lift.

    Lemma interp_tvar_lift_ty (n : nat) η ξ δ α :
      (n ≤ length η)%nat →
        V⟦ η ; ξ ; δ ; tvar_lift._ty n α ⟧ = V⟦ (delete n η) ; ξ ; δ ; α ⟧.
    Proof.
      revert α n η ξ δ.
      apply (special_ty_ind (λ α, ∀ n η ξ δ, _ → _ = _)); try done.
      - intros α IH n η ξ δ Hlen. by rewrite //= IH.
      - intros α IH n η ξ δ Hlen. by rewrite //= IH.
      - intros α β IHα IHβ n η ξ δ Hlen. rewrite //=.
        by rewrite IHα; [rewrite IHβ|].
      - intros α β IHα IHβ n η ξ δ Hlen. rewrite //=.
        by rewrite IHα; [rewrite IHβ|].
      - intros t α IH n η ξ δ Hlen. simpl.
        destruct t; simpl; f_equal;
        apply functional_extensionality =>?;
        by (rewrite //= IH; [|simpl; lia]).
      - intros n var_ind η ξ δ Hlen. simpl.
        case_decide; simplify_eq/=; try lia.
        * by rewrite lookup_total_delete_lt; [|lia].
        * by rewrite lookup_total_delete_ge; [|lia].
      - intros a α ρ β IHα IHρ IHβ n η ξ δ Hlen. simpl.
        rewrite (IHα _ _ _ _ Hlen).
        rewrite (IHβ _ _ _ _ Hlen).
        f_equal.
        induction ρ as [|s ρ IH] ; [done|].
        inversion IHρ.
        rewrite /interp._row_pre //=.
        f_equal; [|by apply IH].
        destruct s; simplify_eq; [|done].
        clear H2. destruct H1 as [Hτ Hτ0]. simpl.
        rewrite (Hτ0 _ _ _ _ Hlen).
        rewrite (Hτ  _ _ _ _ Hlen). done.
    Qed.

    Lemma interp_rvar_lift_ty (n : nat) η ξ δ α :
      (n ≤ length ξ)%nat →
        V⟦ η ; ξ ; δ ; rvar_lift._ty n α ⟧ = V⟦ η ; (delete n ξ) ; δ ; α ⟧.
    Proof.
      revert α n η ξ δ.
      apply (special_ty_ind (λ α, ∀ n η ξ δ, _ → _ = _)); try done.
      - intros α IH n η ξ δ Hlen. by rewrite //= IH.
      - intros α IH n η ξ δ Hlen. by rewrite //= IH.
      - intros α β IHα IHβ n η ξ δ Hlen. rewrite //=.
        by rewrite IHα; [rewrite IHβ|].
      - intros α β IHα IHβ n η ξ δ Hlen. rewrite //=.
        by rewrite IHα; [rewrite IHβ|].
      - intros t α IH n η ξ δ Hlen. simpl.
        destruct t; simpl; f_equal;
        apply functional_extensionality=>?;
        by (rewrite //= IH; [|simpl; lia]).
      - intros a α ρ β IHα IHρ IHβ n η ξ δ Hlen. simpl.
        rewrite (IHα _ _ _ _ Hlen).
        rewrite (IHβ _ _ _ _ Hlen).
        f_equal.
        induction ρ as [|s ρ IH] ; [done|].
        inversion IHρ.
        rewrite /interp._row_pre //=.
        f_equal; [|by apply IH].
        destruct s; simplify_eq.
        + clear H2. destruct H1 as [Hτ Hτ0]. simpl.
          rewrite (Hτ0 _ _ _ _ Hlen).
          rewrite (Hτ  _ _ _ _ Hlen). done.
        + simpl. case_decide; simplify_eq/=; try lia.
          * by rewrite lookup_total_delete_lt; [|lia].
          * by rewrite lookup_total_delete_ge; [|lia].
    Qed.

    Lemma interp_tvar_lift_ty_0 A η ξ δ α :
      V⟦ (A :: η) ; ξ ; δ ; tvar_lift._ty 0 α ⟧ = V⟦ η ; ξ ; δ ; α ⟧.
    Proof. apply interp_tvar_lift_ty; simpl; lia. Qed.

    Lemma interp_rvar_lift_ty_0 E η ξ δ α :
      V⟦ η ; (E :: ξ) ; δ ; rvar_lift._ty 0 α ⟧ = V⟦ η ; ξ ; δ ; α ⟧.
    Proof. apply interp_rvar_lift_ty; simpl; lia. Qed.

    Lemma interp_env_tvar_lift_ty (n : nat) η ξ δ Γ :
      (n ≤ length η)%nat →
        interp._env η ξ δ (tvar_lift._ty n <$> Γ) =
        interp._env (delete n η) ξ δ Γ.
    Proof.
      intros. rewrite /interp._env -map_fmap_compose.
      apply map_fmap_ext=> x A _ /=. by rewrite interp_tvar_lift_ty.
    Qed.

    Lemma interp_env_rvar_lift_ty (n : nat) η ξ δ Γ :
      (n ≤ length ξ)%nat →
        interp._env η ξ δ (rvar_lift._ty n <$> Γ) =
        interp._env η (delete n ξ) δ Γ.
    Proof.
      intros. rewrite /interp._env -map_fmap_compose.
      apply map_fmap_ext=> x A _ /=. by rewrite interp_rvar_lift_ty.
    Qed.

    Lemma interp_env_tvar_lift_ty_0 A η ξ δ (Γ : gmap string ty) :
      interp._env (A :: η) ξ δ (tvar_lift._ty 0 <$> Γ) = interp._env η ξ δ Γ.
    Proof. apply interp_env_tvar_lift_ty; simpl; lia. Qed.

    Lemma interp_env_rvar_lift_ty_0 E η ξ δ (Γ : gmap string ty) :
      interp._env η (E :: ξ) δ (rvar_lift._ty 0 <$> Γ) = interp._env η ξ δ Γ.
    Proof. apply interp_env_rvar_lift_ty; simpl; lia. Qed.

    Lemma interp_rvar_lift_row (n : nat) η ξ δ ρ :
      (n ≤ length ξ)%nat →
        R⟦ η ; ξ ; δ ; rvar_lift._row n ρ ⟧ = R⟦ η ; (delete n ξ) ; δ ; ρ ⟧.
    Proof.
      intro Hlen.
      induction ρ as [|s ρ IH]; [done|]. revert IH.
      rewrite /interp._row /interp._row_pre /rvar_lift._row //=.
      intro IH. rewrite IH. f_equal.
      destruct s; simpl.
      + by rewrite !interp_rvar_lift_ty.
      + simpl. case_decide; simplify_eq/=; try lia.
        * by rewrite lookup_total_delete_lt; [|lia].
        * by rewrite lookup_total_delete_ge; [|lia].
    Qed.

    Lemma interp_tvar_lift_row (n : nat) η ξ δ ρ :
      (n ≤ length η)%nat →
        R⟦ η ; ξ ; δ ; tvar_lift._row n ρ ⟧ = R⟦ (delete n η) ; ξ ; δ ; ρ ⟧.
    Proof.
      intro Hlen.
      induction ρ as [|s ρ IH]; [done|]. revert IH.
      rewrite /interp._row /interp._row_pre /rvar_lift._row //=.
      intro IH. rewrite IH. f_equal.
      destruct s; simpl; [|done].
      by rewrite !interp_tvar_lift_ty.
    Qed.

    Lemma interp_tvar_lift_row_0 A η ξ δ ρ :
      R⟦ (A :: η) ; ξ ; δ ; tvar_lift._row 0 ρ ⟧ = R⟦ η ; ξ ; δ ; ρ ⟧.
    Proof. apply interp_tvar_lift_row; simpl; lia. Qed.

    Lemma interp_rvar_lift_row_0 E η ξ δ ρ :
      R⟦ η ; (E :: ξ) ; δ ; rvar_lift._row 0 ρ ⟧ = R⟦ η ; ξ ; δ ; ρ ⟧.
    Proof. apply interp_rvar_lift_row; simpl; lia. Qed.

  End interp_lift.


  (** Interpretation and substitution. *)

  (* Rewriting rules for the interpretation of types after the substitution of
     de Bruijn's indices. *)

  Section interp_subst.

    Lemma interp_rvar_subst_ty (i : nat) ρ η ξ δ α :
      (i ≤ length ξ)%nat →
        V⟦ η ; ξ ; δ ; rvar_subst._ty i ρ α ⟧ =
          V⟦ η ; (take i ξ ++ interp._row η ξ δ ρ :: drop i ξ) ; δ ; α ⟧.
    Proof.
      revert α i ρ η ξ δ.
      apply (special_ty_ind (λ α, ∀ i ρ η ξ δ, _ → _ = _)); try done.
      - intros α IH n ρ η ξ δ Hlen. by rewrite //= IH.
      - intros α IH n ρ η ξ δ Hlen. by rewrite //= IH.
      - intros α β IHα IHβ n ρ η ξ δ Hlen. rewrite //=.
        by rewrite IHα; [rewrite IHβ|].
      - intros α β IHα IHβ n ρ η ξ δ Hlen. rewrite //=.
        by rewrite IHα; [rewrite IHβ|].
      - intros t α IH i ρ η ξ δ Hlen. simpl.
        destruct t; simpl; f_equal;
        apply functional_extensionality=>X.
        rewrite IH; [|simpl; lia]. simpl.
        by rewrite interp_tvar_lift_row_0.
        rewrite IH; [|simpl; lia].
        by rewrite interp_rvar_lift_row_0.
      - intros a α ρ β IHα IHρ IHβ i ρ' η ξ δ Hlen. simpl.
        rewrite (IHα _ _ _ _ _ Hlen).
        rewrite (IHβ _ _ _ _ _ Hlen).
        f_equal.
        induction ρ as [|s ρ IH] ; [done|].
        simpl. inversion IHρ. revert IH.
        rewrite /interp._row_pre /rvar_subst._row_pre //=.
        intro IH. f_equal. rewrite -IH; [|done].
        destruct s; simplify_eq.
        + clear H2. destruct H1 as [Hτ Hτ0]. simpl.
          rewrite (Hτ0 _ _ _ _ _ Hlen).
          rewrite (Hτ  _ _ _ _ _ Hlen). done.
        + simpl. rewrite /rvar_subst._row_pre //=.
          case_decide; simplify_eq/=; try lia.
          * rewrite lookup_total_app_l; last (rewrite take_length; lia).
            rewrite lookup_total_take; last lia. done.
          * case_decide; simplify_eq/=; try lia.
            -- rewrite list_lookup_total_middle;
               last (rewrite take_length; lia).
               by rewrite -concat_app fmap_app.
            -- rewrite lookup_total_app_r; last (rewrite take_length; lia).
               rewrite take_length lookup_total_cons_ne_0; last lia.
               rewrite (_ : Init.Nat.pred n =
                 (i + Init.Nat.pred (n - i `min` length ξ))%nat); [|lia].
               by rewrite lookup_total_drop.
    Qed.

    Lemma interp_tvar_subst_ty (i : nat) η β ξ δ α :
      (i ≤ length η)%nat →
        V⟦ η ; ξ ; δ ; tvar_subst._ty i β α ⟧ =
          V⟦ (take i η ++ interp._ty η ξ δ β :: drop i η) ; ξ ; δ ; α ⟧.
    Proof.
      revert α i β η ξ δ.
      apply (special_ty_ind (λ α, ∀ i β η ξ δ, _ → _ = _)); try done.
      - intros α IH n β η ξ δ Hlen. by rewrite //= IH.
      - intros α IH n β η ξ δ Hlen. by rewrite //= IH.
      - intros α' β' IHα' IHβ' n β η ξ δ Hlen. rewrite //=.
        by rewrite IHα'; [rewrite IHβ'|].
      - intros α' β' IHα' IHβ' n β η ξ δ Hlen. rewrite //=.
        by rewrite IHα'; [rewrite IHβ'|].
      - intros t α IH i ρ η ξ δ Hlen. simpl.
        destruct t; simpl; f_equal;
        apply functional_extensionality=>X;
        (rewrite IH; [|simpl; lia]).
        by rewrite interp_tvar_lift_ty_0.
        by rewrite interp_rvar_lift_ty_0.
      - intros n i β η ξ δ Hlen. simpl.
        case_decide; simplify_eq/=; try lia.
        + rewrite lookup_total_app_l; last (rewrite take_length; lia).
          rewrite lookup_total_take; last lia. done.
        + case_decide; simplify_eq/=; try lia.
          * by rewrite list_lookup_total_middle;
            last (rewrite take_length; lia).
          * rewrite lookup_total_app_r; last (rewrite take_length; lia).
            rewrite take_length lookup_total_cons_ne_0; last lia.
            rewrite (_ : Init.Nat.pred n =
              (i + Init.Nat.pred (n - i `min` length η))%nat); [|lia].
            by rewrite lookup_total_drop.
      - intros a α' ρ β' IHα' IHρ IHβ' i β η ξ δ Hlen. simpl.
        rewrite (IHα' _ _ _ _ _ Hlen).
        rewrite (IHβ' _ _ _ _ _ Hlen).
        f_equal.
        induction ρ as [|s ρ IH] ; [done|].
        simpl. inversion IHρ. revert IH.
        rewrite /interp._row_pre /rvar_subst._row_pre //=.
        intro IH. rewrite -IH; [|done].
        destruct s; simplify_eq; [|done].
        clear H2. destruct H1 as [Hτ Hτ0]. simpl.
        rewrite (Hτ0 _ _ _ _ _ Hlen).
        rewrite (Hτ  _ _ _ _ _ Hlen). done.
    Qed.

    Lemma interp_rvar_subst_ty_0 ρ η ξ δ α :
      V⟦ η ; ξ ; δ ; rvar_subst._ty O ρ α ⟧ =
        V⟦ η ; (interp._row η ξ δ ρ) :: ξ ; δ ; α ⟧.
    Proof. apply interp_rvar_subst_ty; simpl; lia. Qed.

    Lemma interp_tvar_subst_ty_0 β η ξ δ α :
      V⟦ η ; ξ ; δ ; tvar_subst._ty O β α ⟧ =
        V⟦ (interp._ty η ξ δ β) :: η ; ξ ; δ ; α ⟧.
    Proof. apply interp_tvar_subst_ty; simpl; lia. Qed.

  End interp_subst.

End sem_typed_properties.


(** Properties of the semantic weakening relation. *)

Section sem_le_properties.
  Context `{!heapG Σ}.

  Implicit Types (η : list (sem_ty Σ))
                 (ξ : list (sem_row Σ))
                 (δ : gmap eff_name eff_label)
                 (D : le.disj_ctx).


  (** Instances of the predicate [Persistent]. *)

  Section sem_le_persistent.

    Global Instance interp_le_disj_ctx_persistent η ξ δ D :
      Persistent (interp_le._disj_ctx η ξ δ D).
    Proof. apply _. Qed.
    Global Instance sem_le_ty_persistent D α α' :
      Persistent (D ⊨ α ≤T α').
    Proof. rewrite /sem_le_ty /=. apply _. Qed.
    Global Instance sem_le_eff_sig_persistent D σ σ' :
      Persistent (D ⊨ σ ≤S σ').
    Proof. rewrite /sem_le_eff_sig /=. apply _. Qed.
    Global Instance sem_le_row_persistent D b ρ ρ' :
      Persistent (D ⊨ ρ ≤R ρ' @ b).
    Proof. rewrite /sem_le_row /=. apply _. Qed.

  End sem_le_persistent.


  (** General properties. *)

  (* The empty context is a valid disjointness context. *)
  Lemma interp_le_disj_ctx_empty η ξ δ :
    ⊢ interp_le._disj_ctx η ξ δ ∅.
  Proof. by iSplit; unfold labeled_effects.Eff. Qed.

  (* An effect name [s] in the domain of a disjointness context
     is associated to a valid dynamic instance [δ !!! s]. *)
  Lemma disj_ctx_dom η ξ δ D s :
    s ∈ dom D →
      interp_le._disj_ctx η ξ δ D -∗
        (δ !!! s) ↦□ #().
  Proof.
    rewrite -elem_of_elements.
    iIntros (Hs) "[HD _]".
    rewrite /labeled_effects.Eff.
    destruct (elem_of_list_lookup_1 _ _ Hs) as [i Hi].
    iApply (big_sepL_lookup _ _ i with "HD").
    by rewrite list_lookup_fmap Hi.
  Qed.

  (* This lemma is key in proving the soundness of the rule [TArrow_Abs_le].
     Indeed, it allows us to prove the _well-formedness_ of the row
     [EAbs s :: ρ] under the assumption that [ρ] is well-formed and under the
     assumptions of the rule [TArrow_Abs_le].

     By "well-formedness of a row [ρ]", we mean that the following
     assertion holds:

       EffLabels (R⟦ η ; ξ ; δ ; ρ ⟧.*1)

   *)
  Lemma EffLabels_row_cons η ξ δ s α β ρ D ss js :
    D !! s = Some (ss, js) →
      le.conc_sigs ρ ⊆ ss →
        le.abst_sigs ρ ⊆ js →
          interp_le._disj_ctx η ξ δ D -∗
            EffLabels (R⟦ η ; ξ ; δ ; ρ ⟧.*1) -∗
              EffLabels (R⟦ η ; ξ ; δ ; ESig s α β :: ρ ⟧.*1).
  Proof.
    set (E  := R⟦ η; ξ; δ; RVar <$> elements (le.abst_sigs ρ) ⟧.*1).
    set (E' := R⟦ η; ξ; δ; RVar <$> elements js ⟧.*1).

    iIntros (Hlkp Hinclss Hincljs) "#HD [%HND #Hρ]".

    (* We must show that the dynamic instance of [s] does not
       belong to any of the labels in the interpretation of [ρ]:

         (δ !!! s) ∉ R⟦ η ; ξ ; δ ; ρ ⟧.*1.

       We achieve this in two parts:
       (1) (δ !!! s) does not belong to the interpretation of the
           abstract signatures in [ρ].
       (2) (δ !!! s) does not belong to the interpretation of the
           concrete signatures in [ρ].

       Both parts rely on the disjointness information stored in [D].
       Moreover, to prove (1), we use the hypothesis [le.abst_sigs ρ ⊆ js].
       And, to prove (2), we use the hypothesis [le.conc_sigs ρ ⊆ ss].
    *)

    (* Statement and proof of (1). *)
    iAssert (⌜ (δ !!! s) ∉ E ⌝)%I as "%Hnotin_E".
    { iDestruct "HD" as "[_ %Hm]". iPureIntro.
      specialize (map_Forall_lookup_1 _ _ _ _ Hm Hlkp) as Hnotin'.
      intro Hin. apply Hnotin'.
      revert Hin. cut (E ⊆ E'); [auto|].
      apply list_fmap_subseteq; [done|].
      apply interp_row_subseteq, list_fmap_subseteq; [done|].
      set_solver.
    }

    (* Statement and proof of (2). *)
    iAssert (⌜ (δ !!! s) ∉ (δ !!!.) <$> elements (le.conc_sigs ρ) ⌝)%I
      as "%Hnotin_csρ".
    { iDestruct "HD" as "[_ %Hm]". iPureIntro.
      specialize (map_Forall_lookup_1 _ _ _ _ Hm Hlkp) as [Hnotin' _].
      intros Hin. apply Hnotin'.
      apply (list_fmap_subseteq (δ !!!.) (δ !!!.) (λ _, eq_refl) _ _
            (submseteq_subseteq _ _
            (gmultiset_elements_submseteq _ _ Hinclss))).
      done.
    }

    iSplit.
    - iPureIntro. 
      by apply NoDup_row_cons; [apply not_elem_of_interp_row|].
    - iApply Eff_row_cons; [|done].
      iApply (disj_ctx_dom with "HD").
      by rewrite elem_of_dom; exists (ss, js).
  Qed.

  (* The disjointness context obtained from a well-formed row [ρ']
     is _valid_. A valid disjointness context [D] is one of which
     the assertion [interp_le._disj_ctx η ξ δ D] holds. *)
  Lemma interp_le_disj_ctx_row η ξ δ ρ' :
    EffLabels (R⟦ η ; ξ ; δ ; ρ' ⟧.*1) -∗
      interp_le._disj_ctx η ξ δ (le.row_to_disj_ctx ρ').
  Proof.
    iIntros "[%HND #Hρ']". iSplit.
    - rewrite dom_row_to_disj_ctx.
      iApply (Eff_incl with "Hρ'").
      intros l Hl. revert Hl.
      rewrite elem_of_list_fmap.
      intros [s [-> Hs]]. revert Hs.
      rewrite elem_of_elements gmultiset_elem_of_dom. intros Hin.
      destruct (elem_of_conc_sigs _ _ Hin)  as [α  [β  Hsig]].
      rewrite elem_of_list_fmap.
      exists (δ !!! s, (SIG V⟦ η; ξ; δ; α ⟧ V⟦ η; ξ; δ; β ⟧)).
      split; [done|]. by apply interp_row_elem_of.
    - iPureIntro. apply map_Forall_lookup_2.
      intros s (ss, js) Hlkp.
      destruct (lookup_row_to_disj_ctx _ _ _ _ Hlkp) as (Hs & -> & ->).
      split.
      + rewrite elem_of_list_fmap.
        intros [s' [Heq Hin']]. revert Hin'.
        revert Hs. rewrite gmultiset_elem_of_dom. intros Hs.
        rewrite gmultiset_elem_of_elements. intros Hs'.
        assert (list_to_set_disj [s; s'] ⊆ le.conc_sigs ρ')
          as [ts [Hlen Hsubm]]%incluced_in_conc_sigs.
        { rewrite //= (gmultiset_disj_union_difference' _ _ Hs).
          apply gmultiset_disj_union_mono. done.
          rewrite gmultiset_disj_union_right_id.
          by rewrite gmultiset_singleton_subseteq_l.
        }
        assert (∃ t t', ts = [t; t']) as [(α, β) [(α', β') ->]].
        { by apply length_2_inv. }
        assert (∃ ρ, ρ' ≡ₚ (ESig s α β) :: (ESig s' α' β') :: ρ)
          as [ρ Hρ%Permutation_sym].
       { by apply (submseteq_Permutation _ _ Hsubm). }
        specialize (submseteq_NoDup_fmap fst
          R⟦ η; ξ; δ; (ESig s α β :: ESig s' α' β' :: ρ)⟧ _
          (interp_row_submseteq _ _ _ _ _ (Permutation_submseteq _ _ Hρ))
          HND).
        simpl. rewrite NoDup_cons. intros [H _]. revert H.
        rewrite not_elem_of_cons. by intros [Hcontradiction _].
      + rewrite elem_of_list_fmap.
        intros [(l, Ψ) [Heq Hin']]. revert Hin'. simpl in Heq. rewrite -Heq.
        rewrite elem_of_list_In in_concat.
        intros [E HE]. revert HE. rewrite -!elem_of_list_In.
        rewrite elem_of_list_fmap. intros [[e [-> He]] Hin].
        revert He. rewrite elem_of_list_fmap.
        intros [j [-> Hj]]. revert Hj.
        rewrite elem_of_elements. intros Hj. revert Hs.
        rewrite gmultiset_elem_of_dom. intros Hs.
        specialize (elem_of_conc_sigs _ _ Hs) as [α [β Hsig]].
        specialize (elem_of_abst_sigs _ _ Hj) as       Hsig'.
        assert (∃ ρ, ρ' ≡ₚ (ESig s α β) :: (RVar j) :: ρ)
          as [ρ Hρ%Permutation_sym].
        { by apply elem_of_Permutation_2. }
        specialize (submseteq_NoDup_fmap fst
          R⟦ η; ξ; δ; (ESig s α β :: RVar j :: ρ)⟧ _
          (interp_row_submseteq _ _ _ _ _ (Permutation_submseteq _ _ Hρ))
          HND).
        simpl. rewrite NoDup_cons. intros [H _]. revert H.
        simpl in Hs. rewrite elem_of_list_fmap. intro H. apply H.
        exists (δ !!! s, Ψ). by set_solver.
  Qed.

  (* The fusion of two valid disjointness contexts is still valid. *)
  Lemma interp_le_disj_ctx_merge η ξ δ D D' :
    interp_le._disj_ctx η ξ δ D -∗
      interp_le._disj_ctx η ξ δ D' -∗
        interp_le._disj_ctx η ξ δ (le.merge_ctx D D').
  Proof.
    iIntros "[#HD %HM] [#HD' %HM']".
    iSplit.
    - rewrite dom_merge_ctx.
      set (domD  := elements (dom D)).
      set (domD' := elements (dom D')).
      set (δ' := (λ (s : eff_name), δ !!! s)).
      iApply (Eff_incl _ (δ' <$> (domD ++ domD'))).
      { by apply list_fmap_subseteq; [|apply elements_subseteq]. }
      rewrite fmap_app. by iApply Eff_app.
    - iPureIntro. apply map_Forall_lookup_2.
      intros s (ss, js). rewrite lookup_union_with.
       case_eq (D  !! s); [intros (ssl, jsl) Hs |intros Hs ]; rewrite Hs; try
      (case_eq (D' !! s); [intros (ssr, jsr) Hs'|intros Hs']; rewrite Hs');
      last done; simpl; injection 1; intros <- <-;
      try specialize (map_Forall_lookup_1 _ _ _ _ HM  Hs)  as [Hcm  Ham];
      try specialize (map_Forall_lookup_1 _ _ _ _ HM' Hs') as [Hcm' Ham'];
      try done.
      split.
      + rewrite gmultiset_union_disj_union.
        rewrite gmultiset_elements_disj_union fmap_app.
        rewrite not_elem_of_app. split; [done|].
        intros Hin. apply Hcm'.
        have Hincl: ((ssr ∖ (ssl ∩ ssr)) ⊆ ssr).
        { by multiset_solver. }
        apply (list_fmap_subseteq (δ !!!.) (δ !!!.) (λ _, eq_refl) _ _
              (submseteq_subseteq _ _
              (gmultiset_elements_submseteq _ _ Hincl))).
        done.
      + intros Hin.
        assert (R⟦ η; ξ; δ; RVar <$> elements (jsl ∪ jsr) ⟧.*1 ⊆
                R⟦ η; ξ; δ; RVar <$> elements jsl ++ elements jsr ⟧.*1)
          as Hincl.
        { apply list_fmap_subseteq; [done|].
          apply interp_row_subseteq, list_fmap_subseteq; [done|].
          by set_solver.
        }
        specialize (Hincl _ Hin) as Hin'. revert Hin'.
        rewrite fmap_app interp_row_app fmap_app elem_of_app.
        by intros [|].
  Qed.

  (* This lemma is key in proving the rule [TArrow_le]. Indeed, it states
     that it is sound to update the current disjointness context with
     the information we learn from the well-formedness of the row [ρ']. *)
  Lemma interp_le_disj_ctx_update η ξ δ D ρ' :
    let D' := le.update_disj_ctx ρ' D in

    EffLabels (R⟦ η ; ξ ; δ ; ρ' ⟧.*1) -∗
      interp_le._disj_ctx η ξ δ D -∗
        interp_le._disj_ctx η ξ δ D'.
  Proof.
    intros D'.
    iIntros "#Hρ' #HD".
    iApply interp_le_disj_ctx_merge; [|auto].
    by iApply interp_le_disj_ctx_row.
  Qed.

End sem_le_properties.
