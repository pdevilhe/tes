(* fundamental.v *)

(* This file contains the proof of the Fundamental Theorem. *)

From logic Require Import tactics.
From effect_system Require Export fundamental_le compatibility.

Section fundamental.
  Context `{!heapG Σ}.

  Theorem fundamental {Δ Γ a e ρ α}
    (Hty: Δ .| Γ .|-{a}- e : ρ : α) : ⊢ (Δ .| Γ .|={a}= e : ρ : α).
  Proof.
    - induction Hty.
      + by iApply Unit_sem_typed.
      + by iApply Bool_sem_typed.
      + by iApply Int_sem_typed.
      + by iApply Nil_sem_typed.
      + by iApply Cons_sem_typed.
      + by iApply Var_sem_typed.
      + by iApply Prod_sem_typed.
      + by iApply Fst_sem_typed.
      + by iApply Snd_sem_typed.
      + by iApply InjL_sem_typed.
      + by iApply InjR_sem_typed.
      + by iApply (Case_sem_typed $! IHHty1 IHHty2).
      + by iApply (ListCase_sem_typed $! IHHty1 IHHty2).
      + by iApply Alloc_sem_typed.
      + by iApply Load_sem_typed.
      + by iApply (Store_sem_typed $! IHHty1 IHHty2).
      + by iApply RLam_sem_typed.
      + by iApply RApp_sem_typed.
      + by iApply TLam_sem_typed.
      + by iApply TApp_sem_typed.
      + by iApply (App_sem_typed $! IHHty1 IHHty2).
      + by iApply (If_sem_typed $! IHHty1 IHHty2).
      + by iApply (BinOp_sem_typed $! IHHty1 IHHty2).
      + by iApply Rec_sem_typed.
      + iApply Mono_sem_typed; try done.
        * by iApply fundamental_le_row.
        * by iApply fundamental_le_ty.
      + by iApply (LetEff_sem_typed H).
      + by iApply Perform_sem_typed.
      + by iApply (SHandle_sem_typed _ _ _ H $! IHHty1 IHHty2).
      + by iApply TryFinally_sem_typed.
      + by iApply DynamicWind_sem_typed.
      + by iApply RemoveEff_sem_typed.
  Qed.

End fundamental.
