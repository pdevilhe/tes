(* lists.v *)

(* This file contains general-purpose lemmas about lists. *)

From stdpp Require Import gmap base list.


(** * Properties of [submseteq]. *)

Section submseteq.

  (* If the list [l] can be obtained by removing elements from [k],
     then [l] is included in [k]. *)
  Lemma submseteq_subseteq {A} (l k : list A) : l ⊆+ k → l ⊆ k.
  Proof. by induction 1; set_solver. Qed.

  (* If [k] does not contain duplicates, then neither does a list [l]
     obtained by removing elements from [k]. *)
  Lemma submseteq_NoDup {A} (l k : list A) : l ⊆+ k → NoDup k → NoDup l.
  Proof.
    induction 1; try auto.
    - inversion 1. apply NoDup_cons.
      split; [|by apply IHsubmseteq].
      intro Hin. apply H3. by apply (submseteq_subseteq _ _ H).
    - inversion 1. inversion H3.
      apply NoDup_cons. split; [set_solver|].
      apply NoDup_cons. split; [set_solver|]. done.
    - inversion 1. by apply IHsubmseteq.
  Qed.

  (* If the range of a function [f] over a list [k] does not contain
     duplicates, then neither does the range of [f] over a list [l]
     obtained by removing elements from [k]. *)
  Lemma submseteq_NoDup_fmap {A B : Type} (f : A → B) (l k : list A) :
    l ⊆+ k → NoDup (f <$> k) → NoDup (f <$> l).
  Proof.
    intros Hsub Hk; induction Hsub; try by auto.
    - simpl; apply NoDup_cons; split.
      + intro Hin. inversion Hk. simplify_eq. apply H1.
        by apply (submseteq_subseteq _ _ (fmap_submseteq f _ _ Hsub)).
      + apply IHHsub. by inversion Hk.
    - revert Hk. rewrite !fmap_cons.
      by apply NoDup_Permutation_proper, Permutation_swap.
    - apply IHHsub. by inversion Hk.
  Qed.

  (* The inclusion of a no-duplicate list [l] into a list [r]
     implies the relation [l ⊆+ r]. *)
  Lemma NoDup_subseteq_submseteq {A : Type} (l k : list A) :
    NoDup l → l ⊆ k → l ⊆+ k.
  Proof.
    revert k; induction l as [|x l]; intros k HD Hincl.
    - by apply submseteq_nil_l.
    - assert (∃ r, k ≡ₚ x :: r) as [r Heq].
      { by rewrite <-elem_of_Permutation; set_solver. }
      apply submseteq_cons_l.
      exists r. split; [done|].
      apply IHl; [by inversion HD|].
      intros y Hin.
      assert (y ≠ x) as Hneq.
      { by inversion HD; intros ->. }
      cut (y ∈ x :: r).
      + by rewrite elem_of_cons; intros [|].
      + by rewrite <-Heq; apply Hincl; set_solver.
  Qed.

  (* If the list [l] can be obtained by removing the elements of [r]
     from the list [k], then it is possible to rearrange [k]
     as [l ++ r]. *)
  Lemma submseteq_Permutation {A : Type} (l k : list A) :
    l ⊆+ k → ∃ r, k ≡ₚ l ++ r.
  Proof.
    revert k; induction l as [|x l];
    intros k Hsubm; [by exists k|].
    assert (∃ r', k ≡ₚ x :: r') as [r' Hr'].
    { rewrite <-elem_of_Permutation.
      by apply (submseteq_subseteq _ _ Hsubm); set_solver.
    }
    rewrite Hr' in Hsubm.
    destruct (IHl _ (submseteq_app_inv_l _ _ [x] Hsubm)) as [p Hp].
    exists p.
    by transitivity (x :: r'); [|apply Permutation_cons].
  Qed.

  (* Remove [x] from both the lists [l] and [k]. *)
  Lemma submseteq_remove {A : Type} (l k : list A) (i j : nat) (x : A) :
    l !! i = Some x →
      k !! j = Some x →
        delete i l ⊆+ delete j k →
          l ⊆+ k.
  Proof.
    intros Hi Hj Hsubm.
    transitivity (x :: delete i l).
    - by rewrite <-(delete_Permutation l i x Hi).
    - transitivity (x :: delete j k).
      + by apply submseteq_skip.
      + by rewrite <-(delete_Permutation k j x Hj).
  Qed.

  (* Remove [x] from the list [l] and from head of the list [x :: k]. *)
  Lemma submseteq_delete_r {A : Type} (l k : list A) (i : nat) (x : A) :
    l !! i = Some x →
      k ⊆+ delete i l →
        x :: k ⊆+ l.
  Proof. intros Hlkp Hsubm. by apply (submseteq_remove _ _ 0 i x). Qed.

End submseteq.


(** * Properties of [concat]. *)

Section concat.

  (* [concat] is monotone on the subseteq relation. *)
  Lemma concat_subseteq {A : Type} :
    Proper ((⊆) ==> (⊆)) (@concat A).
  Proof.
    intros l s Hincl x.
    rewrite !elem_of_list_In, !in_concat.
    intros [xs [Hxs Hx]].
    specialize (Hincl xs).
    revert Hincl. rewrite !elem_of_list_In.
    intros Hincl.
    exists xs. by split; [apply Hincl|].
  Qed.

End concat.


(** * Properties of [fmap]. *)

Section fmap.

  (* [list_fmap] is monotone on the subseteq relation. *)
  Lemma list_fmap_subseteq {A B : Type} :
    Proper ((pointwise_relation A eq) ==> (⊆) ==> (⊆)) (@list_fmap A B).
  Proof.
    intros f g Hfg xs ys Hincl fx.
    rewrite !elem_of_list_fmap.
    intros [x [-> Hx]]. exists x.
    by split; [apply Hfg|apply Hincl].
  Qed.

End fmap.


(** * Miscellaneous. *)

Section misc.

  (* A list containing at least two distinct elements [x] and [y]
     can be rearranged in such a way that [x] and [y] appear as the
     two first elements of the list. *)
  Lemma elem_of_Permutation_2 {A : Type} (x y : A) (l : list A) :
    x ≠ y →
      x ∈ l →
        y ∈ l →
          ∃ r, l ≡ₚ x :: y :: r.
  Proof.
    intros Hneq Hx Hy.
    apply (submseteq_Permutation [x; y] l).
    apply NoDup_subseteq_submseteq; [|set_solver].
    by repeat constructor; set_solver.
  Qed.

  (* If [f <$> l] has no duplicates, then [f] is _injective on [l]_.
     A function [f : A → B] is injective on a set [X], which can be
     implemented as a list, if for every [a] and [a'] in [X], the
     assertion [f a = f a'] implies [a = a']. *)
  Lemma NoDup_fmap_Inj {A B : Type} (f : A → B) (l : list A) :
    NoDup (f <$> l) →
      Inj (λ a b, a ∈ l → b ∈ l → a = b) (eq) f.
  Proof.
    intros HND a b Heq Ha Hb.
    assert (∃ l', l ≡ₚ a :: l') as [l' Hl'].
    { by apply elem_of_Permutation. }
    rewrite Hl', elem_of_cons in Hb.
    destruct Hb as [|Hb]; [done|].
    assert (∃ l'', l' ≡ₚ b :: l'') as [l'' Hl''].
    { by apply elem_of_Permutation. }
    rewrite Hl'' in Hl'.
    rewrite Hl' in HND. simpl in HND.
    inversion HND. rewrite not_elem_of_cons in H1.
    by destruct H1 as [? _].
  Qed.

  (* If [l.*1] has no duplicates, then, for every element [a] in [l.*1],
     there is a unique element [b] such that [(a, b) ∈ l]. *)
  Lemma NoDup_fmap_fst_Inj {A B : Type} a b c (l : list (A * B)) :
    NoDup (l.*1) →
      (a, b) ∈ l →
        (a, c) ∈ l →
          b = c.
  Proof.
    intros HND Hb Hc.
    by injection (NoDup_fmap_Inj fst l HND (a, b) (a, c) eq_refl Hb Hc).
  Qed.

  (* A list of length two has two elements. *)
  Lemma length_2_inv {A : Type} (l : list A) :
    length l = 2 → ∃ x y, l = [x; y].
  Proof.
    case l as [|x l]; [inversion 1|
    case l as [|y l]; [inversion 1|
    case l          ; [inversion 1|]]];
    by exists x, y.
  Qed.

End misc.
