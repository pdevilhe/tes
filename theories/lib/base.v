(* base.v *)

(* This file contains definitions and lemmas of general purpose. *)

From stdpp Require Import base.


(** * Definitions. *)

Section definitions.

  (* A function [f] has _decidable range_ if it is decidable
     to determine whether an element belongs to its range. *)
  Class DecRange `{Equiv A, Equiv B} (f : A → B) :=
    dec_range b : {a | b ≡ f a} + {∀ a, b ≢ f a}.

  (* A function [f] is a _marker_ if it is
     injective and has decidable range. *)
  Class Marker `{Equiv A, Equiv B} (f : A → B) := {
    marker_inj : Inj (≡) (≡) f;
    marker_dec_range : DecRange f;
  }.

  (* [DisjRang f g] holds if [f] and [g] have disjoint range. *)
  Class DisjRange {A} `{Equiv B} (f g : A → B) : Prop :=
    disj_range a a' : f a ≢ g a'.

End definitions.


(** * Properties. *)

Section properties.

  (* If an element [b] belongs to the range of a marker [f],
     then there is a unique [a] s.t. [b ≡ f a]. *)
  Lemma marker_dec_range'
    `{Equiv A, Equiv B, !Symmetric (≡@{B}), !Transitive (≡@{B})}
    (f : A → B) {Hf: Marker f} b :
    {a | b ≡ f a ∧ ∀ a', b ≡ f a' → a ≡ a'} + {∀ a, b ≢ f a}.
  Proof.
    case (marker_dec_range b) as [(a & Ha)|Hb]; [left|right; apply Hb].
    exists a. split; [apply Ha|]. intros a' Ha'.
    apply marker_inj. transitivity b.
    { symmetry. apply Ha. } { apply Ha'. }
  Qed.

  (* The composition of markers is a marker. *)
  Global Instance compose_marker
    `{Equiv A, Equiv B, Equiv C, !Symmetric (≡@{C}), !Transitive (≡@{C})}
    (f : A → B) {Hf: Marker f}
    (g : B → C) {Hg: Marker g} {Hg': Proper ((≡) ==> (≡)) g} :
      Marker (g ∘ f).
  Proof.
    split; [|intro c].
    - apply (compose_inj _ (≡)); apply marker_inj.
    - case (marker_dec_range' g c) as [(b & [Hb Hb'])|Hc  ];[
      case (marker_dec_range    b) as [(a &  Ha)     |Hb''] |].
      { left. exists a. transitivity (g b). apply Hb. apply Hg'. apply Ha. }
      { right. intros a Hc. apply (Hb'' a). apply Hb'. apply Hc. }
      { right. intros a. apply Hc. }
  Qed.

  (* The order of the arguments of [DisjRange] is not important. *)
  Lemma DisjRange_comm {A} `{Equiv B, !Symmetric (≡@{B})}
    (f g : A → B) {Hfg: DisjRange f g} : DisjRange g f.
  Proof. intros a a' Heq. apply (disj_range a' a). symmetry. apply Heq. Qed.

  (* Two functions, [f] and [g], of disjoint range preserve this property
     after the restriction of their domains. *)
  Global Instance compose_disj_range {A B} `{Equiv C}
    (f g : B → C) {Hfg: DisjRange f g} (h h' : A → B) : DisjRange (f ∘ h) (g ∘ h').
  Proof. intros a a'. simpl. apply disj_range. Qed.

End properties.
