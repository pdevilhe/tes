(* pure_weakestpre.v *)

(* In this file, we introduce the _pure extended weakest precondition_,
   a notion of weakest precondition for reasoning about pure programs,
   that is, programs that do not interact with the store. Becasue we are
   restricted to such a class of programs, the definition of the pure
   extended weakest precondition avoids the update modality and can be
   written in terms of only the later modality. This alternative definition
   endows this notion of weakest precondition with stronger reasoning rules.
   For example, the rule [pewp_forall] shows that it is sound to hoist an
   universal quantification from the postcondition.

   The pure extended weakest precondition is useful to give a
   semantic justification of the value restriction.
*)

From iris.proofmode Require Import base tactics classes.
From iris.base_logic.lib Require Import iprop.
From iris.program_logic Require Import weakestpre.
From logic Require Import lang ieff protocol_agreement
                          shallow_handler deep_handler
                          labeled_effects notation heap tactics.
From lib Require Import lists.


(** * Pure Extended Weakest Precondition. *)

(** Definition. *)

Definition pewp_pre `{!irisGS eff_lang Σ} :
  (expr -d> iEff Σ -d> (val -d> iPropO Σ) -d> iPropO Σ) →
  (expr -d> iEff Σ -d> (val -d> iPropO Σ) -d> iPropO Σ)
  := λ pewp e₁ Ψ Φ,
  match to_val e₁ with Some  v     => Φ v | None =>
  match to_eff e₁ with Some (v, k) =>
    protocol_agreement v Ψ (λ w, ▷ pewp (fill k (Val w)) Ψ Φ)
  | None =>
    ∀ σ₁,
      ⌜ reducible e₁ σ₁ ⌝ ∗
      ∀ e₂ σ₂,
        ⌜ prim_step' e₁ σ₁ [] e₂ σ₂ [] ⌝ -∗
          ⌜ σ₁ = σ₂ ⌝ ∧
          ▷ pewp e₂ Ψ Φ
  end
  end%I.

Local Instance pewp_pre_contractive `{!irisGS eff_lang Σ} :
  Contractive pewp_pre.
Proof.
  rewrite /pewp_pre /from_option /protocol_agreement=> n pewp pewp' Hwp e Ψ Φ.
  repeat (f_contractive || f_equiv); try apply Hwp.
Qed.
Definition pewp_def `{!irisGS eff_lang Σ} :
  expr -d> iEff Σ -d> (val -d> iPropO Σ) -d> iPropO Σ :=
  fixpoint pewp_pre.
Definition pewp_aux `{!irisGS eff_lang Σ} : seal pewp_def. by eexists. Qed.
Definition pewp_eq `{!irisGS eff_lang Σ} := pewp_aux.(seal_eq).


(** Notation. *)

Notation "'PEWP' e <| Ψ '|' '>' {{ Φ } }" :=
  (pewp_def e%E Ψ%ieff Φ)
  (at level 20, e, Ψ, Φ at level 200,
   format "'[' 'PEWP'  e  '/' <|  Ψ  '|' '>'  {{  Φ  } }  ']'") : bi_scope.
Notation "'PEWP' e <| Ψ '|' '>' {{ v , Φ } }" :=
  (pewp_def e%E Ψ%ieff (λ v, Φ)%I)
  (at level 20, e, Ψ, Φ at level 200,
   format "'[' 'PEWP'  e  '/'  <|  Ψ  '|' '>'  {{  v ,  Φ  } } ']'") : bi_scope.


(** Reasoning rules. *)

(* We state and prove the reasoning rules for the pure extended weakest
   precondition. *)

Section pewp.
  Context `{!irisGS eff_lang Σ}.
  Implicit Types P : iProp Σ.
  Implicit Types Φ : val → iProp Σ.
  Implicit Types v : val.
  Implicit Types e : expr.

  (* Rewriting lemma. *)
  Lemma pewp_unfold e Ψ Φ : PEWP e <| Ψ |> {{ Φ }} ⊣⊢ pewp_pre pewp_def e Ψ Φ.
  Proof. by apply (fixpoint_unfold pewp_pre). Qed.


  (* Non-expansiveness. *)

  (* [pewp_def] is non-expansive. *)
  Global Instance pewp_ne e n :
    Proper ((dist n) ==> (dist n) ==> (dist n)) (pewp_def e).
  Proof.
    revert e. induction (lt_wf n) as [n _ IH]=> e Ψ Ψ' HΨ Φ Φ' HΦ.
    rewrite !pewp_unfold /pewp_pre /protocol_agreement.
    f_equiv. { by f_equiv. }
    do 6 f_equiv.
    - by apply HΨ.
    - do 4 (f_contractive || f_equiv).
      apply IH; try lia; eapply dist_le; eauto with lia.
    - do 5 (f_contractive || f_equiv).
      induction (num_laters_per_step n) as [|k IH']; simpl.
      + apply IH; try lia; eapply dist_le; eauto with lia.
      + by apply IH'.
  Qed.

  (* [pewp_def] is proper. *)
  Global Instance pewp_proper e : Proper ((≡) ==> (≡) ==> (≡)) (pewp_def e).
  Proof.
    by intros Ψ Ψ' ? Φ Φ' ?; apply equiv_dist=>n; apply pewp_ne; apply equiv_dist.
  Qed.


  (* Value and effects. *)

  (* [pewp_value] *)
  Lemma pewp_value Ψ Φ v : PEWP of_val v <| Ψ |> {{ Φ }} ⊣⊢ Φ v.
  Proof. rewrite pewp_unfold /pewp_pre to_of_val. auto. Qed.
  Lemma pewp_eff Ψ Φ v k : PEWP of_eff v k <| Ψ |> {{ Φ }} ⊣⊢
    protocol_agreement v Ψ (λ w, ▷ PEWP fill k (Val w) <| Ψ |> {{ Φ }}).
  Proof. by rewrite pewp_unfold /pewp_pre /=. Qed.


  (* Monotonicity. *)

  (* In a multishot-continuation semantics, a program [e] can terminate more
     than one, therefore we may need to apply the postcondition-inclusion
     assertion [(∀ w, Φ1 w -∗ Φ2 w)] multiple times. *)
  Lemma pewp_strong_mono Ψ1 Ψ2 Φ1 Φ2 e :
    PEWP e <| Ψ1 |> {{ Φ1 }} -∗
      (Ψ1 ⊑ Ψ2)%ieff -∗
        □ (∀ w, Φ1 w -∗ Φ2 w) -∗
          PEWP e <| Ψ2 |> {{ Φ2 }}.
  Proof.
    iIntros "He #HΨ #HΦ". iLöb as "IH" forall (e).
      destruct (to_val e) as [ v    |] eqn:?;
    [|destruct (to_eff e) as [(v, k)|] eqn:?].
    - rewrite !pewp_unfold /pewp_pre Heqo. by iApply "HΦ".
    - rewrite -(of_to_eff _ _ _ Heqo0) !pewp_eff.
      iApply (protocol_agreement_strong_mono with "He"); auto.
      iModIntro. iIntros (w) "Hk". iNext.
      by iApply ("IH" with "Hk").
    - rewrite !pewp_unfold /pewp_pre Heqo Heqo0.
      iIntros (σ1). iDestruct ("He" $! σ1) as "[%Hred He]".
      iSplit; [done|].
      iIntros (e2 σ2) "%Hσ".
      iDestruct ("He" $! e2 σ2 Hσ) as "[-> He]".
      iSplit; [done|].
      by iApply ("IH" with "He").
  Qed.

  (* If [e] cannot perform effects, then it terminates at most once. *)
  Lemma pewp_mono_no_eff Ψ Φ1 Φ2 e :
    PEWP e <| ⊥ |> {{ Φ1 }} -∗
      (∀ v, Φ1 v -∗ Φ2 v) -∗
        PEWP e <| Ψ |> {{ Φ2 }}.
  Proof.
    iIntros "He HΦ". iLöb as "IH" forall (e).
      destruct (to_val e) as [ v    |] eqn:?;
    [|destruct (to_eff e) as [(v, k)|] eqn:?].
    - rewrite !pewp_unfold /pewp_pre Heqo. by iApply "HΦ".
    - rewrite -(of_to_eff _ _ _ Heqo0) !pewp_eff.
      by rewrite protocol_agreement_bottom.
    - rewrite !pewp_unfold /pewp_pre Heqo Heqo0.
      iIntros (σ1). iDestruct ("He" $! σ1) as "[%Hred He]".
      iSplit; [done|].
      iIntros (e2 σ2) "%Hσ".
      iDestruct ("He" $! e2 σ2 Hσ) as "[-> He]".
      iSplit; [done|].
      by iApply ("IH" with "He").
  Qed.


  (* Step rule. *)

  Lemma pewp_pure_step' e e' Ψ Φ :
    pure_prim_step e e' → 
      ▷ PEWP e' <| Ψ |> {{ Φ }} ⊢ PEWP e <| Ψ |> {{ Φ }}.
  Proof.
    intros Hstep.
      destruct (to_val e) as [ v    |] eqn:He;
    [|destruct (to_eff e) as [(v, k)|] eqn:He'].
    - by specialize (val_not_pure' _ _   e' He).
    - by specialize (eff_not_pure' _ _ _ e' He').
    - rewrite !(pewp_unfold e) /pewp_pre He He'.
      iIntros "He" (σ1). iSplitR.
      { iPureIntro. exists [], e', σ1, []. by apply Hstep. }
      iIntros (e2 σ2 Hpstep).
      destruct (pure_prim_step_det e e' Hstep σ1 e2 σ2 Hpstep) as (-> & ->).
      by (repeat (iSplit; [done|])).
  Qed.

  Lemma pewp_pure_step e e' Ψ Φ :
    pure_prim_step e e' → 
      PEWP e' <| Ψ |> {{ Φ }} ⊢ PEWP e <| Ψ |> {{ Φ }}.
  Proof. iIntros "% He". by iApply (pewp_pure_step' with "He"). Qed.

  Lemma pewp_pure_steps' e e' Ψ Φ :
    tc pure_prim_step e e' → 
      ▷ PEWP e' <| Ψ |> {{ Φ }} ⊢ PEWP e <| Ψ |> {{ Φ }}.
  Proof.
    intros Hstep; iInduction Hstep as [|] "IH".
    - by iApply pewp_pure_step'.
    - iIntros "He". iApply pewp_pure_step'. apply H. iNext. by iApply "IH".
  Qed.

  Lemma pewp_pure_steps e e' Ψ Φ :
    rtc pure_prim_step e e' → 
      PEWP e' <| Ψ |> {{ Φ }} ⊢ PEWP e <| Ψ |> {{ Φ }}.
  Proof.
    intros Hstep; iInduction Hstep as [|] "IH".
    - by iIntros "?".  
    - iIntros "He". iApply pewp_pure_step. apply H. by iApply "IH".
  Qed.


  (* Bind rule. *)

  Lemma pewp_eff_steps K `{NeutralEctx K} Ψ Φ v k :
    PEWP Eff v (ectx_app K k) <| Ψ |> {{ Φ }} ⊢
      PEWP fill K (Eff v k)     <| Ψ |> {{ Φ }}.
  Proof. apply pewp_pure_steps. by apply rtc_pure_prim_step_eff. Qed.

  Lemma pewp_bind K `{NeutralEctx K} Ψ Φ e e' :
    e' = fill K e  →
      PEWP e  <| Ψ |> {{ v, PEWP fill K (of_val v) <| Ψ |> {{ Φ }} }} ⊢
        PEWP e' <| Ψ |> {{ Φ }}.
  Proof.
    intros ->. iIntros "He". iLöb as "IH" forall (e Ψ).
    rewrite !(pewp_unfold e) /pewp_pre.
      destruct (to_val e) as [ v    |] eqn:He;
    [|destruct (to_eff e) as [(v, k)|] eqn:He'].
    - by rewrite <- (of_to_val _ _ He).
    - rewrite <- (of_to_eff _ _ _ He').
      iApply pewp_eff_steps. iApply pewp_eff.
      iDestruct "He" as (Q) "[HP #HQ]".
      iExists Q. iFrame. iIntros "!>" (w) "HQ'".
      iDestruct ("HQ" with "HQ'") as "HQ''". iNext.
      rewrite fill_ectx_app. by iApply "IH".
    - rewrite !pewp_unfold /pewp_pre.
      rewrite (fill_not_val _ _ He) (fill_not_eff K _ He').
      iIntros (σ1). iDestruct ("He" $! σ1) as "[%Hred He]".
      iSplitR. { iPureIntro. by apply reducible_fill. }
      iIntros (e2 σ2) "%Hpstep".
      destruct (Ectx_prim_step_inv K e e2 σ1 σ2 He He' Hpstep) as (e2'&Hstep&->).
      simpl in *.
      iDestruct ("He" $! e2' σ2 Hstep) as "[-> He]".
      iSplit; [done|].
      by iApply ("IH" with "He").
  Qed.

  Lemma Ectxi_pewp_bind Ki `{NeutralEctxi Ki} e Ψ Φ :
    PEWP e <| Ψ |> {{ v, PEWP fill_item Ki (of_val v) <| Ψ |> {{ Φ }} }} ⊢
      PEWP fill_item Ki e <|Ψ|> {{ Φ }}.
  Proof. by iApply (pewp_bind (ConsCtx Ki EmptyCtx)). Qed.


  (* Universal quantification. *)

  (* We prove that it is sound to hoist an universal quantification in the
     postcondition of the pure weakest precondition. *)

  (* Auxiliary lemma - a specified program, that is neither a value nor an
                       effect, takes at least one reduction step. *)
  Lemma pewp_reducible e Ψ Φ :
    TCEq (to_val e) None →
      TCEq (to_eff e) None →
        PEWP e <| Ψ |> {{ Φ }} -∗
          ⌜ ∀ σ, reducible e σ ⌝.
  Proof.
    intros Hval Heff.
    rewrite !pewp_unfold /pewp_pre /=.
      destruct (to_val e) as [ v    |] eqn:?;
    [|destruct (to_eff e) as [(v, k)|] eqn:?].
    - by inversion Hval.
    - by inversion Heff.
    - iIntros "Hpewp". iApply pure_forall_2.
      iIntros (σ). by iDestruct ("Hpewp" $! σ) as "[%H _]".
  Qed.

  (* Auxiliary lemma - reduction steps taken by a [pewp]-specified program
                       do not update the store and do not fork new threads. *)
  Lemma pewp_prim_step e Ψ Φ :
    PEWP e <| Ψ |> {{ Φ }} -∗
      ⌜ ∀ σ1 κ e2 σ2 efs,
          prim_step' e σ1 κ e2 σ2 efs →
            κ = [] ∧ efs = [] ∧ σ1 = σ2 ⌝.
  Proof.
    rewrite !pewp_unfold /pewp_pre /=.
      destruct (to_val e) as [ v    |] eqn:?;
    [|destruct (to_eff e) as [(v, k)|] eqn:?].
    - iIntros "_" (σ1 κ e2 σ2 efs Hstep).
      specialize (val_stuck e σ1 κ e2 σ2 efs Hstep).
      simpl; rewrite Heqo. by inversion 1.
    - iIntros "_" (σ1 κ e2 σ2 efs Hstep).
      cut (reducible e σ1).
      { intros Hred. by rewrite (reducible_not_eff _ _ Hred) in Heqo0. }
      { by exists κ, e2, σ2, efs. }
    - iIntros "Hpewp".
      repeat (iApply pure_forall_2; iIntros (?)).
      destruct (prim_step_inv' _ _ _ _ _ _ a4) as [-> ->].
      iDestruct ("Hpewp" $! a) as "[_ Hpewp]".
      iDestruct ("Hpewp" $! a1 a2 a4) as "[-> _]".
      done.
  Qed.

  (* Auxiliary lemma - universal quantification commutes with the
                       intuitionistically modality. *)
  Lemma intuitionistically_forall' {A : Type} (P : A → iProp Σ) :
    □ (∀ x, P x) ⊣⊢ ∀ x, □ P x.
  Proof.
    iSplit.
    - by iApply bi.intuitionistically_forall.
    - rewrite bi.intuitionistically_into_persistently.
      rewrite bi.persistently_forall.
      iIntros "H" (x).
      rewrite -bi.intuitionistically_into_persistently.
      by iApply "H".
  Qed.

  (* [pewp_def] commutes with [forall]. *)
  Lemma pewp_forall {A} `{Inhabited A} Ψ `{PureProtocol Σ Ψ} e (Φ : A → _ → _) :
    □ (∀ x, PEWP e <| Ψ |> {{ y,      Φ x y }}) ⊢
      (     PEWP e <| Ψ |> {{ y, ∀ x, Φ x y }}).
  Proof.
    iLöb as "IH" forall (e).
    iIntros "#He".
    rewrite !pewp_unfold /pewp_pre.
      destruct (to_val e) as [ v    |] eqn:?;
    [|destruct (to_eff e) as [(v, k)|] eqn:?].
    - iIntros (x). iSpecialize ("He" $! x).
      by rewrite -(pewp_value _ (Φ x)) (of_to_val _ _ Heqo).
    - iDestruct ("He" $! (@inhabitant A H)) as "He'".
      rewrite !(pewp_unfold e) /pewp_pre Heqo Heqo0.
      iDestruct "He'" as "[%Q' [#HΨ' #HQ']]".
      destruct (pure_protocol v) as [Q HQ].
      iDestruct (HQ with "HΨ'") as "[HΨ _]".
      iExists Q. iFrame "HΨ". iModIntro.
      iIntros (w) "Hw". iApply "IH".
      rewrite intuitionistically_forall'.
      rewrite (@bi.later_forall _ A).
      iIntros (x). iSpecialize ("He" $! x).
      rewrite !(pewp_unfold e) /pewp_pre Heqo Heqo0.
      iDestruct "He" as "[%Q'' [HΨ'' #Hk]]".
      iDestruct (HQ with "HΨ''") as "[_ HQ'']".
      iDestruct ("HQ''" with "Hw") as "#Hw'".
      iSpecialize ("Hk" with "Hw'"). iNext. by iModIntro.
    - iIntros (σ1). iSplit; [|].
      { iSpecialize ("He" $! (@inhabitant A H)).
        iDestruct (pewp_reducible with "He") as "%Hred"; try done.
        by rewrite Heqo.
        by rewrite Heqo0.
      }
      iIntros (e2 σ2 Hstep).
      iDestruct (pewp_prim_step with "[He]") as "%Hpstep".
      { by iApply ("He" $! (@inhabitant A H)). }
      destruct (Hpstep σ1 [] e2 σ2 [] Hstep) as (_&_&->).
      iSplit; [done|].
      iApply "IH". iClear "IH".
      rewrite intuitionistically_forall'.
      rewrite bi.later_forall.
      iIntros (x'). iSpecialize ("He" $! x').
      rewrite !(pewp_unfold e) /pewp_pre Heqo Heqo0.
      iDestruct ("He" $! σ2) as "[_ He']".
      iDestruct ("He'" with "[]") as "(_&$)".
      { iPureIntro. by apply Hstep. }
  Qed.


  (* Relation with [ewp]. *)

  (* We prove that [pewp] is stronger than [ewp] in the sense that,
     if the assertion [pewp_def e Ψ Φ] holds for a program [e],
     a protocol [Ψ], and a postcondition [Φ], then the assertion
     [ewp_def e Ψ Φ] holds as well. *)

  Lemma pewp_ewp e Ψ Φ :
    PEWP e <| Ψ |> {{ Φ }} -∗
      EWP e <| Ψ |> {{ Φ }}.
  Proof.
    iLöb as "IH" forall (e).
    iIntros "He".
    rewrite !pewp_unfold /pewp_pre.
      destruct (to_val e) as [ v    |] eqn:?;
    [|destruct (to_eff e) as [(v, k)|] eqn:?].
    - apply of_to_val in Heqo as <-.
      by iApply ewp_value.
    - apply of_to_eff in Heqo0 as <-.
      iApply ewp_eff.
      iApply (protocol_agreement_mono with "He").
      iIntros "!#" (w) "Hk !>".
      by iApply "IH".
    - rewrite ewp_unfold /ewp_pre //= Heqo Heqo0.
      iIntros (?????) "Hσ1".
      iMod (fupd_mask_subseteq ∅) as "Hclose"; first done.
      iModIntro. iDestruct ("He" $! σ₁) as "[[%ks %Hred] He]".
      iSplit; [iPureIntro|].
      { destruct Hred as [e' [σ' [efs Hstep]]].
        by exists ks, e', σ', efs.
      }
      iIntros (?? Hstep).
      destruct κ; [|by inversion Hstep].
      iDestruct ("He" $! _ _ Hstep) as "[-> He]".
      iIntros "!> !> !>".
      iApply (step_fupdN_wand _ _ _ True%I).
      { by iApply step_fupdN_intro. }
      iMod (state_interp_mono with "Hσ1") as "Hσ1".
      iMod "Hclose". iIntros "_ !>".
      rewrite app_nil_l.
      iFrame "Hσ1". by iApply "IH".
  Qed.

End pewp.


(** Tactics for [pewp_def]. *)

Ltac match_pewp_goal lemma tac :=
  match goal with
  | [ |- @bi_emp_valid _                (pewp_def ?e _ _) ] => tac lemma e
  | [ |- @environments.envs_entails _ _ (pewp_def ?e _ _) ] => tac lemma e
  end.

Ltac pewp_pure_step_lemma :=
  iApply pewp_pure_step'.

Ltac pewp_bind_rule_lemma K :=
  iApply (pewp_bind K).

Ltac pewp_pure_step :=
  match_pewp_goal pewp_pure_step_lemma pure_step_tac.

Ltac pewp_bind_rule :=
  match_pewp_goal pewp_bind_rule_lemma bind_rule_tac.

Ltac pewp_value_or_step :=
  ((iApply pewp_value) || (pewp_bind_rule; pewp_pure_step));
  try iNext; simpl.

Ltac pewp_pure_steps :=
  repeat pewp_value_or_step.


(** * Shallow Handler. *)

(* We introduce a reasoning rule for installing a shallow handler. *)

Section shallow_handler.
  Context `{!irisGS eff_lang Σ}.

  Definition is_pure_shallow_handler (h r : expr) Ψ Ψ' Φ Φ' : iProp Σ :=
    (* Return branch. *)
    (∀ (v : val),
       Φ v -∗ PEWP (r v) <| Ψ' |> {{ Φ' }})%I ∧

    (* Effect branch. *)
    (∀ (v k : val),
       protocol_agreement v Ψ (λ w,
           PEWP (k w) <| Ψ |> {{ Φ }}) -∗
       PEWP (h v k) <| Ψ' |> {{ Φ' }})%I.

  Arguments is_pure_shallow_handler _%_E _%_E _%_ieff _%_ieff _%_I _%_I.

  Lemma pewp_try_with Ψ Ψ' Φ Φ' e h r :
    PEWP e <| Ψ  |> {{ Φ  }} -∗
      is_pure_shallow_handler h r Ψ Ψ' Φ Φ' -∗
        PEWP (TryWith e h r) <| Ψ' |> {{ Φ' }}.
  Proof.
    set (Ki := TryWithCtx h r).
    iLöb as "IH" forall (e Ψ).
    destruct (to_val e) as [ v    |] eqn:He; [|
    destruct (to_eff e) as [(v, k)|] eqn:He' ].
    - rewrite <-(of_to_val _ _ He), pewp_value.
      iIntros "HΦ [Hr _]". pewp_pure_step. by iApply "Hr".
    - rewrite <-(of_to_eff _ _ _ He'), pewp_eff.
      iIntros "Hprot [_ Hh]".
      iApply pewp_pure_step'; [by apply pure_prim_step_try_with_eff|].
      iApply "Hh". iApply (protocol_agreement_mono with "Hprot").
      iIntros "!> !#" (w) "Hk".
      iApply pewp_pure_step'; [by apply pure_prim_step_cont|].
      by iApply "Hk".
    - iIntros "He Hhandler".
      rewrite !(pewp_unfold (TryWith _ _ _))
              !(pewp_unfold e) /pewp_pre He He' //=.
      iIntros (σ1). iDestruct ("He" $! σ1) as "[%Hred He]".
      iSplitR. { iPureIntro. by apply (reducible_fill_item Ki). }
      iIntros (e2 σ2) "%Hpstep".
      destruct (Ectxi_prim_step_inv Ki e e2 σ1 σ2 He He' Hpstep) as (e2'&Hstep&->).
      iDestruct ("He" $! e2' σ2 Hstep) as "(%&He)".
      repeat (iSplit; [done|]).
      iNext. by iApply ("IH" with "He").
  Qed.

End shallow_handler.


(** * Deep Handler. *)

(* We introduce a reasoning rule for installing a deep handler. *)

Section deep_handler.
  Context `{!irisGS eff_lang Σ}.

  Definition is_pure_deep_handler :
    (expr -d> expr -d> iEff Σ -d> iEff Σ -d> (_ -d> iPropO Σ) -d>
       (_ -d> iPropO Σ) -d> iPropO Σ)
    := (λ h r Ψ Ψ' Φ Φ',
   (* Return branch. *)
   □ (∀ (v : val),
      Φ v -∗ PEWP (r v) <| Ψ' |> {{ Φ' }}) ∧

   (* Effect branch. *)
   □ (∀ (v k : val),
      protocol_agreement v Ψ (λ w,
        PEWP (k w) <| Ψ' |> {{ Φ' }}) -∗
      PEWP (h v k) <| Ψ' |> {{ Φ' }}))%I.

  Lemma pewp_deep_try_with Ψ Ψ' Φ Φ' (e : expr) (h r : val) :
    PEWP e <| Ψ |> {{ Φ }} -∗
      is_pure_deep_handler h r Ψ Ψ' Φ Φ' -∗
        PEWP (deep_try_with (λ: <>, e) h r) <| Ψ' |> {{ Φ' }}.
  Proof.
    iLöb as "IH" forall (Ψ Ψ' Φ Φ' e).
    unfold deep_try_with.
    iIntros "He [#Hr #Hh]".
    pewp_pure_steps.
    iApply (pewp_try_with with "[He]").
    - pewp_pure_steps. by iApply "He".
    - iSplit. { by iApply "Hr". }
      iIntros (v k) "Hprot".
      do 6 pewp_value_or_step.
      pewp_bind_rule. pewp_pure_step.
      iApply pewp_value. simpl. iApply "Hh".
      iApply (protocol_agreement_strong_mono with "Hprot");
      first iApply iEff_le_refl.
      iIntros "!> !#" (w) "Hk".
      iApply pewp_pure_step'. apply pure_prim_step_beta. simpl.
      iApply ("IH" with "Hk"). iNext.
      by iSplit.
  Qed.

End deep_handler.


(** * Pure Weakest Precondition. *)

(* Now, following the same approach as in the theory [labeled_effects.v],
   we introduce a notion of weakest precondition to reason about pure
   programs that might perform labeled effects. *)


(** Definition. *)

Section pure_weakest_precondition.
  Context `{!heapG Σ}.

  Definition pwp_def :
    expr -d> eff_listO Σ -d> (val -d> iPropO Σ) -d> iPropO Σ :=
    (λ e E Φ,
      EffLabels (E.*1) -∗
        PEWP e <| EFF E |> {{ y, Φ y }})%I.

  Global Instance pwp_ne e E n :
    Proper ((dist n) ==> (dist n)) (pwp_def e E).
  Proof. unfold pwp_def. intros => ???. by repeat (f_equiv; try intros ?). Qed.

  Global Instance pwp_proper e E : Proper ((≡) ==> (≡)) (pwp_def e E).
  Proof. by intros ???; apply equiv_dist=>n; apply pwp_ne; apply equiv_dist. Qed.

End pure_weakest_precondition.


(** Notation. *)

Notation "'PWP' e <| E '|' '>' {{ Φ  } }" :=
  (pwp_def e%E E Φ)
  (at level 20, e, E, Φ at level 200).

Notation "'PWP' e <| E '|' '>' {{ v , Φ } }" :=
  (pwp_def e%E E (λ v, Φ)%I)
  (at level 20, e, E, Φ at level 200).


(** Handler judgment. *)

Section handler_judgment.
  Context `{!heapG Σ}.

  Definition is_pure_handler :
    (expr -d> expr -d> iEff Σ -d> eff_list Σ -d> (_ -d> iPropO Σ) -d>
       (_ -d> iPropO Σ) -d> iPropO Σ)
    := (λ h r Ψ E' Φ Φ',
   (* Return branch. *)
   □ (∀ (v : val),
      Φ v -∗ PWP (r v) <| E' |> {{ Φ' }}) ∧

   (* Effect branch. *)
   □ (∀ (v k : val),
      protocol_agreement v Ψ (λ w,
        PWP (k w) <| E' |> {{ Φ' }}) -∗
      PWP (h v k) <| E' |> {{ Φ' }}))%I.

End handler_judgment.


(** Reasoning rules. *)

Section reasoning_rules.
  Context `{!heapG Σ}.

  Lemma pwp_value E Φ v : Φ v ⊢ PWP of_val v <| E |> {{ Φ }}.
  Proof. iIntros "HΦ [%H HS]". iApply pewp_value. by iFrame. Qed.

  Lemma pwp_pure_step e e' E Φ :
    pure_prim_step e e' → 
      ▷ PWP e' <| E |> {{ Φ }} ⊢ PWP e <| E |> {{ Φ }}.
  Proof.
    iIntros "% He' [% HL]".
    iApply (pewp_pure_step' _ e'); first done.
    iNext. iApply "He'". by iSplit.
  Qed.

  Lemma pwp_bind K `{NeutralEctx K} E Φ e e' :
    e' = fill K e  →
      PWP e  <| E |> {{ v, PWP fill K (of_val v) <| E |> {{ Φ }} }} ⊢
        PWP e' <| E |> {{ Φ }}.
  Proof.
    iIntros (->) "He #HL".
    iSpecialize ("He" with "HL").
    iApply (pewp_bind K); first done.
    iApply (pewp_strong_mono with "He").
    { by iApply iEff_le_refl. }
    iIntros "!#" (w) "HK".
    by iApply "HK".
  Qed.

  Lemma pwp_wp E Φ e :
    PWP e <| E |> {{ Φ }} -∗
      WP e <| E |> {{ Φ }}.
  Proof. iIntros "He #HL". iApply pewp_ewp. by iApply "He". Qed.

End reasoning_rules.


(** Tactics. *)

Ltac match_pwp_goal lemma tac :=
  match goal with
  | [ |- @bi_emp_valid _                (pwp_def ?e _ _) ] => tac lemma e
  | [ |- @environments.envs_entails _ _ (pwp_def ?e _ _) ] => tac lemma e
  end.

Ltac pwp_pure_step_lemma :=
  iApply pwp_pure_step.

Ltac pwp_bind_rule_lemma K :=
  iApply (pwp_bind K).

Ltac pwp_pure_step :=
  match_pwp_goal pwp_pure_step_lemma pure_step_tac.

Ltac pwp_bind_rule :=
  match_pwp_goal pwp_bind_rule_lemma bind_rule_tac.

Ltac pwp_value_or_step :=
  ((iApply pwp_value) || (pwp_bind_rule; pwp_pure_step));
  try iNext; simpl.

Ltac pwp_pure_steps :=
  repeat pwp_value_or_step.


(** Monotonicity. *)

Section monotonicity.
  Context `{!heapG Σ}.

  Lemma pwp_mono E Φ Φ' e :
    □ (∀ v, Φ v -∗ Φ' v) -∗
      PWP e <| E |> {{ Φ }} -∗
        PWP e <| E |> {{ Φ' }}.
  Proof.
    iIntros "#HΦ' He #HL".
    iSpecialize ("He" with "HL").
    iApply (pewp_strong_mono with "He").
    { by iApply iEff_le_refl. }
    iIntros "!#" (y) "H".
    by iApply "HΦ'".
  Qed.

  Lemma pwp_mono_pure E Φ Φ' e :
    (∀ v, Φ v -∗ Φ' v) -∗
      PWP e <| [] |> {{ Φ }} -∗
        PWP e <| E |> {{ Φ' }}.
  Proof.
    iIntros "HΦ' He #HL".
    iSpecialize ("He" with "[]").
    { iApply (EffLabels_incl with "HL").
      by apply fmap_submseteq, submseteq_nil_l.
    }
    iApply (pewp_mono_no_eff _ Φ with "[He]").
    { iApply (pewp_strong_mono with "He").
      { by iApply EFF_empty. }
      { by iIntros "!#" (v) "$". } }
    { iIntros (y) "Hy". by iApply "HΦ'". }
  Qed.

  Lemma pwp_eff_frame E E' Φ e :
    E ⊆+ E' →
      PWP e <| E |> {{ Φ }} -∗
        PWP e <| E' |> {{ Φ }}.
  Proof.
    iIntros (Hsubm) "He #HL".
    iSpecialize ("He" with "[HL]").
    { iApply (EffLabels_incl with "HL").
      by apply fmap_submseteq. }
    iApply (pewp_strong_mono with "He"); last by auto.
    iApply EFF_incl.
    by apply (submseteq_subseteq _ _ Hsubm).
  Qed.

  Corollary pwp_strong_mono E E' Φ Φ' e :
    E ⊆+ E' →
      □ (∀ v, Φ v -∗ Φ' v) -∗
        PWP e <| E |> {{ Φ }} -∗
          PWP e <| E' |> {{ Φ' }}.
  Proof.
    iIntros (Hsub) "#HΦ He".
    iApply (pwp_eff_frame _ _ _ _ Hsub).
    by iApply (pwp_mono with "HΦ He").
  Qed.

  Lemma pwp_weaken l E (Ψ Ψ' : pure_iEff Σ) Φ e :
    (Ψ ⊑ Ψ')%ieff -∗
      PWP e <| (l, Ψ) :: E |> {{ Φ }} -∗
        PWP e <| (l, Ψ') :: E |> {{ Φ }}.
  Proof.
    iIntros "#Hle He #HL".
    iSpecialize ("He" with "[]").
    { by rewrite !fmap_cons. }
    (* Proof by monotonicity of [pewp]. *)
    iApply (pewp_strong_mono with "He"); last by auto.
    by iApply EFF_weaken.
  Qed.

  Corollary pwp_dismiss l E Ψ Φ e :
    PWP e <| (l, ⊥) :: E |> {{ Φ }} -∗
      PWP e <| (l, Ψ) :: E |> {{ Φ }}.
  Proof. iApply pwp_weaken. by iApply iEff_le_bottom. Qed.

End monotonicity.


(** Client side. *)

(* Reasoning rule for performing an effect. *)

Section effects.
  Context `{!heapG Σ}.

  Lemma pwp_perform (l : eff_label) (v : val) E (Ψ : pure_iEff Σ) Φ :
    (l, Ψ) ∈ E →
      protocol_agreement v Ψ Φ -∗
        PWP (perform #l v) <| E |> {{ Φ }}.
  Proof.
    iIntros (Hin) "Hprot".
    unfold perform. pwp_pure_steps.
    iIntros "#HL".
    iApply pewp_eff. rewrite EFF_agreement.
    iDestruct "Hprot" as (Q) "[HP #Hk]".
    iExists l, v, Ψ, Q. iSplit; [done|]. iFrame.
    iSplit; [done|].
    iIntros "!#" (w) "HQ". iNext.
    iApply pewp_value.
    by iApply "Hk".
  Qed.

End effects.


(** Shallow Handler. *)

(* Reasoning rule for shallow handlers. *)

Section shallow_handler.
  Context `{!heapG Σ}.

  (* Handler judgment. *)
  Definition is_pure_shandler :
    (expr -d> expr -d> eff_label -d>
       pure_iEff Σ -d> eff_list Σ -d> eff_list Σ -d>
         (_ -d> iPropO Σ) -d> (_ -d> iPropO Σ) -d> iPropO Σ)
    := (λ h r l Ψ E E' Φ Φ',
   (* Return branch. *)
   □ (∀ (v : val),
      Φ v -∗ PWP (r v) <| E' |> {{ Φ' }}) ∧

   (* Effect branch. *)
   □ (∀ (v k : val),
      protocol_agreement v Ψ (λ w,
        PWP (k w) <| (l, Ψ) :: E |> {{ Φ }}) -∗
      PWP (h v k) <| E' |> {{ Φ' }}))%I.

  Lemma pwp_shandle (l : eff_label) E Ψ Ψ' Φ Φ' (client h r : val) :
    let E' := (l, Ψ') :: E in
    PWP client #() <| (l, Ψ) :: E |> {{ Φ }} -∗
      is_pure_shandler h r l Ψ E E' Φ Φ' -∗
        PWP (shandle #l client h r) <| E' |> {{ Φ' }}.
  Proof.
    intros E'.
    iIntros "Hclient [#Hr #Hh] [%HND #HEff]".
    assert (l ∉ E.*1) as Hnot_in. { by inversion HND. }
    iSpecialize ("Hclient" with "[]"). { by iSplit. }
    iLöb as "IH" forall (client).
    unfold shandle. pewp_pure_steps.
    iApply (pewp_try_with with "Hclient").
    iSplit.
    (* Return branch. *)
    { iIntros (v) "Hv". pewp_pure_steps.
      iApply ("Hr" with "Hv"); by iSplit.
    }
    (* Effect branch. *)
    { iIntros (v k). rewrite EFF_agreement.
      iIntros "[%l' [%v' [%Ψ'' [%Q (-> & [%Hin HΨ''] & #Hk) ]]]]".
      revert Hin; rewrite elem_of_cons or_comm; intro Hl'.
      pewp_pure_steps; [done|].
      case (decide (l = l')) as [->|Hneq].
      { rewrite bool_decide_eq_true_2; [|done].
        case Hl'.
        { intro Hin. cut (l' ∈ E.*1); [done|].
          by apply (elem_of_list_fmap_1 fst _ _ Hin).
        }
        { injection 1. intros ->. pewp_pure_steps.
          iApply ("Hh" with "[HΨ'']"); [|by iSplit].
          iExists Q. iFrame.
          iIntros "!#" (w) "HQ _".
          by iApply ("Hk" with "HQ").
        }
      }    
      { rewrite bool_decide_eq_false_2; [|by injection 1; intros ->].
        pewp_pure_steps.
        iApply pewp_eff.
        rewrite EFF_agreement.
        iExists l', v', Ψ'', Q. repeat (iSplit; try done).
        { iPureIntro. by case Hl' as [|]; set_solver. }
        iIntros "!#" (w) "HQ".
        iNext. iApply pewp_value. do 5 pewp_value_or_step.
        iApply "IH". pewp_pure_steps.
        by iApply "Hk".
      }
    }
  Qed.

End shallow_handler.


(** Deep Handler. *)

(* Reasoning rule for deep handlers. *)

Section handler.
  Context `{!heapG Σ}.

  Lemma pwp_handle (l : eff_label) E Ψ Ψ' Φ Φ' (client h r : val) :
    let E' := (l, Ψ') :: E in
    PWP client #() <| (l, Ψ) :: E |> {{ Φ }} -∗
      is_pure_handler h r Ψ E' Φ Φ' -∗
        PWP (handle #l client h r) <| E' |> {{ Φ' }}.
  Proof.
    intros E'.
    iIntros "Hclient [#Hr #Hh]".
    unfold handle.
    do 2 pwp_value_or_step.
    iLöb as "IH" forall (client).
    pwp_pure_steps.
    iApply (pwp_shandle with "Hclient").
    iSplit; iModIntro.
    (* Return branch. *)
    { iIntros (v) "Hv". pwp_pure_steps.
      iApply ("Hr" with "Hv"); by iSplit.
    }
    (* Effect branch. *)
    { iIntros (v k) "Hprot".
      pwp_pure_steps.
      iApply "Hh".
      iApply (protocol_agreement_mono with "Hprot").
      iIntros "!#" (w) "Hk". do 3 pwp_value_or_step.
      iApply "IH". by pwp_pure_steps.
    }
  Qed.

End handler.


(** Catch All. *)

(* Reasoning rule for catch-all handlers. *)

Section catch_all.
  Context `{!heapG Σ}.

  (* Catch-all judgment. *)
  Definition is_pure_catch_all_handler :
    (expr -d> expr -d> eff_list Σ -d> (_ -d> iPropO Σ) -d>
       (_ -d> iPropO Σ) -d> iPropO Σ)
    := (λ h r E Φ Φ',
   (* Return branch. *)
   □ (∀ (v : val),
      Φ v -∗ PWP (r v) <| E |> {{ Φ' }}) ∧

   (* Effect branch. *)
   □ (∀ Ψ (f x k : val),

      (* Specification of [f]. *)
      □ (∀ (x : val) Φ'',
         protocol_agreement x Ψ Φ'' -∗
           PWP (f x) <| E |> {{ Φ'' }}) -∗

      (* Joint specification of [x] and [k]. *)
      protocol_agreement x Ψ (λ w,
        PWP (k w) <| E |> {{ Φ' }}) -∗

      PWP (h f x k) <| E |> {{ Φ' }}))%I.

  Lemma pewp_catch_all E Φ Φ' (client h r : val) :
    PEWP client #() <| EFF E |> {{ Φ }} -∗
      is_pure_catch_all_handler h r E Φ Φ' -∗
        PWP (catch_all client h r) <| E |> {{ Φ' }}.
  Proof.
    iIntros "Hclient [#Hr #Hh] [%HND #HEff]".
    iLöb as "IH" forall (client).
    unfold catch_all. do 7 pewp_value_or_step.
    iApply (pewp_try_with with "Hclient").
    iSplit.
    (* Return branch. *)
    { iIntros (v) "HΦ". pewp_pure_steps.
      iApply ("Hr" with "HΦ"); by iSplit.
    }
    (* Effect branch. *)
    { iIntros (x k) "Hprot".
      rewrite EFF_agreement.
      iDestruct "Hprot" as (l' v' Ψ Q) "(-> & (% & HΨ'') & #Hk)".
      pewp_pure_steps.
      iApply ("Hh" $! Ψ _ v' _ with "[] [HΨ'']"); last by iSplit.
      - iIntros "!#" (x Φ'') "Hprot". pwp_pure_steps.
        by iApply (pwp_perform with "Hprot").
      - iExists Q. iFrame.
        iIntros "!#" (w) "HQ _". do 3 pewp_value_or_step.
        iApply "IH". pewp_pure_steps.
        by iApply "Hk".
    }
  Qed.

  Lemma pwp_catch_all E Φ Φ' (client h r : val) :
    PWP client #() <| E |> {{ Φ }} -∗
      is_pure_catch_all_handler h r E Φ Φ' -∗
        PWP (catch_all client h r) <| E |> {{ Φ' }}.
  Proof.
    iIntros "Hclient Hrh #HLabels".
    iSpecialize ("Hclient" with "HLabels").
    by iApply (pewp_catch_all with "Hclient Hrh").
  Qed.

End catch_all.


(** Try Finally. *)

Section try_finally.
  Context `{!heapG Σ}.

  Lemma pwp_try_finally (e₁ e₂ : val) E Φ :
    PWP e₁ #() <| E |> {{ _, True }} -∗
      PWP e₂ #() <| [] |> {{ Φ }} -∗
        PWP (try_finally e₁ e₂) <| E |> {{ Φ }}.
  Proof.
    iIntros "He₁ He₂ #HL".
    iSpecialize ("He₁" with "HL").
    iSpecialize ("He₂" with "[]").
    { by iApply EffLabels_nil. }
    unfold try_finally. pewp_pure_steps.
    iApply (pewp_try_with with "He₁").
    iSplit.
    { iIntros (?) "_".
      pewp_pure_steps. pewp_bind_rule.
      iApply (pewp_strong_mono with "He₂").
      { iApply (iEff_le_trans _ ⊥%ieff).
        + by iApply EFF_empty.
        + by iApply iEff_le_bottom.
      }
      { by auto. }
    }
    { iIntros (v k) "Hk". pewp_pure_steps.
      iApply (pewp_strong_mono with "He₂").
      { iApply (iEff_le_trans _ ⊥%ieff).
        + by iApply EFF_empty.
        + by iApply iEff_le_bottom.
      }
      { by auto. }
    }
  Qed.

End try_finally.


(** Dynamic Wind *)

(* Reasoning rule for [dynamic_wind]. *)

Section dynamic_wind.
  Context `{!heapG Σ}.

  Lemma pwp_dynamic_wind (pre body post : val) E Φ :
    □ PWP pre #() <| [] |> {{ _, True }} -∗
      □ PWP post #() <| [] |> {{ _, True }} -∗
        PWP body #() <| E |> {{ Φ }} -∗
          PWP dynamic_wind pre body post <| E |> {{ Φ }}.
  Proof.
    iIntros "#Hpre #Hpost Hbody".
    unfold dynamic_wind.
    pwp_pure_steps.
    pwp_bind_rule. simpl.
    iApply (pwp_mono_pure with "[Hbody] Hpre").
    iIntros (w) "_".
    pwp_pure_steps.
    iApply (pwp_catch_all with "Hbody").
    iSplit.
    { iIntros "!#" (x) "Hx".
      pwp_pure_steps. pwp_bind_rule.
      iApply (pwp_mono_pure with "[Hx] Hpost").
      iIntros (?) "_". simpl. by pwp_pure_steps.
    }
    { iIntros "!#" (Ψ do' x k) "#Hdo Hprot".
      pwp_pure_steps. pwp_bind_rule.
      iApply (pwp_mono_pure with "[Hprot] Hpost").
      iIntros (?) "_". simpl. pwp_pure_steps.
      pwp_bind_rule. simpl.
      iApply "Hdo".
      iApply (protocol_agreement_mono with "Hprot").
      iIntros "!#" (w') "Hk". pwp_pure_steps.
      pwp_bind_rule. simpl.
      iApply (pwp_mono_pure with "[Hk] Hpre").
      iIntros (?) "_". simpl. by pwp_pure_steps.
    }
  Qed.

End dynamic_wind.


(** Universal quantification. *)

Section universal.
  Context `{!heapG Σ}.

  (* [pwp_def] commutes with [forall]. *)
  Lemma pwp_forall {A} `{Inhabited A} e E (Φ : A → _ → _) :
    □ (∀ x, PWP e <| E |> {{ y,      Φ x y }}) ⊢
      (     PWP e <| E |> {{ y, ∀ x, Φ x y }}).
  Proof.
    iIntros "#He [%HND #HE]".
    specialize (EFF_pure_protocol E HND) as HE.
    iApply (@pewp_forall _ _ A _ (EFF E) HE).
    iIntros "!#" (x).
    by iApply ("He" $! x); iSplit.
  Qed.

End universal.
