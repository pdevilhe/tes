# Tes - A Type System for Effect Handlers and Dynamic Labels

This repository contains the Coq formalization of `Tes`.


## Prerequisites

The project is known to compile with:

 - Coq 8.19.2

 - [std++](https://gitlab.mpi-sws.org/robbertkrebbers/coq-stdpp) version dev.2024-06-17.2.2bcaf3c5

 - [Iris](https://gitlab.mpi-sws.org/iris/iris) version dev.2024-07-19.0.426f2d2e


## Installation

To build the project, please follow the instructions in the file
[INSTALL.md](INSTALL.md).

These instructions show you how to create a separate opam switch with the
necessary packages to build the project, thus keeping your previous
switches clean.

Alternatively, you can install the packages by yourself
according to their versions as listed in the file [opam](opam).


## Repository Structure

The repository is divided into three subrepositories: lib, logic, and
effect_system. The subrepository lib includes general-purpose lemmas that
complement the library `stdpp`; effect_system contains the development of
`Tes` itself; and logic contains the development of `TesLogic`.


### Lib

 - [theories/lib/base.v](theories/lib/base.v): General-purpose lemmas.
 - [theories/lib/lists.v](theories/lib/lists.v): Lemmas about lists.
 - [theories/lib/maps_and_sets.v](theories/lib/maps_and_sets.v): Lemmas
   about `stdpp`'s maps and sets.

### Logic

 - [theories/logic/lib.v](theories/logic/lib.v): General definitions and
   lemmas.
 - [theories/logic/lang.v](theories/logic/lang.v): Definition of the low-level
   language HH.
 - [theories/logic/notation.v](theories/logic/notation.v): Introduction of
   notation for common constructs of the language.
 - [theories/logic/ieff.v](theories/logic/ieff.v): Definition of protocols and
   auxiliary operators.
 - [theories/logic/protocol_agreement.v](theories/logic/protocol_agreement.v):
   Introduction and study of the protocol agreement judgment.
 - [theories/logic/extended_weakestpre.v](theories/logic/extended_weakestpre.v):
   Definition of the weakest precondition and proof of usual reasoning rules.
 - [theories/logic/labeled_effects.v](theories/logic/pure_weakestpre.v):
   Definition of the pure weakest precondition.
 - [theories/logic/tactics.v](theories/logic/tactics.v): Automation of step and
   bind rules.
 - [theories/logic/heap.v](theories/logic/heap.v): Proof of the reasoning rules
   for operations manipulating the heap.
 - [theories/logic/shallow_handler.v](theories/logic/shallow_handler.v):
   Reasoning rule for shallow handlers.
 - [theories/logic/deep_handler.v](theories/logic/deep_handler.v): Reasoning
   rule for deep handlers.
 - [theories/logic/labeled_effects.v](theories/logic/labeled_effects.v): Reasoning
   rules for labeled effects.
 - [theories/logic/adequacy.v](theories/logic/adequacy.v): Proof of the Adequacy
   Theorem, the soundness theorem of `TesLogic`.

### Effect System

 - [theories/effect_system/subst_map.v](theories/effect_system/subst_map.v):
   Definition of the substitution of variables with values given by a map.
 - [theories/effect_system/types.v](theories/effect_system/types.v): The syntax
   of types and related definitions.
 - [theories/effect_system/notation.v](theories/effect_system/notation.v):
   Introduction of notation for types and constructs of the surface language.
 - [theories/effect_system/rules.v](theories/effect_system/rules.v): Definition
   of the typing and subsumption rules. This file also includes a number of examples
   of well-typed programs.
 - [theories/effect_system/mixed_weakestpre.v](theories/effect_system/mixed_weakestpre.v):
   Introduction of the notion of the _mixed weakest precondition_, which fuses the
   pure and impure flavors of wp into a single notion indexed by a purity attribute.
 - [theories/effect_system/sem_operators.v](theories/effect_system/sem_operators.v):
   Specification of binary and unary operators.
 - [theories/effect_system/interp.v](theories/effect_system/interp.v):
   Semantic interpretation of typing judgments and subsumption relations.
 - [theories/effect_system/compatibility_le.v](theories/effect_system/compatibility_le.v):
   Proof of the compatibility lemmas for the subsumption relation.
 - [theories/effect_system/compatibility.v](theories/effect_system/compatibility.v):
   Proof of the compatibility lemmas.
 - [theories/effect_system/fundamental_le.v](theories/effect_system/fundamental_le.v):
   Proof of the Fundamental Theorem of the subsumption relation.
 - [theories/effect_system/fundamental.v](theories/effect_system/fundamental.v):
   Proof of the Fundamental Theorem.
 - [theories/effect_system/soundness.v](theories/effect_system/soundness.v):
   Proof of soundness of `Tes`.


## Paper-Formalization Correspondence

### Syntax and semantics of `TesLang`

The `TesLang` calculus is defined on top of a more basic calculus,
simply called `lang`. This basic calculus has support for unnamed effects,
multi-shot continuations, and shallow handlers. Both the syntax and the
operational semantics of `lang` are defined in the file
[lang.v](theories/logic/lang.v).
There, you will find the following definitions:

  + **Syntax.** `expr` is the type of expressions.

  + **Evaluation contexts.** `ectx_item` is the type of shallow evaluation contexts
    and `ectx`, the type of deep evaluation contexts.

  + **Semantics.** `head_step` is the head step reduction relation and `prim_step`
    is its closure under evaluation contexts.

  + **Neutral contexts.** a evaluation context is neutral when it does not
    catch an effect. The predicate `NeutralEctxi` holds for neutral shallow
    contexts and `NeutralEctx`, for deep contexts.

In `lang`, the instruction for performing an effect is noted `Do e`,
and a shallow handler for `e` with effect and return branches `h` and `r`
is noted `TryWith e h r`. Binders are noted `Var t x`; they  carry a
_sort_ `t` and a string `x`. If the sort is `V`, then `x` binds a value.
If the sort is `L`, then `x` binds a label (a memory location).
Recursive functions are noted `Rec t f x e`. They also carry a sort
indicating the nature of the string `x` (if it binds values or labels);
we use the sort `L` to encode the introduction of effect labels in `TesLang`.
The instruction for creating a reference with initial value `v` is noted
`Alloc v`.

We encode the `TesLang` layer in the file
[labeled_effects.v](theories/logic/labeled_effects.v).
There, you will see that `TesLang` programs are a subset of `lang` programs
that perform effects whose payload are pairs of a label and a value.
A handler for a label `l` intercepts every effect. If the first component
of the payload coincides with `l` then the handler calls the effect branch,
otherwise the handler _forwards_ this effect. First, we define a shallow
handlers for labeled effects (noted `shandle`) and then
we a define deep handler (noted `handle`) as a recursive shallow handler
that reinstalls itself on the top frame of the captured continuation.

The Coq notation closely matches the paper notation:


| Paper                       | Coq formalization |
|-----------------------------|-------------------|
| `effect s in e`             | `effect: s in e`  |
| `perform l v`               | `perform l v`     |
| `handle e with l: h \| r`   | `handle l e h r`  |


### Type system

The set of typing rules is defined in the file
[rules.v](theories/effect_system/rules.v).
as the inductive type `typed`.

We include a rule for shallow
handlers, rule `SHandle_typed`, and 
prove the rule for deep handlers,
rule `Handle_typed`, as a derived rule.
Moreover,
we include rule `DynamicWind_typed`
to prove that the system extended with `dynamic-wind` is sound,
a claim we make in the Related Work Section of the paper.

The subsumption relations are also defined in the file
[rules.v](theories/effect_system/rules.v).
The subsumption relations on signatures, rows, and types
are respectively defined as the inductive types `le._eff_sig`,
`le._row`, and `le._ty`. They match the rules
presented in the paper.

Here is how the Coq notation of types, typing judgments, and
subsumption relations compares to the notation introduced
in the paper:


| Paper                      | Coq formalization            |
|----------------------------|------------------------------|
| `Ξ \| Δ \| Γ ⊢ e : ρ : τ`  | `Δ .\| Γ .\|-{a}- e : ρ : τ` |
| `D ⊢ σ ≤_S σ'`             | `D ⊢ σ ≤S σ'`                |
| `D ⊢_b ρ ≤_R ρ'`           | `D ⊢ ρ ≤R ρ' @ b`            |
| `D ⊢ τ ≤_T κ`              | `D ⊢ τ ≤T κ`                 |
| `τ ->^ρ κ`                 | `τ -{a;ρ}-> κ`               |
| `(s : ι => κ)`             | `ESig s ι κ`                 |
| `(s : abs)`                | `EAbs s`                     |
| `α`                        | `TVar i`                     |
| `∀ α, τ`                   | `∀T: τ`                      |
| `θ`                        | `RVar i`                     |
| `∀ θ, τ`                   | `∀R: τ`                      |


*Notes*

 + Row- and type-variable context disappears in the Coq
   formalization, because we use de Bruijn indices
   to represent these variables.

 + The term `a` that appears in the Coq version of
   typing judgments and arrows is a _purity attribute_.
   Purity attributes allow the system to soundly
   generalize polymorphic row and type variables
   over a class of expressions larger than the
   the class of values.
   Purity attributes are omitted from the paper for
   the sake of simplicity and conciseness of the
   presentation.

